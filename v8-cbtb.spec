------------------------------------------
-- Testbed infrastructure for setting up
-- MMU tests to compare with the ARM MMU
-- model
--
-- Christoph Baumann, KTH, 2016
-- cbaumann@kth.se
------------------------------------------

{-

val () = Runtime.LoadF "../v8-base_types.spec, ../v8-mem_types.spec, ../v8-base.spec, ../v8-mem.spec, ../v8-mmu_types.spec, ../v8-mmu.spec, ../v8-virt.spec, ../v8-iset.spec, ../v8-seqmem.spec,  ../v8-core.spec, ../v8-testbed.spec";

-}

record CB_pte {
    a :: ADR
    d :: dword
}

-- no walk cache types / shareability for the moment
-- no mair setup
-- no ASID
record CB_PT {
    v	:: bool 		-- valid page table?
    pto :: ADR 			-- page table base address
    up :: bool 		-- upper or lower page table (for EL1/0 only)
    output_w :: nat 	-- output width
    input_w :: nat 		-- input width
    granule :: nat 		-- 4 16, or 64K granule
    skipl :: nat 		-- skip this number of levels (for ST2 only)
    pte0 :: CB_pte 		-- level 0 page table entry
    pte1 :: CB_pte 		-- level 1 page table entry
    pte2 :: CB_pte 		-- level 2 page table entry
    pte3 :: CB_pte 		-- level 3 page table entry
}


record CB_params {
    pt_st1 :: CB_PT		-- stage 1 setup
    pt_st2 :: CB_PT		-- stage 2 setup
    el :: bits(2)		-- exception level or the test
    ns :: bool			-- non-secure bit
    lend :: bool 		-- little endian
    A :: bool 			-- alignment checks
}

MEM_ABS CB_MMU_setup (cbp :: CB_params) =
{
    var pm :: MEM_ABS;
    mode_setup(cbp.ns, [cbp.el]);
    set_endianness([cbp.el], cbp.lend);
    set_align_checks([cbp.el], cbp.A);
    
    when cbp.pt_st1.v do {
	set_PTO_ST1 ([cbp.el], cbp.pt_st1.up, cbp.pt_st1.pto, 0`16); --`
	set_input_address_size_ST1([cbp.el], cbp.pt_st1.up, cbp.pt_st1.input_w);
	match cbp.pt_st1.output_w {
	    case 32 => set_output_address_size_ST1([cbp.el], 0`32) --`
	    case 36 => set_output_address_size_ST1([cbp.el], 0`36) --`
	    case 40 => set_output_address_size_ST1([cbp.el], 0`40) --`
	    case 42 => set_output_address_size_ST1([cbp.el], 0`42) --`
	    case 44 => set_output_address_size_ST1([cbp.el], 0`44) --`
	    case _ => set_output_address_size_ST1([cbp.el], 0`48) --`
		};
	match cbp.pt_st1.granule {
	    case 4 => set_granule_size_ST1([cbp.el], cbp.pt_st1.up, 0`4) --`
	    case 16 => set_granule_size_ST1([cbp.el], cbp.pt_st1.up, 0`16) --`
	    case _ => set_granule_size_ST1([cbp.el], cbp.pt_st1.up, 0`64) --`
		};
	pm <- write_dword (cbp.pt_st1.pte0.a, cbp.pt_st1.pte0.d, pm);
	pm <- write_dword (cbp.pt_st1.pte1.a, cbp.pt_st1.pte1.d, pm);
	pm <- write_dword (cbp.pt_st1.pte2.a, cbp.pt_st1.pte2.d, pm);
	pm <- write_dword (cbp.pt_st1.pte3.a, cbp.pt_st1.pte3.d, pm)
	};

    when cbp.pt_st2.v do {
	set_PTO_ST2 (cbp.pt_st2.pto, 0`8); --`
	set_input_address_size_ST2(cbp.pt_st2.input_w);
	match cbp.pt_st2.output_w {
	    case 32 => set_output_address_size_ST2(0`32) --`
	    case 36 => set_output_address_size_ST2(0`36) --`
	    case 40 => set_output_address_size_ST2(0`40) --`
	    case 42 => set_output_address_size_ST2(0`42) --`
	    case 44 => set_output_address_size_ST2(0`44) --`
	    case _ => set_output_address_size_ST2(0`48) --`
		};
	match cbp.pt_st1.granule {
	    case 4 => set_granule_size_ST2(0`4) --`
	    case 16 => set_granule_size_ST2(0`16) --`
	    case _ => set_granule_size_ST2(0`64) --`
	        };

	skip_levels_ST2(cbp.pt_st2.skipl);
	
	pm <- write_dword (cbp.pt_st1.pte0.a, cbp.pt_st1.pte0.d, pm);
	pm <- write_dword (cbp.pt_st1.pte1.a, cbp.pt_st1.pte1.d, pm);
	pm <- write_dword (cbp.pt_st1.pte2.a, cbp.pt_st1.pte2.d, pm);
	pm <- write_dword (cbp.pt_st1.pte3.a, cbp.pt_st1.pte3.d, pm)
    };
    return pm
}




