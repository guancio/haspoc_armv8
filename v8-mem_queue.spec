-----------------------------------------------------------------------
-- This is specification of a simple storage of memory requests      --	
-- 								     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------

{-

val () = Runtime.LoadF "v8-mem_types.spec, v8-mem_queue.spec";

-}


--------------------------
-- memory request queue --
--------------------------

declare {
    Req :: nat -> REQUEST
    Qsize :: nat
    Pend :: nat option
}

nat issue_read (r :: READ) = 
{
    (desc, sz, at) = r;
    var req;
    req.write <- false;
    req.desc <- desc;
    req.bytesize <- sz;
    req.acctype <- at;
    req.served <- false;
    q = Qsize;
    Req(q) <- req;
    Qsize <- Qsize + 1;
    Pend <- Some(q);
    return q
}

exception MEMORY_MODEL_SCHED_FAULT

bool list * FaultRecord answer_read (q :: nat) = 
{
    req = Req(q);
 
    -- bad scheduling of receive action or write requested
    when (req.write or !req.served) do #MEMORY_MODEL_SCHED_FAULT;

    Pend <- None;
    return (req.data, req.fault)
}


nat issue_write (w :: WRITE) = 
{
    (desc, sz, at, d) = w;
    var req;
    req.write <- true;
    req.desc <- desc;
    req.bytesize <- sz;
    req.acctype <- at;
    req.data <- d;
    req.served <- false;
    q = Qsize;
    Req(q) <- req;
    Qsize <- Qsize + 1;
    Pend <- Some(q);
    return q
}

FaultRecord answer_write (q :: nat) = 
{
    req = Req(q);

    -- bad scheduling of receive action or write requested
    when (!req.write or !req.served) do #MEMORY_MODEL_SCHED_FAULT;

    Pend <- None;
    return req.fault
}
