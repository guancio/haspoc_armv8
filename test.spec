type MEM = bits(48) -> bits(8)

nat * MEM make_mem() = {var m :: MEM; return (1, m)}
