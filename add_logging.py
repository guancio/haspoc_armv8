# script to add logging functionality to the ARMv8 L3 model, scans spec files
# for register references and updates, replaces them with LOG functions, also
# uncomments event LOG functions marked with --@LOG.*

import re, sys

# important patterns to catch different system variables
confvar = r'(?<!M)(?:C|MMU)\.&?\w+(?:\(\w*\))?'
abstrvar = r'&?(?:SCTLR|TTBR0|MAIR)\(\w*\)'
mainvar = r'(?:' + confvar + '|' + abstrvar + ')'
sysvar = mainvar + r'(?:\.\w+)?'


# filter out irrelevant configuration components, w=write
# MMU components are never written by MMU, thus irrelevant
def relevant(c,w):
    m = re.match(r'(C|MMU)\.&?(\w*).*', c)
    if re.match(abstrvar + r'(?:\.\w+)?', c) != None:
        return True
    elif m.group(1) == 'C':
        return not(m.group(2) in ('S', 'CURR_INSTR', 'CURR_REQ',
                                  'CURR_REPLY', 'CURR_FAULT', 'CORE_ID',
                                  'REQ_CNT', 'branch_hint'))
    elif m.group(1) == 'MMU' and not w:
        return not(m.group(2) in ('ST1', 'W1', 'F1', 'SZ1', 'DESC1',
                                  'ST2', 'W2', 'F2', 'DESC2', 'MEMREQ',
                                  'ST2RPL', 'DBG1', 'DBG2', 'S', 'CURR_REQ'))
    else:
        return False

def add_tc(s):
    if re.match(r'\[.*\]', s) == None:
        return '[' + s + ']'
    else:
        return s

def strip_amp(s):
    m = re.match(r'(.*)&(.*)', s)
    if m == None:
        return s
    else:
        return m.group(1) + m.group(2)
        
def repl(m,w):
    # get main variable and potential bitfield
    part = re.match(r'(' + mainvar + r')\.(\w+)',m.group(1))

    if relevant(m.group(1),w):
        # try to extract component index expression if there is one
        comp = re.match(r'(.*)\((\w+)\).*',m.group(1))
        if comp == None:
            idx = 'NO_INDEX'
            if part == None:
                mp = m.group(1)
            else:
                mp = part.group(1)
        else:
           idx = add_tc(comp.group(2))
           mp = comp.group(1)

           
        # distinguish reads and writes
        if w:
            log = m.group(1) + ' <- [LOG_RW'
            fwd = add_tc(m.group(2))
        else:
            log = '[LOG_RR'
            fwd = add_tc(m.group(1))

        mp = strip_amp(mp)
            
        if part == None:
            #LOG_R[RW]X on value for whole register write
            return (log + 'X("' + mp + '",' + idx + ',' + fwd + ')]')
        else:
            #LOG_R[RW]B on value for bitfield write
            return (log + 'B("' + mp + '",' + idx + ',"' + part.group(2) +
                    '",' + fwd + ')]')
        
    # if not relevant leave unchanged
    else:
        return m.group(0)

def repl_write(m):
    return repl(m,True)

def repl_read(m):
    return repl(m,False)
    
def process_line(m):
    #print(m.group(0))

    # replace assignments
    l1 = re.sub(r'('+sysvar+r')\s*<-\s*([^;\n\}]*)',
                repl_write, m.group(0))

    # replace read references in result
    l2 = re.sub(r'(?<!")('+sysvar+r')(?= |,|:|;|\n|\)|\]|`|\})(?!\s*<-\s*)',
                repl_read, l1)
    
    #print l2
    
    return l2

fh = file(sys.argv[1], 'r')
f = fh.read()
fh.close()

l = re.sub(r'.*' + sysvar + r'.*\n', process_line, f)

# uncomment --@LOG.*
l = re.sub(r'--@LOG', r'LOG', l)

# remove excessive type casts
l = re.sub(r'\[\[(LOG.*?)\]\]', r'[\1]', l)

#print(l)

# write output file
out = re.sub(r'(\W*)(.*)', r'\1LOG_\2', sys.argv[1])
f_out = file(out, 'w')
f_out.write(l)
f_out.close()
