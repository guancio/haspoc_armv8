-----------------------------------------------------------------------
-- This is a formal specification of the			     --
-- ARMv8 address translation mechnism and MMU			     --
-- 								     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2014-11-05	moved to separate file
-- 2014-12-09   first version of stage 1 MMU
-- 2014-12-11   reorganized functions into atomic transitions on memory
-- 2014-12-18   added address size faults for output address, all faults depending on implementation defined behaviour

{-

val () = Runtime.LoadF "v8-base_types.spec, v8-mem_types.spec, v8-mmu_types.spec, v8-mmu.spec";

-}


-- declare MMU :: MMU_CONFIG
declare MMUISW :: MMU_IMPL_SWITCHES



---------------------------------
-- Address translation parameters
---------------------------------


-- implemented physical address size
nat PAlen(IDMM :: mm_feat) = ps2len(IDMM.PAR<2:0>, 48)


-- (intermediate) physical address length (for stage 1 translation), bounded by implemented physical address size
bits(3) PS_ST1(EL :: nat, MMU :: MMU_CONFIG1) = 
{
    ps = match EL
    {
	case 0 or 1    => MMU.TCR1.IPS
        case _         => MMU.TCR23.PS
    };
    if ps >+ MMU.IDMM1.PAR<2:0> then MMU.IDMM1.PAR<2:0> else ps
}

nat TSZ_ST1(va :: VA, EL :: nat, MMU :: MMU_CONFIG1) = 
{
    u = va<63>;
    var tsz = match EL
    {
	case 0 or 1 => if u then MMU.TCR1.T1SZ else MMU.TCR1.T0SZ
	case _      => MMU.TCR23.T0SZ
    };
    when ([tsz]::nat > 39 and !MMUISW.IMPL_TxSZ_BIGGER_THAN_39_FAULT) do tsz <- [0n39]`6; --` max 39
    when ([tsz]::nat < 16 and !MMUISW.IMPL_TxSZ_LESS_THAN_16_FAULT) do tsz <- [0n16]`6; --` min 16
    return [tsz]
}

-- TG field defining translation granule size
-- TODO: define for request
bits(2) TG_ST1(va :: VA, EL :: nat, MMU :: MMU_CONFIG1) = 
{
    u = va<63>;
    tg = match EL
    {
	case 0 or 1    => if u then match MMU.TCR1.TG1         -- translating TG1 to TG0 format
                                         {
					     case '10' => '00'     -- 4K
					     case '01' => '10'     -- 16K
					     case '11' => '01'     -- 64K
 					     case '00' => '11'     -- reserved
                                         } 
                                    else MMU.TCR1.TG0
	case _         => MMU.TCR23.TG0
    };
    return (if TGImplemented(MMU.&IDMM1,tg) then tg else MMUISW.IMPL_TCR_TG_DEFAULT)
}

bool upper (va :: VA, el :: nat, MMU :: MMU_CONFIG1) =
{
    msb = match va<55>, MMU.TCR1.TBI0, MMU.TCR1.TBI1
    {
	case false, false, _ or true, _ , false => va<63>
	case false, true,  _ or true, _ , true  => va<55>
    };
    return msb and (el < 2) 
}

bool tbi (va :: VA, el :: nat, MMU :: MMU_CONFIG1) = match el
{
    case 0 or 1 => if va<55> then MMU.TCR1.TBI1 else MMU.TCR1.TBI0
    case _      => MMU.TCR23.TBI
}

 

------------------------
-- Translation semantics
------------------------

-- looking up first descriptor address from XTBRn_ELx, use epd for EPD bit for Stage 1 and SL0 failure for stage 2 translations
Walk * FaultRecord prepare_initial_lookup (w::Walk, pto::ADR, epd::bool, bt39fault :: bool, lt16fault :: bool, tbi :: bool) = 
{
    var fault = NoFaultfromWalk(w);
    var walk = w;

    -- check input address size according to TCR is in limits 
    TxSZ = 64-w.n;
    TxSZfault = (TxSZ > 39 and bt39fault  or  TxSZ < 16 and lt16fault);

    -- check if input address is not too big, ignore tags according to tbi
    at = if tbi then 56 else 64;
    -- desired prefix modulo tag:
    prefix = match w.up, tbi
    {
        case false, false => nullvec(at-w.n)
	case false, true  => nullvec(at-8-w.n)
	case true,  false => onevec(at-w.n)
	case true,  true  => onevec(at-8-w.n)
    };
    -- actual prefix including tag:
    taprfx = Take(64-w.n, [w.va]::bool list);
    -- actual prefix modulo tag:
    vaprfx = if tbi then Drop(8,taprfx) else taprfx;

    -- if EPD, input address to long, or TTBR size fault -> Translation fault, level 0
    if (TxSZfault or epd or vaprfx != prefix) then fault.typ <- Fault_Translation
    else
    {
	-- compute address of first descriptor
	conc = w.conc and w.l == 4-nlu(w.n, w.br, w.conc);
        px = [px(w.va<47:0>, w.l, w.br, w.n, conc)]::bits(17);
	walk.adr <- pto + (0b0`28 : px : 0b0`3);

	-- check for address size error
	fault <- check_address_size(fault, [walk.adr], w.m)
    };

    when (fault.typ != Fault_None) do walk.l <- 0;
	
    return (walk, fault)
} 

-- valid bit, levels fit typ
bool PBD_invalid (desc::PBDescriptor, n::nat, l::nat, br::nat, conc::bool) = 
{
    valid = match br
    {
	case 9  => desc.v and (desc.pte or l == 1 or l == 2)
	case 11 => desc.v and (desc.pte or l == 2)
	case 13 => desc.v and (desc.pte or l == 2)
	case _  => UNKNOWN
    };
    return !(valid and (l >= 4-nlu(n,br,conc)))
}

-- checks fetched PTE and extends walk by one level, initial TTBR lookup is handled separately
Walk * FaultRecord walk_extend(w::Walk, desc::PBDescriptor, iswicbf::bool, mair :: dword) =
{
    var fault = NoFaultfromWalk(w);
    var walk = w;

    startl = 4-nlu(w.n, w.br, w.conc);

    -- contiguous bit can only cause translation fault in first level of translation
    -- iswicbf = MMUISW.IMPL_CONTIGUOUS_BIT_FAULT
    cont_fault = w.l == startl and iswicbf and desc.Contiguous and cont_bits(w.br,w.l) > nflb(w.n, w.br, w.conc);

    if (PBD_invalid(desc, w.n, w.l, w.br, w.conc) or cont_fault or w.l>3) then  fault.typ <- Fault_Translation   
    else 
    {
	-- Walk Extension

	-- base address of next section
	base = DESC_ADR(desc,w.br,w.l);

	-- page index for next lookup, concatenation only at initial lookup
        px = [px(w.va<47:0>, w.l+1, w.br, w.n, false)]::bits(17);

	-- byte index for page/block
	bx = [bx(w.va<47:0>, w.br, w.l, !desc.pte)]::bits(48);

	-- last level of lookup, either block descriptor or level 3 page descriptor
	lastl = (!desc.pte or w.l == 3);

	-- compute new output address
	if !lastl then walk.adr <- base + (0b0`28 : px : 0b0`3)
	-- read access flag in last level of lookup
	else { walk.adr <- base + bx; walk.A  <- desc.AF }; 

	-- accumulate rights, only in stage 1 or at final lookup
	when (w.st == 1 or lastl) do
	{
	    RO  = if (!lastl) then desc.APT<1> else (if (w.st == 1) then desc.AP<1> else !desc.AP<1>);  -- read-only, inverted for stage 2
	    UM  = if (!lastl) then !desc.APT<0> else (if (w.st == 1) then desc.AP<0> else true); -- user mode, not present in stage 2
	    XN  = if (!lastl) then desc.XNT else desc.XN;
	    PXN = if (!lastl) then desc.PXNT else desc.PXN;   
	    	    
	    walk.W   <- w.W and !RO;
	    walk.U   <- w.U and UM;

	    -- set readable bit in final lookup of stage 2
	    when (w.st == 2) do walk.R <- desc.AP<0>; 

	    walk.XN  <- w.XN or XN;
	    walk.PXN <- w.PXN or PXN
	};

	-- update secure state in case of ST1
	when (w.st == 1) do
	{
	    -- select NS bit or NST
	    nsnext = if lastl then desc.Attr<3> else desc.NST;
	    walk.NS <- w.NS or nsnext
	};
	
	-- update memory attributes of walk at final lookup, different formats in stage 1/2
	when (lastl) do {
	    if (w.st == 1) then walk.attr <- MAIRtoAttr(MAIRbyte(mair,[desc.Attr<2:0>]))
	               else walk.attr <- Stage2toAttr(desc.Attr);
	    walk.attr.shareable <- desc.SH<1>;
	    walk.attr.outershareable <- desc.SH == '10'	    
	};
	 
	-- check for address size fault
	fault <- check_address_size(fault, [w.adr], w.m);

	-- if no fault either increase level or walk complete
	match (fault.typ == Fault_None), lastl {
	    case true, true  => walk.c <- true
	    case true, false => walk.l <- w.l + 1
	    case false, _    => nothing
	}
    };
    return (walk,fault)    
}


-- check resulting address for faults
FaultRecord check_walk_result(w::Walk, size::nat, PTW::bool, WXN :: bool) = 
{
    var fault = NoFaultfromWalk(w);
    -- access flag check
    when (!w.A) do fault.typ <- Fault_AccessFlag;

    -- alignment check for device memory, addresses are always aligned in second stage walk
    when (fault.typ == Fault_None and !Aligned(w.adr,size) and w.attr.typ == MemType_Device and w.acctype != AccType_IFETCH) do
       fault.typ <- Fault_Alignment;
    
    -- if PTW bit is set in HCR_EL2 treat walks on Device memory as stage 2 translation fault
    when (fault.typ == Fault_None and fault.s2fs1walk and PTW and w.attr.typ == MemType_Device) do
       fault.typ <- Fault_Translation;

    -- permission check
    wxn = w.st == 1 and WXN and w.W;   -- WXN makes writable regions execute never in stage 1
    pxn = match w.st
    {
	case 1 => w.PXN or (w.el < 2 and w.U and w.W)   -- memory writable from EL0 is always XN for EL1 in stage 1,
	case _ => w.XN
    };
    xn = wxn or match w.el
    {
	case 1 => pxn
        case _ => w.XN
    };

    -- violation: write to read-only mem, EL0 access if !U or 2nd stage read restriction, fetch of (P)XN
    w2rom = w.wr and !w.W;
    forb = w.st == 1 and (w.el == 0) and !w.U  or !w.wr and !w.R;
    datavio = w2rom or ((w.acctype != AccType_IFETCH) and forb);
    fetchvio = (w.acctype == AccType_IFETCH) and xn;

    when (fault.typ == Fault_None and (datavio or fetchvio)) do fault.typ <- Fault_Permission;

    return fault
}

-- get address descriptor from walk & fault
AddressDescriptor ADfromResult(w::Walk, f::FaultRecord, hcrid :: bool, hcrcd :: bool) = 
{
    var ad;
    ad.paddress <- w.adr;
    ad.fault <- f;
    ad.memattrs <- w.attr;

    -- in stage 2 for EL1&0, set result to non cacheable if CD or ID are active
    when (w.st == 2 and w.el < 2 and w.attr.typ == MemType_Normal and (w.acctype == AccType_IFETCH and hcrid or hcrcd and w.acctype != AccType_IFETCH))
         do ad.memattrs <- set_NC(ad.memattrs);

    return ad
}


--------------------
-- Stage 1 specifics
--------------------


bool Disabled_ST1(MMU :: MMU_CONFIG1) = ( (MMU.HCR1.DC or MMU.HCR1.TGE) and MMU.W1.el < 2 and MMU.W1.NS or !MMU.SCTLr.M )


-- translation table disable bits for stage 1
bool EPD_ST1 (MMU :: MMU_CONFIG1) =
{
    match MMU.W1.el
    {
	case 0 or 1 => if MMU.W1.va<63> then MMU.TCR1.EPD1 else MMU.TCR1.EPD0
	case _      => MMU.TCR23.EPD0
    }
}


-- mapping IPA when ST1 address translation disabled, DC bit from stage 2
Walk FlatMap_ST1 (w :: Walk, sctlrI :: bool, hcrDC :: bool) = 
{
    var walk = w;
    if (w.acctype == AccType_IFETCH) then {
	    rgn = if sctlrI then '10' else '00';
	    walk.attr.typ <- MemType_Normal;
	    walk.attr.inner <- ShortConvertAttrsHints(rgn, true);
	    walk.attr.outer <- ShortConvertAttrsHints(rgn, true);
	    walk.attr.shareable <- true;
	    walk.attr.outershareable <- true
	    }
    else if hcrDC and w.el < 2 and w.NS then {
	    walk.attr.typ <- MemType_Normal;
	    walk.attr.inner <- ShortConvertAttrsHints('11', true);
	    walk.attr.outer <- ShortConvertAttrsHints('11', true);
	    walk.attr.shareable <- false;
	    walk.attr.outershareable <- false
	    }
    else {
    	    walk.attr.typ <- MemType_Device;
	    walk.attr.device <- DeviceType_nGnRnE
	 };

    walk.adr <- w.va<47:0>;
    walk.c <- true;

    return walk
}


MemoryAttributes TCRtoAttr (va :: VA, EL :: nat, ns :: bool, MMU :: MMU_CONFIG1) = 
{
    u = va<63>;
    sh, orgn, irgn = match EL
    {
	case 0 or 1 => if u then MMU.TCR1.SH1, MMU.TCR1.ORGN1, MMU.TCR1.IRGN1 else MMU.TCR1.SH0, MMU.TCR1.ORGN0, MMU.TCR1.IRGN0
	case _      => MMU.TCR23.SH0, MMU.TCR23.ORGN0, MMU.TCR23.IRGN0
    };

    var attr::MemoryAttributes;
    attr.typ <- MemType_Normal;
    attr.outer <- ShortConvertAttrsHints(orgn, MMU.SCTLr.C or MMU.HCR1.DC and ns);
    attr.inner <- ShortConvertAttrsHints(irgn, MMU.SCTLr.C or MMU.HCR1.DC and ns);
    attr.shareable <- sh<1>;
    attr.outershareable <- sh == '10';

    return attr
}


Walk InitWalk_ST1(va :: VA, acctype :: AccType, wr :: bool, EL :: nat, ns :: bool, MMU :: MMU_CONFIG1) = 
   init_walk(va, acctype, wr, EL, 1, TG_ST1(va,EL,MMU), PS_ST1(EL,MMU), TSZ_ST1(va,EL,MMU), TCRtoAttr(va,EL,ns,MMU), PAlen(MMU.IDMM1), false, ns, upper(va,EL,MMU))

-- iswtmtaz = MMUISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO
ADR PTO_ST1 (w :: Walk, pto :: dword, iswtmtaz :: bool) = 
{
    return page_table_origin(nflb(w.n,w.br,w.conc),pto,iswtmtaz)
}


---------------------
-- Atomic transitions
---------------------

-- r = CURR_REQ
MMU_CONFIG1 MMU_Init_ST1 (r :: MEM_REQUEST, mmu :: MMU_CONFIG1) = 
{
    var MMU = mmu;
    when (MMU.ST1 != wait) do #MMU_SCHED_FAULT;

    -- choose correct write and el parameters for AT instruction
    write, el = match r.acctype, r.sopar {
	case AccType_AT, ATrans(_,_,EL,wr) => wr, EL
	case _, _ => r.write, r.EL
    };

    MMU.W1 <- InitWalk_ST1(r.va, r.acctype, write, el, r.ns, MMU);
    MMU.F1 <- NoFaultfromWalk(MMU.W1);
    MMU.SZ1 <- r.bytesize;
    
    MMU.ST1 <- init;
    return MMU
}

-- Flat map transition when MMU disabled
MMU_CONFIG1 MMU_FlatMap_ST1 (mmu :: MMU_CONFIG1) = 
{
    var MMU = mmu;
    when (MMU.ST1 != init) do #MMU_SCHED_FAULT;

    -- modify relevant parameters
    MMU.W1.n <- 48;
    MMU.W1.m <- PAlen(MMU.IDMM1);
    MMU.W1.l <- 0;

    -- flat map, check if input address size > implemented physical address
    MMU.W1 <- FlatMap_ST1(MMU.W1,MMU.SCTLr.I,MMU.HCR1.DC);
    MMU.F1 <- check_address_size(NoFaultfromWalk(MMU.W1), MMU.W1.va, PAlen(MMU.IDMM1));

    MMU.DESC1 <- ADfromResult(MMU.W1,MMU.F1,MMU.HCR1.ID,MMU.HCR1.CD);

    MMU.ST1 <- final;
    return MMU
}

-- Walk init and preparation of initial TTBR lookup
MMU_CONFIG1 MMU_StartWalk_ST1 (mmu :: MMU_CONFIG1) = 
{
    var MMU = mmu;
    when (MMU.ST1 != init) do #MMU_SCHED_FAULT;

    (w,f) = prepare_initial_lookup(MMU.W1, PTO_ST1(MMU.W1,MMU.&TTBR,MMUISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO), EPD_ST1(MMU), MMUISW.IMPL_TxSZ_BIGGER_THAN_39_FAULT, MMUISW.IMPL_TxSZ_LESS_THAN_16_FAULT, tbi(MMU.W1.va, MMU.W1.el, MMU));
    MMU.W1 <- w;
    MMU.F1 <- f;
    if (f.typ == Fault_None) then
    {
	MMU.DESC1 <- ADfromWalk(MMU.W1);
	MMU.ST1 <- mem
    }
    else 
    {
	MMU.DESC1 <- ADfromResult(MMU.W1,MMU.F1,MMU.HCR1.ID,MMU.HCR1.CD);
	MMU.ST1 <- final
    };
    return MMU
}

-- request descriptor from memory
MEM_REQUEST * MMU_CONFIG1 MMU_FetchDescriptor_ST1 (curr_req :: MEM_REQUEST, mmu :: MMU_CONFIG1) = 
{
    var MMU = mmu;
    when (MMU.ST1 != mem) do #MMU_SCHED_FAULT;

    var req :: MEM_REQUEST;

    req.core_id <- curr_req.core_id;
    req.id <- curr_req.id;
    req.write <- false;
    req.desc <- MMU.DESC1;
    req.bytesize <- 8;
    req.acctype <- AccType_PTW;
    -- reset descriptor fault record (maybe updated by ST2)
    req.desc.fault.secondstage <- false;
    req.desc.fault.s2fs1walk <- false;
    req.desc.fault.level <- MMU.W1.l;
    
    MMU.ST1 <- walk;

    return (req, MMU)
}

MMU_CONFIG1 MMU_ExtendWalk_ST1 (r :: MEM_REQUEST, mmu :: MMU_CONFIG1) = 
{
    var MMU = mmu;
    when (MMU.ST1 != walk) do #MMU_SCHED_FAULT;

    MMU.F1 <- r.desc.fault;
    when (MMU.F1.typ == Fault_None) do {
	desc = cast_desc([r.data]);
	(w, f) = walk_extend(MMU.W1, desc, MMUISW.IMPL_CONTIGUOUS_BIT_FAULT, MMU.MAIr);
	MMU.W1 <- w;
	MMU.F1 <- f;
	when (f.typ == Fault_None and !w.c) do {
   	    MMU.DESC1 <- ADfromWalk(w);
	    MMU.ST1 <- mem
	};
	when (f.typ == Fault_None and w.c) do {
	    MMU.F1 <- check_walk_result(w, MMU.SZ1, MMU.HCR1.PTW, MMU.SCTLr.WXN)
	}
    };
    when (MMU.F1.typ != Fault_None or MMU.W1.c) do {
	-- if a fault from the second stage was received than the result was already computed by ST2 in the returned request
	MMU.DESC1 <- if (MMU.F1.typ != Fault_None and MMU.F1.secondstage) then r.desc else ADfromResult(MMU.W1,MMU.F1,MMU.HCR1.ID,MMU.HCR1.CD);
	MMU.ST1 <- final	
    };
    return MMU
}


MEM_REQUEST * MMU_CONFIG1 MMU_Complete_ST1 (curr_req :: MEM_REQUEST, mmu :: MMU_CONFIG1) = 
{
    var MMU = mmu;
    var r = curr_req;
    r.desc <- MMU.DESC1;
    
    MMU.ST1 <- wait;

    return (r, MMU)
}


---------------------------------
-- MMU ST1 automaton
---------------------------------


-- translation automaton for Stage 1
(MEM_REQUEST option) * (MEM_REQUEST option) * bool * MMU_CONFIG1
MMU_ST1_sched (req :: MEM_REQUEST, lookup :: MEM_REQUEST option, mmu :: MMU_CONFIG1) = 
{
    var MMU = mmu;
    l = get_memreq(lookup);
    var out_mem = None;
    var out_mmu = None;
    var consumed = false;

    
    when !(MMU.ST1 == walk and lookup == None) do
    {
	match MMU.ST1
	{
	    case wait => {MMU <- MMU_Init_ST1(req,MMU); consumed <- true}
	    case init => if Disabled_ST1(MMU) then MMU <- MMU_FlatMap_ST1(MMU) else MMU <- MMU_StartWalk_ST1(MMU)
	    case mem  => {(omem, mmu_new) = MMU_FetchDescriptor_ST1(req,MMU); out_mem <- Some(omem); MMU <- mmu_new}
	    case walk => {MMU <- MMU_ExtendWalk_ST1(l,MMU); consumed <- true}
	    case final => {(res, mmu_new) = MMU_Complete_ST1(req,MMU); MMU <- mmu_new;
			   if res.desc.fault.typ == Fault_None and res.acctype notin set {AccType_AT, AccType_TLB} then 
			       out_mem <- Some(res)
			   else
			       out_mmu <- Some(res)}
	}
    };

    return (out_mmu, out_mem, consumed, MMU)
}
