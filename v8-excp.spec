-----------------------------------------------------------------------
-- This is a formal specification of 				     --
-- ARMv8 exception and interrupt handling			     --
-- 								     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2014-11-05	move to separate files


{-

val () = Runtime.LoadF "v8-base.spec, v8-mem.spec, v8-mmu.spec, v8-virt.spec, v8-excp.spec";

-}

---------------------------------------
-- System registers
---------------------------------------


-- exception status register
register ESRType :: word
{
31-26 : EC      -- Exception class
   25 : IL      -- instruction length for synchronous exceptions (0 - 16 bit, 1 - 32 bit and other faults/errors/aborts)
24-0  : ISS     -- instruction specific syndrome
}

declare
{
   ESR_EL1   :: ESRType
   ESR_EL2   :: ESRType
   ESR_EL3   :: ESRType
   FAR_EL1   :: dword
   FAR_EL2   :: dword
   FAR_EL3   :: dword
-- TODO: need AFSRx?
}



ESRType ESR =
{
   regime = TranslationRegime;
   match regime
   {
      case 1 => ESR_EL1
      case 2 => ESR_EL2
      case 3 => ESR_EL3
      case 0 => UNKNOWN -- unreachable
   }
}

dword FAR =
{
   regime = TranslationRegime;
   match regime
   {
      case 1 => FAR_EL1
      case 2 => FAR_EL2
      case 3 => FAR_EL3
      case 0 => UNKNOWN -- unreachable
   }
}


