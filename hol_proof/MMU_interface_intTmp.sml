open HolKernel boolLib bossLib Parse;
open lcsymtacs;

open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open pairLib;
open pairTools;

open armCombineTheory;
open invariantsIntTheory;

open MMU_sched_intTheory;

(*
HOL_Interactive.toggle_quietdec();
open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open pairLib;
open pairTools;

open armCombineTheory;
open invariantsIntTheory;

open MMU_sched_intTheory;

HOL_Interactive.toggle_quietdec();
*)

val _ = new_theory "MMU_interface_int";

val mmu_interface_int_def = ``
(!r l m ommu omem cons m' s'.
  (((ommu, omem, cons, m'), s') = MMU_ST1_sched(r,l,m) ARB) ==>
  (
   ((m'.ST1 = walk) = ((THE omem).acctype = AccType_PTW)) /\
   (((THE omem).acctype <> AccType_PTW) ==>
    ((r.acctype = (THE omem).acctype) /\
     (r.write = (THE omem).write)
   )) /\
   (((THE omem).acctype = AccType_PTW) ==> ¬(THE omem).write) /\
   (s' = ARB)
 )
) ==>
(m.CURR_REQ.EL < 2 /\ m.CURR_REQ.ns) ==>
(mmu_int_invariant mmu_state policy policy_pt) ==>
(* This part depends on the configuration of pts *)
(!w f walk .
(lookup <> NONE) ==>
(
 ((w,f) =walk_extend
         (walk,cast_desc (v2w (THE lookup).data) ARB,
          ARB.MMUISW.IMPL_CONTIGUOUS_BIT_FAULT, ARB) ARB) ==>
 ((w.acctype = walk.acctype) /\
  (w.wr = walk.wr) /\
    (
    (w.c) ==>
    (walk.wr) ==>
    ((check_walk_result (w,8,mmu_state.HCR2.PTW,ARB) ARB).typ = Fault_None) ==>
    (¬policy w.adr)
  ) /\
  (
    (~w.c) ==>
    ((w.st = 2) ∧ (w.n = 32) ∧ (policy_pt w.adr))
  )
 ))
(* We must know something of w if the translation failed *)
 ) ==>
(m.MMU2 = mmu_state) ==>
((icorerPre,icoreRPre) = get_mmureq req ARB) ==>
((((icorerPre.acctype = AccType_DC) /\
  (?v16 v20 v25. icorerPre.sopar = DFlush (v16,v20,T,v25))) \/
 ((icorerPre.acctype = AccType_IC) /\
  (?v41. icorerPre.sopar = IFlush (T,v41))) \/
 (icorerPre.acctype = AccType_MB)) ==> (~icorerPre.write)) ==>
(icorerPre.EL < 2 ∧ icorerPre.ns) ==>
(* Since we can receive the new MMU registers *)
((req <> NONE) ==>
 !x1 x2.
 ((x1,x2) = THE req) ==>
(
 (x2.vtcr.T0SZ = 32w) ∧
 (x2.vtcr.TG0 = 0w) ∧ (x2.vtcr.SL0 = 1w) ∧
 ((rec'mm_feat (reg'mm_feat x2.idmm)).TGRAN4 = 0w) ∧
 (x2.hcr.DC (* \/ x2.hcr.VM *)) ∧
∃ASID' BADDR'.
  (∀b.
     policy_pt
       (8w * (3w && b) +
        if ARB.MMUISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO then
          align (BADDR',5)
        else BADDR')) ∧
  (∀ia: 17 word.
     policy_pt
       ((0w:word28 @@ ((ia @@ (0w:word3)):word20)) +
        if ARB.MMUISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO then
          align (BADDR',5)
        else BADDR'))
  ∧ (x2.vttbr = TTBRType ASID' BADDR')
)) ==>
(* Invariant on states *)
((m.S = mmu_wait) ==> (m.MMU2.ST2 = wait)) ==>
let s' = mmu_interface (req,lookup,m) ARB in
let ((core2mmu, mem2mmu, mmu2core, mmu2mem, m'), s'') = s' in
let goal =  
( 
 (mmu_int_invariant m'.MMU2 policy policy_pt)  /\  
 (mmu2mem_policy mmu2mem policy) /\
 (m'.CURR_REQ.EL < 2 /\ m'.CURR_REQ.ns)
) in
goal``;

val mmu_interface_int_thm = store_thm(
    "mmu_interface_int_thm",
    ``^mmu_interface_int_def``,
  (srw_tac [] [])
  \\ (full_simp_tac (srw_ss()) [mmu_interface_def])
  \\ LET_NORM_TAC
  (* use the existing variables *)
  \\ (rev_full_simp_tac (srw_ss()) [])
  (* remove unused variabled *)
  \\ (rw_tac (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR
  
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR
  \\ RM_ABBR_CONST

  \\ LET_NORM_TAC
  \\ (rpt FIX_ABBREV_TAC)
  \\ LET_NORM_TAC
  \\ (rpt FIX_ABBREV_TAC)
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR
  \\ RM_ABBR_CONST
  \\ RM_ABBR_CONST
  \\ (rpt FIX_ABBREV_TAC)
  \\ (FULL_SIMP_TAC (srw_ss()) [])

  \\ LET_NORM_TAC

  \\ (TERM_PAT_ASSUM_W_THM ``Abbrev(a = if b then c else d)`` (fn a::b::c =>
     (Q.ABBREV_TAC `cnd = ^b`)))
  \\ (Cases_on `~cnd`)

  THENL [
        (fs []) \\ (DEABBREV_AND_FIX_TAC) \\ (fs [mmu2mem_policy_def]),
        ALL_TAC
  ]

  \\ (FULL_SIMP_TAC (srw_ss()) [])

  \\ (Cases_on `m.S = mmu_reply`)

  THENL [
        (fs []) \\ (DEABBREV_AND_FIX_TAC) \\ (fs [])
        \\ (Cases_on `lookup ≠ NONE`)
        THENL [(fs [mmu2mem_policy_def]), (fs [mmu2mem_policy_def])],
        ALL_TAC
  ]

  \\ (Cases_on `m.S = mmu_trans`)

  THENL [
        (fs[])
        \\ (Q.ABBREV_TAC `s' = (MMU_sched (req,lookup,m) ARB)`)
        \\ (LET_NORM_TAC)

        \\ (TERM_PAT_ASSUM_W_THM ``Abbrev (a = MMU_sched (b,c,d) e)``  (fn [a,b,c,d,e] =>
                ASSUME_TAC (
                SPECL [b,c,d,``m.MMU2``] (
                GENL [``req: (MEM_REQUEST # MMU_REGS) option``, ``lookup: MEM_REQUEST option``,
                      ``m:MMU_CONFIG``, ``mmu_state:MMU_CONFIG2``
                             ] MMU_sched_int_thm)
           )))

        \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
        
        \\ (TERM_PAT_ASSUM_W_THM ``a==>b`` (fn [a,b] =>
           (Q.ABBREV_TAC `pre=^a`)))
        \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

        \\ (TERM_PAT_ASSUM_W_THM ``a==>b`` (fn [a,b] =>
           (Q.ABBREV_TAC `pre1=^a`)))
        \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

        \\ (fs [])
        \\ (RM_ABBR_VAR)
        \\ (DEABBREV_AND_FIX_TAC)
        \\ (fs [])
        \\ (Q.ABBREV_TAC `cnd1 = (s'3.MMU1.ST1 = wait) ∧ (s'3.MMU2.ST2 = wait)`)
        \\ (Cases_on `cnd1`)
        THENL [
              (fs [])
              \\ (Cases_on `s'2`)
              THENL [
                    (fs[])
                    \\ (Cases_on `s'1 ≠ NONE`)
                    THENL [
                          (fs[]),
                          (fs[])
                    ],
                    (fs[])
                    \\ (Cases_on `s'1 ≠ NONE`)
                    THENL [
                          (fs[]),
                          (fs[])
                    ]
             ],
             (fs[])
             \\ (Cases_on `s'2`)
             THENL [
                   (fs[]),
                   (fs[])
             ]
        ],

        ALL_TAC
  ]

  \\ (Cases_on `m.S`)
  THENL [
        ALL_TAC,
        (fs[]),
        (fs[])
  ]
  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (Cases_on `tlb`)
  THENL [
        (fs[]) \\ DEABBREV_AND_FIX_TAC \\ (fs [mmu2mem_policy_def]),
        ALL_TAC
  ]

  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (Cases_on `fw`)

  THENL [
        (fs [])
        \\ (Q.ABBREV_TAC `pre = 
           ((icorer.acctype = AccType_DC) /\
            (?v16 v20 v25. icorer.sopar = DFlush (v16,v20,T,v25))) \/
            ((icorer.acctype = AccType_IC) /\
            (?v41. icorer.sopar = IFlush (T,v41))) \/
            (icorer.acctype = AccType_MB)
           `)
        \\ (`pre` by ALL_TAC)
        THENL [
              (Cases_on `icorer.acctype`)
              \\ (DEABBREV_AND_FIX_TAC \\ (fs []))
              THENL [
                    (Cases_on `icorer.sopar`)
                    \\ (fs [])
                    \\ (pairLib.PairCases_on `p`)
                    \\ (fs[])
                    \\ (Cases_on `p2`)
                    \\ (fs[]),
                    ALL_TAC, ALL_TAC, ALL_TAC
              ]
              THENL [
                    (Cases_on `icorer.sopar`)
                    \\ (fs [])
                    \\ (pairLib.PairCases_on `p`)
                    \\ (fs[])
                    \\ (Cases_on `p2`)
                    \\ (fs[]),
                    ALL_TAC, ALL_TAC
              ]
              THENL [
                    (Cases_on `icorer.sopar`)
                    \\ (fs [])
                    \\ (pairLib.PairCases_on `p`)
                    \\ (fs[])
                    \\ (Cases_on `p0`)
                    \\ (fs[]),
                    ALL_TAC
              ]
              \\ (Cases_on `icorer.sopar`)
              \\ (fs [])
              \\ (pairLib.PairCases_on `p`)
              \\ (fs[])
              \\ (Cases_on `p0`)
              \\ (fs[]),

              ALL_TAC
            ]
        \\ (`icorerPre = icorer` by ALL_TAC)
        THENL [
                DEABBREV_AND_FIX_TAC
                \\ (fs[]),
                ALL_TAC
        ]
        \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
        \\ (fs[]) \\ DEABBREV_AND_FIX_TAC \\ (fs [mmu2mem_policy_def]),

        ALL_TAC
  ]
  (* This is done, but we now we must check the real case, i.e. we
     are not directly orwarding the action to the memory *)
  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (Cases_on `req = NONE`)
  THENL [
        (fs[]) \\ DEABBREV_AND_FIX_TAC \\ (fs [mmu2mem_policy_def])
        \\ (REV_FULL_SIMP_TAC (srw_ss()) []),
        ALL_TAC
  ]

  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (fs [init_mmu_regs_def])

  \\ (Q.ABBREV_TAC `ns=init_mmu_regs (m,THE req) ARB`)
  \\ (FULL_SIMP_TAC (srw_ss()) [init_mmu_regs_def])
  \\ (rpt LET_NORM_TAC)
  \\ (RM_ABBR_VAR)

  (* RM abbrev with *)
  \\ (EVERY_ASSUM (fn thm =>
     if (not o markerSyntax.is_abbrev o concl) thm then ALL_TAC
     else 
     let val (var, trm) = markerSyntax.dest_abbrev (concl thm)
         val var = (fst o dest_eq o snd o dest_comb o concl) thm
     in
         if (not o is_var) var then ALL_TAC
         else if (not o is_comb) trm then ALL_TAC
         else if (not o TypeBase.is_record_type o type_of) trm then ALL_TAC
         else if (
              (List.exists (fn x =>
                           concat [(fst o dest_type o type_of) trm, "_", x, "_fupd"] =
                           ((fst o dest_const o fst o strip_comb) trm))) o
              (List.map fst) o TypeBase.fields_of o type_of) trm then
              TRY (FULL_SIMP_TAC (srw_ss()) [Abbr `^var`])
         else ALL_TAC
     end
     ))
   \\ (RM_ABBR_VAR)
  \\ (EVERY_ASSUM (fn thm =>
     if (not o markerSyntax.is_abbrev o concl) thm then ALL_TAC
     else 
     let val (var, trm) = markerSyntax.dest_abbrev (concl thm)
         val var = (fst o dest_eq o snd o dest_comb o concl) thm
     in
         if (not o is_var) var then ALL_TAC
         else if (not o is_comb) trm then ALL_TAC
         else if (not o TypeBase.is_record_type o type_of) trm then ALL_TAC
         else if (
              (List.exists (fn x =>
                           concat [(fst o dest_type o type_of) trm, "_", x, "_fupd"] =
                           ((fst o dest_const o fst o strip_comb) trm))) o
              (List.map fst) o TypeBase.fields_of o type_of) trm then
              TRY (FULL_SIMP_TAC (srw_ss()) [Abbr `^var`])
         else ALL_TAC
     end
     ))
   \\ (NEW_RM_ABBR_VAR)

   \\ (Q.ABBREV_TAC `ns1 = (MMU_sched
                                 (req,lookup,
                                  s0''''''''''''' with
                                  <|CURR_REQ := icorer;
                                    MMU2 :=
                                      s0'''''''''''''.MMU2 with
                                      HCR2 := s0'''''''''''''.MMU1.HCR1;
                                    S := mmu_trans|>) s1'')`)
   \\ LET_NORM_TAC
   \\ (RM_ABBR_VAR)
   \\ (fs [])

   \\ (TERM_PAT_ASSUM_W_THM ``Abbrev (a = MMU_sched (b,c,d) e)``  (fn [a,b,c,d,e] =>
           ASSUME_TAC (
           SPECL [b,c,d,``s0'''''''''''''.MMU2 with HCR2 := s0'''''''''''''.MMU1.HCR1``] (
           GENL [``req: (MEM_REQUEST # MMU_REGS) option``, ``lookup: MEM_REQUEST option``,
                 ``m:MMU_CONFIG``, ``mmu_state:MMU_CONFIG2``
                        ] MMU_sched_int_thm)
      )))

   \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
   \\ (` s1'' = ARB` by ALL_TAC)
   THENL [
         DEABBREV_AND_FIX_TAC \\ (fs[]),
         ALL_TAC
   ]
   \\ (fs[])
   \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

   \\ (TERM_PAT_ASSUM_W_THM ``a==>b`` (fn [a,b] =>
      (Q.ABBREV_TAC `pre=^a`)))
   \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

   \\ (TERM_PAT_ASSUM_W_THM ``a==>b`` (fn [a,b] =>
      (Q.ABBREV_TAC `pre1=^a`)))

  (* OK now we are here *)

   \\ (`pre1` by ALL_TAC)
   THENL [
         DEABBREV_AND_FIX_TAC
         \\ (fs[])
         \\ (Cases_on `at`)
         THENL [
               (fs[])
               \\ (Cases_on `st2`)
               THENL [
                     (fs[mmu_int_invariant_def, Disabled_ST2_def]),
                     (fs[mmu_int_invariant_def, Disabled_ST2_def])
                     \\ (RW_TAC (srw_ss()) [])
                     \\ cheat]
               ],
               (fs[mmu_int_invariant_def, Disabled_ST2_def])
         ],
         ALL_TAC
   ]

   \\ (fs [])

   \\ (`s0'''''''''''''.MMU1.HCR1.PTW = m.MMU2.HCR2.PTW` by cheat)
   \\ (fs[])
   \\ (rev_full_simp_tac (srw_ss()) [])


   \\ (TERM_PAT_ASSUM_W_THM ``a==>b`` (fn [a,b] =>
      (Q.ABBREV_TAC `pre3=^a`)))
   \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

   (* I've established the invariant *)
   \\ DEABBREV_AND_FIX_TAC
   \\ (fs[])
   \\ (Cases_on `ns12`)
   THENL [
         (fs []),
         (fs [])
   ]
);

val _ = export_theory();
