open HolKernel boolLib bossLib Parse;
open lcsymtacs;

open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open armCombineTheory;
open invariantsIntTheory;

(*

HOL_Interactive.toggle_quietdec();
open lcsymtacs;
open dharma8Theory;
open wordsLib;
open guancioTacticsLib;
open guancioTactics2Lib;
open armCombineTheory;
open invariantsIntTheory;
HOL_Interactive.toggle_quietdec();

*)

val _ = new_theory "MMU_Init_ST2_int";

val disabled_st2_thm = prove(``
    !mmu_state mmu_state'.
    (mmu_state.HCR2.DC = mmu_state'.HCR2.DC) ==>
    (mmu_state.HCR2.VM = mmu_state'.HCR2.VM) ==>
    (Disabled_ST2 mmu_state = Disabled_ST2 mmu_state')
``,
  RW_TAC (srw_ss())  [Disabled_ST2_def]);

val MMU_Init_ST2_int_def = ``
(mmu_state.ST2 = wait) ==>
(mmu_int_invariant mmu_state policy policy_pt) ==>
(ptw = (mmu_state.MEMREQ.acctype = AccType_PTW)) ==>
  (* so except if the MMU1 is performing a page walk, *)
  (* curr_req and mmu_state.MEMREQ access types are in sync *)
  (* Notice that mmu_state.MEMREQ is the current MMU1 request and it *)
  (* is not the initial request of the core, that is passed as parameter *)
  (* as curr_req *)
(((mmu_state.MEMREQ.acctype <> AccType_PTW)) ==>
 ((curr_req.acctype = mmu_state.MEMREQ.acctype) /\
  (curr_req.write = mmu_state.MEMREQ.write)
 )) ==>
let (mmu_state, s) = MMU_Init_ST2 (ptw,ipaddress,curr_req,mmu_state) ARB in
(mmu_int_invariant mmu_state policy policy_pt)
``;

val MMU_Init_ST2_int_thm = store_thm(
    "MMU_Init_ST2_int_thm",
    ``^MMU_Init_ST2_int_def``,
  (srw_tac [] [])
  \\ (RULE_ASSUM_TAC (SIMP_RULE (srw_ss()) [mmu_int_invariant_def]))
  \\ (FULL_SIMP_TAC (srw_ss()) [MMU_Init_ST2_def])
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RW_TAC (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])
  \\ LET_NORM_TAC

  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])

  \\ LET_NORM_TAC
  \\ (fs [InitWalk_ST2_def, init_walk_def])
  \\ (rpt FIX_ABBREV_TAC)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`, Abbr `s10`, Abbr `mmu_state'`])

  \\ (FULL_SIMP_TAC (srw_ss()) [TSZ_ST2_def, LET_DEF])

  \\ (FULL_SIMP_TAC (srw_ss()) [VAlen_def])

  \\ (fs [TG_ST2_def, TGImplemented_def])
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

  \\ (fs [gran_bits_def,
          EVAL_RULE (SIMP_CONV (srw_ss()) [nlu_def] ``nlu (32,9,F)``),
          EVAL_RULE (SIMP_CONV (srw_ss()) [concatenated_def] ``concatenated (1,32,9)``)
          ])
  \\ (DEABBREV_AND_FIX_TAC)
  \\ (fs [mmu_int_invariant_def, mmu_intermediate_walk_inv_def])
  \\ (RW_TAC (srw_ss()) [])
  THENL [
    (* Show that the policy_pt is respected when the 1 stage is accessing a PT *)
    (fs [gran_bits_def,
          EVAL_RULE (SIMP_CONV (srw_ss()) [nlu_def] ``nlu (32,9,F)``),
          EVAL_RULE (SIMP_CONV (srw_ss()) [concatenated_def] ``concatenated (1,32,9)``),
          px_def,
          PTO_ST2_def, page_table_origin_def, ADfromWalk_def,
          EVAL_RULE (SIMP_CONV (srw_ss()) [nflb_def, nlu_def] ``nflb (32,9,F)``)
          ])
    \\ (fs [reg'TTBRType_def, rec'TTBRType_def])
    \\ (fs [blastLib.BBLAST_PROVE ``(((47 :num) >< (0 :num)) (((ASID:word16) @@ BADDR) :word64) :word48) = BADDR:word48``]),
    ALL_TAC,
    ALL_TAC,
    ALL_TAC,
    ALL_TAC]

 THENL [
    (* Show that the policy_pt is respected when the 1 stage is not accessing a PT *)
    (fs [gran_bits_def,
          EVAL_RULE (SIMP_CONV (srw_ss()) [nlu_def] ``nlu (32,9,F)``),
          EVAL_RULE (SIMP_CONV (srw_ss()) [concatenated_def] ``concatenated (1,32,9)``),
          px_def,
          PTO_ST2_def, page_table_origin_def, ADfromWalk_def,
          EVAL_RULE (SIMP_CONV (srw_ss()) [nflb_def, nlu_def] ``nflb (32,9,F)``)
          ])
    \\ (fs [reg'TTBRType_def, rec'TTBRType_def])
    \\ (fs [blastLib.BBLAST_PROVE ``(((47 :num) >< (0 :num)) (((ASID:word16) @@ BADDR) :word64) :word48) = BADDR:word48``]),
    ALL_TAC,
    ALL_TAC,
    ALL_TAC]

 THENL [
   ALL_TAC,
   (fs [Disabled_ST2_def]),
   (fs [Disabled_ST2_def])
  ]

  \\ (Cases_on `mmu_state.MEMREQ.acctype ≠ AccType_PTW`)
  THENL [
        (fs []),
        (fs [])
  ]
);

val _ = export_theory();
