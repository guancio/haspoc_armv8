(* HOL_Interactive.toggle_quietdec(); *)
open HolKernel boolLib bossLib Parse;

open dharma8Theory;
(* load "wordsLib"; *)
open wordsLib;
(* HOL_Interactive.toggle_quietdec(); *)

val _ = new_theory "armCombine";

val mem_step_def = Define `
mem_step (m_state, thr2mem, mem2thr) =
  let (MC,TZC,PM) = m_state in
  let ((used_msg, rpl, rcvr, MC, PM), no_state) = mem_interface(thr2mem,nofault,MC,TZC,PM) ARB in
  let thr2mem = if (used_msg <> NONE) then NONE else thr2mem in
  let mem2thr = if (rpl <> NONE) then rpl else mem2thr in
  ((MC,TZC,PM), thr2mem, mem2thr)
`;

val core_step_def = Define `
core_step (c_state, thr2core, core2thr) =
  let ((used_msg, rpl), c_state) = core_interface thr2core c_state in
  let thr2core = if (used_msg <> NONE) then NONE else thr2core in
  let core2thr = if (rpl <> NONE) then rpl else core2thr in
  (c_state, thr2core, core2thr)
`;

val mmu_step_def = Define `
mmu_step (mmu_state, core2mmu, mem2mmu, mmu2core, mmu2mem) =
  let ((used_core, used_mem, rpl2core, rpl2mem, mmu_state), no_state) = mmu_interface(core2mmu,mem2mmu,mmu_state) ARB in
  let core2mmu  = if (used_core <> NONE) then NONE else core2mmu in
  let mem2mmu  = if (used_mem <> NONE) then NONE else mem2mmu in
  let mmu2core  = if (rpl2core <> NONE) then rpl2core else mmu2core in
  let mmu2mem  = if (rpl2mem <> NONE) then rpl2mem else mmu2mem in
  (mmu_state, core2mmu, mem2mmu, mmu2core, mmu2mem)
`;

val thr_step_def = Define `
thr_step ((c_state, mmu_state, core2mmu, mmu2core), mem2thr, thr2mem) =
  (* or the MEM is sending a message to the mmu that must be forwarded to the core *)
  if (mmu_state.S = mmu_wait) then
     let (c_state, mmu2core, core2mmu) = core_step (c_state, mmu2core, core2mmu) in
     ((c_state, mmu_state, core2mmu, mmu2core), mem2thr, thr2mem)
  else
     let (mmu_state, core2mmu, mem2thr, mmu2core, thr2mem) = mmu_step (mmu_state, core2mmu, mem2thr, mmu2core, thr2mem) in
     ((c_state, mmu_state, core2mmu, mmu2core), mem2thr, thr2mem)
`;


val sys_step_def = Define `
sys_step ((c_state, mmu_state, core2mmu, mmu2core), mem_state, mem2thr, thr2mem) =
  let (MC,TZC,PM) = mem_state in
  if (MC.S = mem_wait) /\ (thr2mem = NONE) then
     let ((c_state, mmu_state, core2mmu, mmu2core), mem2thr, thr2mem) =
         thr_step ((c_state, mmu_state, core2mmu, mmu2core), mem2thr, thr2mem)
     in
     ((c_state, mmu_state, core2mmu, mmu2core), mem_state, mem2thr, thr2mem)
  else
     let (mem_state, thr2mem, mem2thr) = mem_step (mem_state, thr2mem, mem2thr)
     in
     ((c_state, mmu_state, core2mmu, mmu2core), mem_state, mem2thr, thr2mem)
`;

val _ = export_theory();
