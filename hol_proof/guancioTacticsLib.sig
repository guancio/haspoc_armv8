signature guancioTacticsLib =
sig

include Abbrev

val my_match : term -> term -> term list
val FIX_TUPLE_EQ_TAC : tactic
val SPLIT_ABBREVS : tactic
val proof_free_vars :  (term list -> tactic) -> term list * term -> goal list * validation
val LET_ASS_TAC : tactic
val LET_EQ_ASS_TAC : tactic
val LET_EQ2_ASS_TAC : tactic
val FIX_ABBREV_TAC : tactic
val FIX_ABBREV_TAC2 : tactic
val FIX_ABBREV_TAC3 : tactic
val DEABBREV_AND_FIX_TAC : tactic
val DEABBREV_AND_FIX_TUPLE_TAC : tactic
val LET_X_TO_ONE_TAC : term -> thm -> tactic
val LET_TUPLE_TO_TUPLE_TAC : term -> thm -> tactic
val LET_ONE_TO_TUPLE_TAC : term -> thm -> tactic
val SIMPLE_LET_TAC : tactic
val SIMPLE_LET_EQ_ASS_TAC : tactic
val PAT_UNDISCH : term -> tactic
val PROVE_BY_BLAST : (term list * term, thm) gentactic
val ABBREV_GOAL : term list * term -> goal list * validation
val INVERT_ABBR_LET : tactic
val INVERT_LET_EQ : tactic
val SIMP_BY_PAT : term -> thm list -> tactic
val NO_SPLIT_TAC : tactic
val TERM_PAT_ASSUM : term -> (term list -> tactic) -> tactic
val TERM_PAT_ASSUM_W_THM : term -> (term list -> tactic) -> tactic
val SPECL_ASSUM : term -> term list -> tactic
val SYM_ASSUM : term -> tactic
val MY_LET_NORMAL_FORM : tactic
val MY_LET_NORMAL_FORM2 : tactic
val MY_LET_AND_EXEC2 : thm list -> tactic
val FST : tactic
val FIX_CONST_EQ : tactic

end
