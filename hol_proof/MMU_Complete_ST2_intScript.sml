open HolKernel boolLib bossLib Parse;
open lcsymtacs;

open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open armCombineTheory;
open invariantsIntTheory;

(*
HOL_Interactive.toggle_quietdec();
open lcsymtacs;
open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open armCombineTheory;
open invariantsIntTheory;
HOL_Interactive.toggle_quietdec();
*)

val _ = new_theory "MMU_Complete_ST2_int";

val MMU_Complete_ST2_integrity_def = ``
(mmu_state.ST2 = final) ==>
(mmu_int_invariant mmu_state policy policy_pt) ==>
let s' = MMU_Complete_ST2 (desc1,mmu_state) ARB in
let (mmu2mem, mmu_state) = s' in
let goal =
 (mmu_int_invariant mmu_state policy policy_pt) /\
 (mmu2mem_policy mmu2mem policy) in
 goal
``;

val MMU_Complete_ST2_integrity_thm = store_thm("MMU_Complete_ST2_integrity_thm",
  ``^MMU_Complete_ST2_integrity_def``,
  (srw_tac [] [])
  \\ (RULE_ASSUM_TAC (SIMP_RULE (srw_ss()) [mmu_int_invariant_def]))
  \\ (FULL_SIMP_TAC (srw_ss()) [MMU_Complete_ST2_def])
  \\ (LET_NORM_TAC)
  \\ (LET_NORM_TAC)
  \\ (LET_NORM_TAC)
  \\ (LET_NORM_TAC)
  \\ (RM_ABBR_CONST)
  \\ (RM_ABBR_VAR)

  \\ (LET_NORM_TAC)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s`])

  \\ (LET_NORM_TAC)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s`])

  \\ (LET_NORM_TAC)
  \\ (rpt FIX_ABBREV_TAC)
  \\ (LET_NORM_TAC)
  \\ (RM_ABBR_VAR)

  \\ (LET_NORM_TAC)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0`])

  \\ (LET_NORM_TAC)
  \\ (rpt FIX_ABBREV_TAC)
  \\ (LET_NORM_TAC)
  \\ (RM_ABBR_VAR)

  \\ (TERM_PAT_ASSUM_W_THM ``Abbrev(a = if b then c else d)`` (fn a::b::c =>
     Q.ABBREV_TAC `cnd=^b`))
  \\ (Cases_on `cnd`)
  THENL [
    (fs [mmu2mem_policy_def])
    \\ (rpt FIX_ABBREV_TAC)
    \\ (DEABBREV_AND_FIX_TAC)
    \\ (RW_TAC (srw_ss()) [])
    \\ (fs [Disabled_ST2_def]),
    ALL_TAC
  ]

  \\ (DEABBREV_AND_FIX_TAC)
  \\ (fs [mmu2mem_policy_def])
  \\ ((RW_TAC (srw_ss()) []) \\ (fs []))
  THENL [
    (fs [Disabled_ST2_def]),
    ALL_TAC
  ]
  \\ (`mmu_state.DESC2.fault.typ = Fault_None` by ALL_TAC)
  THENL [
     (fs [combine_descriptors_def, IsFault_def])
     \\ (Cases_on `mmu_state.DESC2.fault.typ = Fault_None`)
     THENL [(fs []), ALL_TAC]
     \\ (fs []),
     ALL_TAC
  ]
  \\ (fs [])
);

val _ = export_theory();
