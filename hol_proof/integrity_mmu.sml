use "combine.sml";
use "tactics.sml";
open lcsymtacs;
use "tactics2.sml";


val mmu2mem_policy_def = Define `
  mmu2mem_policy mmu2mem policy =
      ((mmu2mem <> NONE) ==>
        ((THE mmu2mem).write) ==>
        (~ (policy ((THE mmu2mem).desc.paddress))))
`;

val mmu_integrity_def = ``
! mmu_state core2mmu mem2mmu mmu2core mmu2mem .
(mmu2mem_policy mmu2mem policy) ==>
let s' = mmu_step (mmu_state,core2mmu,mem2mmu,mmu2core,mmu2mem) in
let (mmu_state,core2mmu,mem2mmu,mmu2core,mmu2mem) = s' in
(mmu2mem_policy mmu2mem policy)
``;

prove(``^mmu_integrity_def``,
  REPEAT (GEN_TAC)
  THEN (REPEAT DISCH_TAC)
  THEN (ABBREV_GOAL)
  THEN (MY_LET_AND_EXEC2 [mmu_step_def, mmu_interface_def])

  (* This is the forward of messages to the core *)
  THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `s`])
  THEN (pairLib.PairCases_on `s''`)
  THEN (Q.ABBREV_TAC `cpu_state' = s''7`)
  THEN (Q.ABBREV_TAC `mmu_state' = s''6`)
  THEN (Q.ABBREV_TAC `core2mmu' = s''5`)
  THEN (Q.ABBREV_TAC `mem2mmu' = s''4`)

  THEN (`mmu_state''' = mmu_state` by ALL_TAC)
  THENL [
    (Cases_on `mem2mmu ≠ NONE`)
    THENL [
      FST THEN
      (Cases_on `(THE mem2mmu).acctype ≠ AccType_PTW`)
      THENL [
        FST
        THEN (REPEAT FIX_ABBREV_TAC)
        THEN (DEABBREV_AND_FIX_TUPLE_TAC)
        THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF]),
        FST
        THEN (REPEAT FIX_ABBREV_TAC)
        THEN (DEABBREV_AND_FIX_TAC)
        THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
      ],
      FST
      THEN (REPEAT FIX_ABBREV_TAC)
      THEN (DEABBREV_AND_FIX_TAC)
      THEN (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    ],
    ALL_TAC
  ]
  THEN FST
  THEN (Q.ABBREV_TAC `cnd=(mmu_state.S ≠ mmu_wait ∨ core2mmu ≠ NONE ∨ mem2mmu ≠ NONE) ∧ ¬s''0`)
  THEN (Cases_on `~cnd`)
  (* first case we are not doing anithing *)
  THENL [
    (FULL_SIMP_TAC (srw_ss()) [LET_DEF])
    THEN (DEABBREV_AND_FIX_TAC)
    THEN (RW_TAC (srw_ss()) [])
    THEN FST
    THEN ((Cases_on `mem2mmu ≠ NONE`)
    THENL [
       FST
       THEN (Cases_on `(THE mem2mmu).acctype ≠ AccType_PTW`)
       THENL [
         FST, FST
       ],
       FST
    ]),
    ALL_TAC
  ]

  THEN FST
  THEN (Cases_on `mmu_state.S`)
  THENL [

  ]



prove(``^mmu_integrity_def``,
     (srw_tac [] [])
  \\ (full_simp_tac (srw_ss()) [mmu_step_def])
  (* This is let tuple = function in f, *)
  (* shuold be transformed in Abbrev(tuple) = function /\ f *)
  \\ LET_NORM_TAC
  (* This is let x = function in f, *)
  (* shuold be transformed in Abbrev(x = function) /\ f *)
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC

  \\ (fs [])
  \\ (RW_TAC (srw_ss()) [])

  \\ (full_simp_tac (srw_ss()) [mmu_interface_def])

  (* This is let tuple = function in f, *)
  (* shuold be transformed in Abbrev(tuple) = function /\ f *)
  \\ LET_NORM_TAC
  (* We split every pair *)
  \\ LET_NORM_TAC

  (* We reabbreviate the varaibles to use meaningfull names *)
  (* but if we ase reabbrev we have a mess *)
  (* \\ (REABBREV_TAC) *)
  (* \\ (full_simp_tac (srw_ss()) []) *)
  \\ NORMALIZE_VAR_TAC
  (* This is let tuple = function in f, *)
  (* shuold be transformed in Abbrev(tuple) = function /\ f *)
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  (* We reabbreviate the varaibles to use meaningfull names *)
  (* but if we ase reabbrev we have a mess *)
  \\ NORMALIZE_VAR_TAC

  (* This is let x = function in f, *)
  (* shuold be transformed in Abbrev(x = function) /\ f *)
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC


  (* probably ad this point is better to use core2mmu instead of s0 etc *)
  (* probably we want to remove abbreviations that have no effect *)
  \\ (full_simp_tac (srw_ss()) [])

  (* This is let tuple = function in f, *)
  (* shuold be transformed in Abbrev(tuple) = function /\ f *)
  \\ LET_NORM_TAC


  (* This is let tuple = function in f, *)
  (* shuold be transformed in Abbrev(tuple) = function /\ f *)
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ NORMALIZE_VAR_TAC


  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ NORMALIZE_VAR_TAC


  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ NORMALIZE_VAR_TAC

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ NORMALIZE_VAR_TAC

  (* I do not find this let in the original code *)
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (`s6 = s1` by ALL_TAC)
  THENL [
     (* remove all unused assumptions *)
     (PAT_ASSUM ``Abbrev (x = if mem2mmu ≠ NONE then y else z)`` (fn thm =>
        (rpt (PAT_ASSUM ``x`` (fn thm1 => ALL_TAC)))
        THEN (ASSUME_TAC thm)))
     THEN (DEABBREV_AND_FIX_TAC)
     THEN (Cases_on `mem2mmu = NONE`)
     THENL [fs [], fs []]
     THEN (Cases_on `(THE mem2mmu).acctype ≠ AccType_PTW`)
     THENL [fs [], fs []],
     (FULL_SIMP_TAC (srw_ss()) [])
  ]
  THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `s1`])
  THEN (FULL_SIMP_TAC (srw_ss()) [Abbr `s0`])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC

  (* If I do not semplificate I douple the size of assumtion *)
  \\ (FULL_SIMP_TAC (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s4'`, Abbr `s3'`, Abbr `s2''`,
                                Abbr `s0`])

  \\ (Q.ABBREV_TAC `cnd = (mmu_state.S ≠ mmu_wait ∨ core2mmu ≠ NONE ∨ mem2mmu ≠ s1) ∧¬s0'`)
  \\ (Cases_on `~cnd`)
  THENL [
    (FULL_SIMP_TAC (srw_ss()) [])
    \\ (fs [])
    \\ (fs [Abbr `s`])
    \\ (fs [Abbr `s1`])
    \\ (Cases_on `mem2mmu = NONE`)
    THENL [
      (fs [])
      \\ (rpt SPLIT_ABBREVS)
      \\ (rpt FIX_ABBREV_TAC)
      \\ (fs []),
      ALL_TAC
    ]
    \\ (fs [])
    \\ (Cases_on `(THE mem2mmu).acctype ≠ AccType_PTW`)
    THENL [
      (fs [])
      \\ (rpt FIX_ABBREV_TAC)
      \\ (DEABBREV_AND_FIX_TAC)
      \\ (fs []),
      ALL_TAC
    ]
    \\ (fs [])
    \\ (rpt FIX_ABBREV_TAC)
    \\ (DEABBREV_AND_FIX_TAC)
    \\ (fs []),
    ALL_TAC
  ]
  \\ (FULL_SIMP_TAC (srw_ss()) [])

  \\ (Cases_on `mmu_state.S`)
  THENL [
     (FULL_SIMP_TAC (srw_ss()) [])
     \\ LET_NORM_TAC
     \\ LET_NORM_TAC
     \\ LET_NORM_TAC
     \\ (Cases_on `tlb`)
     \\ THENL [
        (FULL_SIMP_TAC (srw_ss()) [])
        \\ (fs [])
        \\ (fs [Abbr `s`])
        \\ (rpt FIX_ABBREV_TAC)
        \\ (DEABBREV_AND_FIX_TAC)
        \\ (fs [])
        \\ (RW_TAC (srw_ss()) [])
     ]  
  ]



