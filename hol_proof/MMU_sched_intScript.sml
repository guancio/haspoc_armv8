open HolKernel boolLib bossLib Parse;
open lcsymtacs;

open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open pairLib;
open pairTools;

open armCombineTheory;
open invariantsIntTheory;

open MMU_ST2_sched_intTheory;

(*
HOL_Interactive.toggle_quietdec();
open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open pairLib;
open pairTools;

open armCombineTheory;
open invariantsIntTheory;

open MMU_ST2_sched_intTheory;
HOL_Interactive.toggle_quietdec();
*)

val _ = new_theory "MMU_sched_int";

val MMU_sched_int_def = ``
(!r l m ommu omem cons m' s'.
  (((ommu, omem, cons, m'), s') = MMU_ST1_sched(r,l,m) ARB) ==>
  (
   ((m'.ST1 = walk) = ((THE omem).acctype = AccType_PTW)) /\
   (((THE omem).acctype <> AccType_PTW) ==>
    ((r.acctype = (THE omem).acctype) /\
     (r.write = (THE omem).write)
   )) /\
   (((THE omem).acctype = AccType_PTW) ==> ¬(THE omem).write) /\
   (s' = ARB)
 )
) ==>
(m.CURR_REQ.EL < 2 /\ m.CURR_REQ.ns) ==>
(mmu_int_invariant mmu_state policy policy_pt) ==>
(* This part depends on the configuration of pts *)
(!w f walk .
(lookup <> NONE) ==>
(
 ((w,f) =walk_extend
         (walk,cast_desc (v2w (THE lookup).data) ARB,
          ARB.MMUISW.IMPL_CONTIGUOUS_BIT_FAULT, ARB) ARB) ==>
 ((w.acctype = walk.acctype) /\
  (w.wr = walk.wr) /\
  (
    (w.c) ==>
    (walk.wr) ==>
    ((check_walk_result (w,8,mmu_state.HCR2.PTW,ARB) ARB).typ = Fault_None) ==>
    (¬policy w.adr)
  ) /\
  (
    (~w.c) ==>
    ((w.st = 2) ∧ (w.n = 32) ∧ (policy_pt w.adr))
  )
 ))
(* We must know something of w if the translation failed *)
 ) ==>
(m.MMU2 = mmu_state) ==>
let s' = MMU_sched (req,lookup,m) ARB in
let ((m2core, message, cons, m'), s'') = s' in
let goal =  
( 
 (mmu_int_invariant m'.MMU2 policy policy_pt)  /\  
 (mmu2mem_policy message policy) /\
 (m'.CURR_REQ.EL < 2 /\ m'.CURR_REQ.ns)
) in
goal``;



val MMU_sched_int_thm = store_thm(
    "MMU_sched_int_thm",
    ``^MMU_sched_int_def``,
  (srw_tac [] [])
  \\ (full_simp_tac (srw_ss()) [MMU_sched_def])
  \\ LET_NORM_TAC
  (* use the existing variables *)
  \\ (rev_full_simp_tac (srw_ss()) [])
  (* remove unused variabled *)
  \\ (rw_tac (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR
  
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_CONST
  \\ RM_ABBR_VAR
  \\ (rpt FIX_ABBREV_TAC)

  (* There is a problem with the standard LET_NORM_TAC *)
  (* that does not abbreviate correctly the state *)
  \\ NEW_LET_NORM_TAC
  \\ LET_NORM_TAC


  \\ (`(mmu_int_invariant s3.MMU2 policy policy_pt) /\
       (s3.CURR_REQ.EL < 2) /\ (s3.CURR_REQ.ns) /\
       ((s2 <> NONE) ==> (
         ((s3.MMU1.ST1 = walk) = ((THE s2).acctype = AccType_PTW)) /\
         (((THE s2).acctype <> AccType_PTW) ==>
             ((s3.CURR_REQ.acctype = (THE s2).acctype) /\
              (s3.CURR_REQ.write = (THE s2).write)
         )) /\
         (((THE s2).acctype = AccType_PTW) ==> ¬(THE s2).write)
       )) /\
       ((s3.MMU2.ST2 <> wait) ==> (s2 = NONE)) /\
       (s4 = ARB) /\
       (s3.MMU2.HCR2.PTW = m.MMU2.HCR2.PTW) /\
       (s3.CURR_REQ = m.CURR_REQ)
       ` by ALL_TAC)


  THENL [
    (PAT_ASSUM ``Abbrev (((r0,r1,r2,r3),s10',s11',s12,s13',s14) = x)`` (fn thm => ALL_TAC))
    \\ (TERM_PAT_ASSUM_W_THM ``Abbrev(a = if b then c else d)`` (fn a::b::rest =>
         (Q.ABBREV_TAC `cnd=^b`)))
    \\ (Cases_on `~cnd`)
    THENL [
      (fs []) \\ (DEABBREV_AND_FIX_TAC) \\ (fs []),
      (ALL_TAC)
    ]
    \\ (FULL_SIMP_TAC (srw_ss()) [])
    \\ (rpt (LET_NORM_TAC))

    \\ NEW_RM_ABBR_VAR

    \\ INVERT_ABBR_CONST
    \\ (RM_ABBR_CONST)
    \\ (fs [])

    \\ (DEABBREV_AND_FIX_TAC)
    (* To avoid duplication *)
    \\ (Q.ABBREV_TAC `cnd1 = s3'2.MMU1.ST1 ≠ wait ∨ req ≠ NONE`)
    \\ (fs [])

    (* No needed anymore *)
    (* \\ (`s3''3 = ARB` by ALL_TAC) *)
    (* THENL [ *)
    (*       (Cases_on `s3'2.MMU1.ST1 = s3'2.MMU2.ST2`) *)
    (*       THENL [fs [], fs[]], *)
    (*       ALL_TAC *)
    (* ] *)
    (* \\ (fs []) *)

    \\ (TERM_PAT_ASSUM_W_THM ``(((a,b,c,d),e) = MMU_ST1_sched (f,g,h) ARB)``
             (fn [ommu,omem,cons,m1,s1,r,l,m] => PAT_ASSUM ``!r l m ommu omem cons m' s'.p`` (fn thm =>
            ASSUME_TAC (SPECL [r,l,m,ommu,omem,cons,m1,s1] thm))))
    \\ (rev_full_simp_tac (srw_ss()) [])
    \\ (fs [mmu_int_invariant_def, Disabled_ST2_def])
   ,ALL_TAC]

  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (PAT_ASSUM ``Abbrev (((r0,r1,r2,r3),s10',s11',s12,s13',s14) = x)`` ASSUME_TAC)
  \\ LET_NORM_TAC
  \\ (Cases_on `~v`)
  THENL [
        (fs []) \\ (DEABBREV_AND_FIX_TAC) \\ (fs [mmu2mem_policy_def]),
        ALL_TAC
  ]
  \\ (FULL_SIMP_TAC (srw_ss()) [])
  (* Remove what I do not use *)
  \\ (PAT_ASSUM ``Abbrev ((s0',s1,s2,s3,ARB) = x)`` (fn thm=>ALL_TAC))
  \\ (rpt LET_NORM_TAC)
  \\ (NEW_RM_ABBR_VAR)
  \\ (INVERT_ABBR_CONST)
  \\ (fs [])

  (* If we are not stepping the stage2, we have done *)
  \\ (Q.ABBREV_TAC `step2=(s3.MMU2.ST2 ≠ wait ∨ IS_SOME s2)`)
  \\ (Cases_on `~step2`)
  THENL [
        (fs []) \\ (DEABBREV_AND_FIX_TAC) \\ (fs []),
        ALL_TAC
  ]

  \\ (fs [])
  \\ (TERM_PAT_ASSUM_W_THM ``Abbrev(a = MMU_ST2_sched
          (lookup,b,c,d,e,f) g)``  (fn [a,b,c,d,e,f,g] =>
          ASSUME_TAC (
          SPECL [f,b,c,d,e] (
          GENL [``mmu_state:MMU_CONFIG2``, ``omem:MEM_REQUEST option``, ``curr_req:MEM_REQUEST``,
                   ``desc1:AddressDescriptor``, ``ptw:bool``
                       ] MMU_ST2_sched_int_thm)
     )))
 \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

 \\ (`(s3.MMU2.ST2 ≠ wait ⇔ (s2 = NONE))` by ALL_TAC)
 THENL [
       (DEABBREV_AND_FIX_TAC)
       \\ (`(s2: MEM_REQUEST option <> NONE) = (IS_SOME s2)` by (METIS_TAC [optionTheory.NOT_IS_SOME_EQ_NONE]))
       \\ (fs [])
       \\ (RW_TAC (srw_ss()) [])
       \\ (rev_full_simp_tac (srw_ss()) [])
       \\ (fs []),
       ALL_TAC
  ]

  \\ (fs [])

  \\ (TERM_PAT_ASSUM_W_THM ``a ==> (b/\c)`` (fn a::b =>
       (Q.ABBREV_TAC `pre=^a`)))
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

  \\ (DEABBREV_AND_FIX_TAC)
  \\ (Q.ABBREV_TAC `cnd1 = (s3''''4.MMU1.ST1 = wait) ∧ IS_SOME s'3`)
  \\ (Cases_on `cnd1`)
  THENL [
    (fs[mmu_int_invariant_def, Disabled_ST2_def, mmu_intermediate_walk_inv_def, PTO_ST2_def])
    \\ (RW_TAC (srw_ss()) [])
    \\ (fs[])
    \\ (REV_FULL_SIMP_TAC (srw_ss()) []),

    ALL_TAC
  ]
  \\ (fs [])
  \\ (RW_TAC (srw_ss()) [])
);


val _ = export_theory();
