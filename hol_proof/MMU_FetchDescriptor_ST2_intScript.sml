open HolKernel boolLib bossLib Parse;
open lcsymtacs;

open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open armCombineTheory;
open invariantsIntTheory;

(*

HOL_Interactive.toggle_quietdec();
open lcsymtacs;
open dharma8Theory;
open wordsLib;
open guancioTacticsLib;
open guancioTactics2Lib;
open armCombineTheory;
open invariantsIntTheory;
HOL_Interactive.toggle_quietdec();

*)

val _ = new_theory "MMU_FetchDescriptor_ST2_int";

val MMU_FetchDescriptor_ST2_int_def = ``
(mmu_state.ST2 = mem) ==>
(mmu_int_invariant mmu_state policy policy_pt) ==>
let s' = MMU_FetchDescriptor_ST2 (req,mmu_state) ARB in
let ((mmu2mem, mmu_state), s) = s' in
let goal =
 (mmu_int_invariant mmu_state policy policy_pt) /\
 (mmu2mem_policy (SOME mmu2mem) policy) in
 goal
``;

val MMU_FetchDescriptor_ST2_int_thm = store_thm("MMU_FetchDescriptor_ST2_int_thm",
  ``^MMU_FetchDescriptor_ST2_int_def``,
  (srw_tac [] [])
  \\ (RULE_ASSUM_TAC (SIMP_RULE (srw_ss()) [mmu_int_invariant_def]))
  \\ (FULL_SIMP_TAC (srw_ss()) [MMU_FetchDescriptor_ST2_def])
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)
  \\ (RM_ABBR_VAR)

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC

  \\ (RM_ABBR_VAR)
  \\ (REV_FULL_SIMP_TAC (srw_ss()) []) 

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])
  \\ (FULL_SIMP_TAC (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])

  \\ (fs [])
  \\ (DEABBREV_AND_FIX_TAC)
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [mmu2mem_policy_def, Disabled_ST2_def])
  \\ (fs [])
);

(* TODO: Add pt_policy to the output *)

val _ = export_theory();
