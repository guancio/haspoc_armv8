open HolKernel boolLib bossLib Parse;
open lcsymtacs;

open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open armCombineTheory;
open invariantsIntTheory;

(*

HOL_Interactive.toggle_quietdec();
open lcsymtacs;
open dharma8Theory;
open wordsLib;
open guancioTacticsLib;
open guancioTactics2Lib;
open armCombineTheory;
open invariantsIntTheory;
HOL_Interactive.toggle_quietdec();

*)

(* val mmu_core_policy_def = Define ` *)
(* mmu_core_policy (req,reg:MMU_REGS) = *)
(*   (req.EL < 2) /\ *)
(*   (req.ns) *)
(* `; *)

(* val policy_opt_def = Define ` *)
(* policy_opt policy channel = *)
(* (channel <> NONE) ==> *)
(* (policy (THE channel)) *)
(* `; *)


(* (\* for now we just use a whole GB as area for the page table. *\) *)
(* val mmu_policy_tbl_def = Define ` *)
(*   mmu_policy_tbl mmu_state = *)
(*      \adr. *)
(*      ((0xffc0000000w && adr) = (0xffc0000000w && mmu_state.MMU.VTTBR.BADDR)) *)
(* `; *)

(* val mmu_policy_data_def = Define ` *)
(*   mmu_policy_data mmu_state = *)
(*      \adr. *)
(*      ((0xffc0000000w && adr) = (0xffc0000000w && mmu_state.MMU.VTTBR.BADDR)) *)
(* `; *)


(* val mmu_invariant_def = Define ` *)
(*   mmu_invariant mmu_state mmu2mem mem2mmu policy policy_tbl GI = *)
(* (mmu_mem_policy mmu2mem mem2mmu policy policy_tbl GI) /\ *)
(* (\* the processed request is performed non in hypervisor mode *\) *)
(* (mmu_state.MMU.CURR_REQ.EL < 2) /\ *)
(* (\* the processed request is performed non non secure mode *\) *)
(* (mmu_state.MMU.CURR_REQ.ns) /\ *)
(* (mmu_state.MMU.HCR.VM) /\ *)
(* ((mmu_state.MMU.F2.typ = Fault_None) ==> *)
(*  (\* if we the second stage is in state mem, then *\) *)
(*  (\* the current request is pointing to the hypervisor memory *\) *)
(*  (\* (hypervisor PT memory) *\) *)
(*  ((mmu_state.MMU.ST2 = mem) (\* \/ (mmu_state.MMU.ST2 = final) *\)) ==> *)
(*  ((policy_tbl mmu_state.MMU.DESC2.paddress)) *)
(* ) /\ *)
(* ((mmu_state.MMU.DESC2.fault.typ = Fault_None) ==> *)
(*  (\* if we the second stage is in state final, then *\) *)
(*  (\* and the current request is a write, then it should point ouside *)
(*   the hypervisor memory *\) *)
(*  ((mmu_state.MMU.ST2 = final)) ==> *)
(*  (mmu_state.MMU.MEMREQ.write) ==> *)
(*  (~(policy mmu_state.MMU.DESC2.paddress)) *)
(* ) /\ *)
(* (mmu_state.MMU.W2.n = 32) /\ *)
(* (mmu_state.MMU.W2.br = 9) /\ *)
(* (\* We only use one level for the second stage *\) *)
(* (mmu_state.MMU.W2.l = 1) /\ *)
(* (\* The second stage walk is always second stage *\) *)
(* (mmu_state.MMU.W2.st = 2) /\ *)
(* (~mmu_state.MMU.W2.conc) /\ *)
(* (mmu_state.MMU.VTCR.T0SZ = 32w) /\ *)
(* (mmu_state.MMU.VTCR.TG0 = 0w) /\ *)
(* (mmu_state.MMU.IDMM.TGRAN4 = 0w) /\ *)
(* (mmu_state.MMU.VTCR.SL0 = 1w) /\ *)
(* (~mmu_state.MMU.ISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO) /\ *)
(* ((mmu_state.MMU.ST2 = mem) ==> (mmu_state.MMU.F2.typ = Fault_None)) /\ *)
(* (\* It must be aligned, since we will use four descriptors from this address, each having 8 bytes? *\) *)
(* (0xffffffffe0w && mmu_state.MMU.VTTBR.BADDR = mmu_state.MMU.VTTBR.BADDR) *)
(* `; *)

val _ = new_theory "static_conf_int";


val check_walk_result_w_thm = prove(``!w.
    ((check_walk_result (w:Walk, 8, mmu_state.HCR2.PTW, ARB) ARB).typ = Fault_None) ==>
    (w.wr) ==>
    (w.W)
``,
  (RW_TAC (srw_ss()) [])
  \\ (fs [check_walk_result_def])
  \\ (Cases_on `~w.A`)
  THENL [
        (fs []),
        (fs [])
  ]
  \\ (Q.ABBREV_TAC `cnd = ((NoFaultfromWalk w ARB).typ = Fault_None) ∧
                          ¬Aligned (w.adr,8) ∧
                          (w.attr.typ = MemType_Device) ∧
                          w.acctype ≠ AccType_IFETCH`)
  \\ (Cases_on `cnd`)
  THENL [
        (fs []),
        (fs [])
  ]
  \\ (Cases_on `~((NoFaultfromWalk w ARB).typ = Fault_None)`)
  THENL [
        (fs []), (fs [])
  ]
  \\ (Cases_on `~(NoFaultfromWalk w ARB).s2fs1walk`)
  THENL [
        (fs []) 
        \\ (Q.ABBREV_TAC `cnd1 = (¬w.W ∨
             w.acctype ≠ AccType_IFETCH ∧ (w.st = 1) ∧ (w.el = 0) ∧
             ¬w.U) ∨
            (w.acctype = AccType_IFETCH) ∧
            ((w.st = 1) ∧ ARB ∧ w.W ∨
             if w.el = 1 then
               if w.st = 1 then w.PXN ∨ w.U ∧ w.W else w.XN
             else w.XN)`)
        \\ (Cases_on `cnd1`)
        THENL [
              (fs []),
              (fs [])
        ]
        \\ (DEABBREV_AND_FIX_TAC) \\ (fs []),
        (fs [])
  ]
  \\ (Cases_on `~mmu_state.HCR2.PTW`)
  THENL [
        (fs [])
        \\ (Q.ABBREV_TAC `cnd = (¬w.W ∨
             w.acctype ≠ AccType_IFETCH ∧ (w.st = 1) ∧ (w.el = 0) ∧
             ¬w.U) ∨
            (w.acctype = AccType_IFETCH) ∧
            ((w.st = 1) ∧ ARB ∧ w.W ∨
             if w.el = 1 then
               if w.st = 1 then w.PXN ∨ w.U ∧ w.W else w.XN
             else w.XN)`)
        \\ (Cases_on `cnd`)
        THENL [(fs[]),
              (DEABBREV_AND_FIX_TAC) \\ (fs [])
       ],
       (fs[])
  ]
  \\ (Cases_on `~(w.attr.typ = MemType_Device)`)
  THENL [
        (fs [])
        \\ (Q.ABBREV_TAC `cnd = (¬w.W ∨
             w.acctype ≠ AccType_IFETCH ∧ (w.st = 1) ∧ (w.el = 0) ∧
             ¬w.U) ∨
            (w.acctype = AccType_IFETCH) ∧
            ((w.st = 1) ∧ ARB ∧ w.W ∨
             if w.el = 1 then
               if w.st = 1 then w.PXN ∨ w.U ∧ w.W else w.XN
             else w.XN)`)
        \\ (Cases_on `cnd`)
        THENL [(fs[]),
              (DEABBREV_AND_FIX_TAC) \\ (fs [])
       ],
       (fs[])
  ]
);



val static_conf_def = Define `
static_conf w =
  (w.br = 9) /\
  (w.n = 32) /\
  (~ w.conc) /\
  (w.l = 1) /\
  (w.st = 2)
`;


val pt_conf_def = Define `
pt_conf lookup policy =
 (!x .(v2w (bx ((47 >< 0) x.va,9,1,T))) = (0x3fffffffw:word48 && (w2w x.va))) /\
 (!base x. policy (base + (0x3FFFFFFFw && w2w x.va)) = policy base) /\
 ((cast_desc (v2w lookup.data) ARB).v) /\
 (* We only use one level *)
 (~(cast_desc (v2w lookup.data) ARB).pte) /\
 ((word_bit 1 (cast_desc (v2w lookup.data) ARB).AP) ==>
  ¬policy (DESC_ADR (cast_desc (v2w lookup.data) ARB,9,1)))
`;



val static_conf_int_def = ``
(!w f walk . (
 (static_conf walk) ==>
 (* We only use one level *)
 (pt_conf lookup policy) ==>
 (walk.wr = mmu_state.MEMREQ.write) ==>
 (((walk.acctype = AccType_PTW) ⇒ ¬walk.wr)) ==>
 (
 ((w,f) =walk_extend
         (walk,cast_desc (v2w (lookup).data) ARB,
          ARB.MMUISW.IMPL_CONTIGUOUS_BIT_FAULT, ARB) ARB) ==>
 let goal = ((w.acctype = walk.acctype) /\
  (w.wr = walk.wr) /\
  (
  (walk.wr) ==>
  ((check_walk_result (w,8,mmu_state.HCR2.PTW,ARB) ARB).typ = Fault_None) ==>
  (f.typ = Fault_None) ==>
  ( (¬policy w.adr) /\
(* we are assuming that we have only one level of pt *)
  w.c)
  )
 ) in
 goal
 )
))``;

val static_conf_int_thm = store_thm(
    "static_conf_int_thm",
    ``^static_conf_int_def``,
  (srw_tac [] [])
  \\ (FULL_SIMP_TAC (srw_ss()) [walk_extend_def])
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RW_TAC (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ (rpt FIX_ABBREV_TAC)
  \\ LET_NORM_TAC
  \\ (RM_ABBR_VAR)

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)
  \\ (RM_ABBR_VAR)

  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF, NoFaultfromWalk_def,
          NoFault_def, CreateFaultRecord_def] thm)))

  \\ LET_NORM_TAC
  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF, nflb_def] thm)))

  \\ (FULL_SIMP_TAC (srw_ss()) [static_conf_def])

  \\ (`(nlu (walk'.n,walk'.br,walk'.conc) ARB) = 3` by (
     (FULL_SIMP_TAC (srw_ss()) [nlu_def])
     \\ EVAL_TAC
  ))
  \\ (FULL_SIMP_TAC (srw_ss()) [])

  \\ (rpt FIX_ABBREV_TAC)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `v0`, Abbr `s0`])

  \\ LET_NORM_TAC
  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF, PBD_invalid_def] thm)))
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

  \\ (rpt FIX_ABBREV_TAC)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s01`])

  \\ (LET_NORM_TAC)
  \\ (LET_NORM_TAC)
  \\ (RM_ABBR_VAR)
  \\ (RM_ABBR_CONST)

  (* If there is a translation fault, then it is not a problem *)
  \\ (TERM_PAT_ASSUM_W_THM ``Abbrev(a = if b then c else d)`` (fn a::b::c =>
      Q.ABBREV_TAC `cnd=^b`))
  \\ (Cases_on `cnd`)
  THENL [
        (fs []) 
        \\ (DEABBREV_AND_FIX_TAC)
        \\ (fs []) ,
        ALL_TAC
  ]
  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (rpt FIX_ABBREV_TAC)
  \\ (LET_NORM_TAC)
  \\ (RM_ABBR_VAR)
  \\ (RM_ABBR_CONST)

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC

  (* We only use one level *)
  \\ (`~((cast_desc (v2w lookup.data) ARB).pte)` by ALL_TAC)
  THENL [
        (FULL_SIMP_TAC (srw_ss()) [pt_conf_def]),
        ALL_TAC
  ]
  \\ (FULL_SIMP_TAC (srw_ss()) [])

  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0'1`])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC

  \\ RM_ABBR_TRUE_VAR
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s`])
  \\ (rpt FIX_ABBREV_TAC)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0'`, Abbr `s1'`])

  \\ LET_NORM_TAC

  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF] thm)))
  \\ (RM_ABBR_CONST)

  \\ (Q.ABBREV_TAC `lk_desc = (cast_desc (v2w lookup.data) ARB)`)
  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0'`, Abbr `s1'`])

  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0'`, Abbr `s1'`])

  \\ LET_NORM_TAC
  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF] thm)))
  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0'`, Abbr `s1'`])
  \\ (RM_ABBR_CONST)

  \\ LET_NORM_TAC
  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF] thm)))
  \\ LET_NORM_TAC

  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0'`, Abbr `s1'`])

  \\ (FULL_SIMP_TAC (srw_ss()) [check_address_size_def])
  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF] thm)))

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF] thm)))

  (* If there is a translation fault, then it is not a problem *)
  \\ (TERM_PAT_ASSUM_W_THM ``Abbrev(a = if b then c else d)`` (fn a::b::c =>
      Q.ABBREV_TAC `cnd1=^b`))
  \\ (Cases_on `cnd1`)
  THENL [
        (fs [])
        \\ (rpt FIX_ABBREV_TAC)
        \\ (RM_ABBR_VAR)
        \\ (RM_ABBR_CONST)
        \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `r`])
        \\ (rpt FIX_ABBREV_TAC)
        \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s1'0`])
        \\ (rpt FIX_ABBREV_TAC)
        \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0`, Abbr `s1`])
        ,
        ALL_TAC
  ]
  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (rpt FIX_ABBREV_TAC)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s`])
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `r`])
  \\ (rpt FIX_ABBREV_TAC)
  \\ (RM_ABBR_VAR)
  \\ (RM_ABBR_CONST)

  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0`])
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
  \\ (fs [Abbr `goal`])
  \\ (RW_TAC (srw_ss()) [])

  \\ (FULL_SIMP_TAC (srw_ss()) [pt_conf_def])
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

  (* we must show that the descriptor has the writable flag *)
  \\ (SUBGOAL_THEN ``word_bit 1 lk_desc.AP`` (fn thm =>
       (fs [thm])))

  \\ (TERM_PAT_ASSUM_W_THM ``(check_walk_result (w ,a,b,c) d).typ = Fault_None`` (fn w::r =>
      Q.ABBREV_TAC `w=^w`))
  \\ (ASSUME_TAC (SPEC ``w:Walk`` check_walk_result_w_thm))
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
  \\ (fs [Abbr `w`])
);  


val bitify_2bit_thm = prove (``(bitify
                [0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0;
                 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0;
                 0; 0; 0; 0; 0; 0; 0]
                [(((47 :num) >< (0 :num)) mmu_state.W2.va :word48) '
                  (31 :num);
                  (((47 :num) >< (0 :num)) mmu_state.W2.va :word48) '
                  (30 :num); F; F; F]) =
                 [
                 0; 0; 0; if (((47 :num) >< (0 :num)) mmu_state.W2.va :word48) '
                  (30 :num) then 1 else 0; if (((47 :num) >< (0 :num)) mmu_state.W2.va :word48) '
                  (31 :num) then 1 else 0;
                 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0;
                 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0;
                 0; 0; 0; 0; 0; 0; 0]``,
       (Cases_on `(((47 :num) >< (0 :num)) mmu_state.W2.va :word48) ' (30 :num)`)
       THENL [
         (fs [])
         \\ (Cases_on `(((47 :num) >< (0 :num)) mmu_state.W2.va :word48) ' (31 :num)`)
         THENL [
           (fs [bitstringTheory.bitify_def]),
           (fs [bitstringTheory.bitify_def])
         ],
         ALL_TAC
       ]
       \\ (Cases_on `(((47 :num) >< (0 :num)) mmu_state.W2.va :word48) ' (31 :num)`)
       THENL [
           (fs [bitstringTheory.bitify_def]),
           (fs [bitstringTheory.bitify_def])
       ]
);

val higest_bit_thm = prove(``
  (n2w
     (8 *
      (2 * (if (((47 :num) >< (0 :num)) mmu_state.W2.va :word48) '
                  (31 :num) then 1 else 0) MOD 2 +
       (if (((47 :num) >< (0 :num)) mmu_state.W2.va :word48) '
                  (30 :num) then 1 else 0) MOD 2))) = ((0b11w && w2w (mmu_state.W2.va >> 30):word48) * 8w)
  ``,
   (blastLib.BBLAST_TAC)
   \\ (Cases_on `mmu_state.W2.va ' 31`)
   THENL [
      (fs [])
      \\ (Cases_on `mmu_state.W2.va ' 30`)
      THENL [
        (fs[]),
        (fs[])
      ],
      ALL_TAC
   ]
   \\ (fs [])
   \\ (Cases_on `mmu_state.W2.va ' 30`)
   THENL [
       (fs[]),
       (fs[])
 ]);



val tmp = ``(policy_pt (
(ADfromWalk
      (mmu_state.W2 with
       adr :=
         PTO_ST2 (mmu_state.W2,mmu_state) (ARB :dharma8_state) +
         (((0w :word28) @@
           (((v2w
                (px
                   ((((47 :num) >< (0 :num)) mmu_state.W2.va :word48),
                    mmu_state.W2.l,mmu_state.W2.br,mmu_state.W2.n,
                    mmu_state.W2.conc ∧
                    (mmu_state.W2.l =
                     (4 :num) −
                     nlu
                       (mmu_state.W2.n,mmu_state.W2.br,
                        mmu_state.W2.conc) (ARB :dharma8_state)))) :
                17 word) @@
            (0w
              :word3))
              :word20))
            :word48)) (ARB :dharma8_state)).paddress)):bool
``;

val static_conf_ptpol_thm = store_thm(
    "static_conf_ptpol_thm",
    ``
(!b.(policy_pt
  (8w * (3w && b) +
   if ARB.MMUISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO then align (BADDR,5)
   else BADDR))) ==>
(mmu_state.VTTBR  = TTBRType ASID BADDR) ==>
(static_conf mmu_state.W2) ==> (^tmp)
``,
  (rpt STRIP_TAC)
  \\ (fs [static_conf_def])
  \\ (fs [ADfromWalk_def, PTO_ST2_def, nflb_def, nlu_def, reg'TTBRType_def])
  \\ (fs [px_def])
  \\ EVAL_TAC
  \\ (fs [])
  \\ (fs [bitify_2bit_thm])
  \\ (fs [numposrepTheory.l2n_def])
  \\ (fs [higest_bit_thm])
  \\ (fs [ADfromWalk_def])
  \\ (fs [page_table_origin_def, rec'TTBRType_def])
  \\ (fs [(blastLib.BBLAST_PROVE ``(((47 :num) >< (0 :num))
     (((ASID :word16) @@ (BADDR :word48)) :word64) :word48) =
  BADDR``)])
  );


val inv_ptpol_thm = store_thm(
    "inv_ptpol_thm",
``(mmu_state.ST2 = init) ==>
  (mmu_int_invariant mmu_state policy policy_pt) ==>
  (^tmp)``,
  (RW_TAC (srw_ss()) [])
  \\ (fs [mmu_int_invariant_def])
  \\ (ASSUME_TAC static_conf_ptpol_thm)
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [static_conf_def])
);

val _ = export_theory();
