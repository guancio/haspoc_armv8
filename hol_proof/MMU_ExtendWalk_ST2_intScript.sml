open HolKernel boolLib bossLib Parse;
open lcsymtacs;

open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open armCombineTheory;
open invariantsIntTheory;

(*

HOL_Interactive.toggle_quietdec();
open lcsymtacs;
open dharma8Theory;
open wordsLib;
open guancioTacticsLib;
open guancioTactics2Lib;
open armCombineTheory;
open invariantsIntTheory;
HOL_Interactive.toggle_quietdec();

*)

val _ = new_theory "MMU_ExtendWalk_ST2_int";

val ADfromResult_fault_thm = prove(``! x y z f g. (ADfromResult (x,y,z,f) g).fault = y``,
  (srw_tac [][ADfromResult_def])
  \\ (Q.ABBREV_TAC `cnd = (x.st = 2) ∧ x.el < 2 ∧ (x.attr.typ = MemType_Normal) ∧
           ((x.acctype = AccType_IFETCH) ∧ z ∨
            f ∧ x.acctype ≠ AccType_IFETCH)`)
  \\ (Cases_on `cnd`)
  \\ (DEABBREV_AND_FIX_TAC)
  \\ (fs [])
);

val ADfromResult_addr_thm = prove(``! x y z f g. (ADfromResult (x,y,z,f) g).paddress = x.adr``,
  (srw_tac [][ADfromResult_def])
  \\ (Q.ABBREV_TAC `cnd = (x.st = 2) ∧ x.el < 2 ∧ (x.attr.typ = MemType_Normal) ∧
           ((x.acctype = AccType_IFETCH) ∧ z ∨
            f ∧ x.acctype ≠ AccType_IFETCH)`)
  \\ (Cases_on `cnd`)
  \\ (DEABBREV_AND_FIX_TAC)
  \\ (fs [])
);


val MMU_ExtendWalk_ST2_int_def = ``
(mmu_state.ST2 = walk) ==>
(mmu_int_invariant mmu_state policy policy_pt) ==>
(* This part depends on the configuration of pts *)
(!w f walk . (
 ((w,f) =walk_extend
         (walk,cast_desc (v2w lookup.data) ARB,
          ARB.MMUISW.IMPL_CONTIGUOUS_BIT_FAULT, ARB) ARB) ==>
 ((w.acctype = walk.acctype) /\
  (w.wr = walk.wr) /\
  (
    (w.c) ==>
    (walk.wr) ==>
    ((check_walk_result (w,8,mmu_state.HCR2.PTW,ARB) ARB).typ = Fault_None) ==>
    (¬policy w.adr)
  ) /\
  (
    (~w.c) ==>
    ((w.st = 2) ∧ (w.n = 32) ∧ (policy_pt w.adr))
  )
(* we are assuming that we have only one level of pt *)
  (* /\ w.c *)
 ))
(* We must know something of w if the translation failed *)
 ) ==>
let s' = MMU_ExtendWalk_ST2 (lookup,mmu_state) ARB in
let (mmu_state, s'') = s' in
(mmu_int_invariant mmu_state policy policy_pt)
``;


val MMU_ExtendWalk_ST2_int_thm = store_thm(
    "MMU_ExtendWalk_ST2_int_thm",
    ``^MMU_ExtendWalk_ST2_int_def``,
  (srw_tac [] [])
  \\ (RULE_ASSUM_TAC (SIMP_RULE (srw_ss()) [mmu_int_invariant_def]))
  \\ (FULL_SIMP_TAC (srw_ss()) [MMU_ExtendWalk_ST2_def])
  \\ (RW_TAC (srw_ss()) [])
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)
  \\ (RM_ABBR_VAR)

  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)
  \\ (RM_ABBR_VAR)

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_VAR)

  (* if we received the wrong value from the memory *)
  \\ (TERM_PAT_ASSUM_W_THM ``Abbrev(a = if b then c else d)`` (fn a::b::c =>
     Q.ABBREV_TAC `cnd=^b`))
  \\ (Cases_on `~cnd`)
  THENL [
        (fs [])
        \\ DEABBREV_AND_FIX_TAC
        \\ (fs [MMU_ExtendWalk_ST2_def, mmu_int_invariant_def]),
        (FULL_SIMP_TAC (srw_ss()) [])
  ]

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'0`])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC


  (* There is an exception from the memory *)
  \\ (Cases_on `lookup.desc.fault.typ <> Fault_None`)
  THENL [
      (FULL_SIMP_TAC (srw_ss()) [])
      \\ (FIX_ABBREV_TAC)
      \\ (DEABBREV_AND_FIX_TAC)
      \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
      (* solve the goal since the desc1 fault type is in conflict with the hypotesis *)
      \\ (fs [ADfromResult_fault_thm, mmu_int_invariant_def, Disabled_ST2_def]),
      ALL_TAC]

  \\ (FULL_SIMP_TAC (srw_ss()) [])

  (* too many if and conditions, we can probably choose a different *)
  (* startegy, that is show that if st2 = final then fault_none or c *)
  \\ LET_NORM_TAC
  
  (* Case we have not completed the look up *)
  (* we shoudl show that the PT request is in the right location *)
  \\ (Cases_on `~w.c`)
  THENL [
     (FULL_SIMP_TAC (srw_ss()) [])
     (* This is a duplication *)
     \\ (Cases_on `f.typ <> Fault_None`)
     THENL [
        (fs [])
        \\ (DEABBREV_AND_FIX_TAC)
        \\ (fs [])
        \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
        \\ (fs [ADfromResult_fault_thm, mmu_int_invariant_def])
        \\ (PAT_ASSUM ``! a . p`` (fn thm => ASSUME_TAC (SPECL [``w:Walk``, ``f:FaultRecord``, ``mmu_state.W2``] thm)))
        \\ (REV_FULL_SIMP_TAC (srw_ss()) [Disabled_ST2_def]),
        ALL_TAC
     ]
     \\ (fs [])
     \\ (DEABBREV_AND_FIX_TAC)
     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
     \\ (fs[mmu_int_invariant_def])
     \\ (PAT_ASSUM ``! a . p`` (fn thm => ASSUME_TAC (SPECL [``w:Walk``, ``f:FaultRecord``, ``mmu_state.W2``] thm)))
     \\ (REV_FULL_SIMP_TAC (srw_ss()) [ADfromWalk_def,  Disabled_ST2_def])
     \\ (fs[])
     ,ALL_TAC
  ]

  (* Walk completed *)
  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (Cases_on `f.typ <> Fault_None`)
  THENL [
      (fs [])
      \\ (DEABBREV_AND_FIX_TAC)
      \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
      \\ (fs [ADfromResult_fault_thm, mmu_int_invariant_def])
      \\ (PAT_ASSUM ``! a . p`` (fn thm => ASSUME_TAC (SPECL [``w:Walk``, ``f:FaultRecord``, ``mmu_state.W2``] thm)))
      \\ (REV_FULL_SIMP_TAC (srw_ss()) [Disabled_ST2_def]),
      ALL_TAC
  ]

  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s''0`])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s''0`])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_CONST)
  \\ (RM_ABBR_CONST)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s''0`])
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'0`])

  (* This must hold, since the final error type depends on this variable *)
  \\ (`(check_walk_result (w,8,mmu_state.HCR2.PTW, ARB) ARB).typ = s0.DESC2.fault.typ` by ALL_TAC)
  THENL [
     (DEABBREV_AND_FIX_TAC)
     \\ (fs [])
     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
     \\ (fs [ADfromResult_fault_thm]),
     ALL_TAC
  ]

  \\ (fs [mmu_int_invariant_def])
  \\ (RW_TAC (srw_ss()) [])
  (* This is repeated for every subgoal *)
  \\ ((DEABBREV_AND_FIX_TAC)
      \\ (fs [])
  )

  \\ (fs [combine_descriptors_def, IsFault_def, ADfromResult_addr_thm])
  \\ (PAT_ASSUM ``! a . p`` (fn thm => ASSUME_TAC (SPECL [``w:Walk``, ``f:FaultRecord``, ``mmu_state.W2``] thm)))
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
  \\ (fs [Disabled_ST2_def])
);

val _ = export_theory();

