open HolKernel boolLib bossLib Parse;

open dharma8Theory;
open wordsLib;

val _ = new_theory "invariantsInt";

val mmu2mem_policy_def = Define `
  mmu2mem_policy mmu2mem policy =
      ((mmu2mem <> NONE) ==>
        ((THE mmu2mem).write) ==>
        ((THE mmu2mem).acctype ≠ AccType_PTW ∧ (THE mmu2mem).acctype ≠ AccType_IFETCH) ==>
        (~ (policy ((THE mmu2mem).desc.paddress))))
`;

val mmu_intermediate_walk_inv_def = Define `
mmu_intermediate_walk_inv mmu_state policy_pt =
((policy_pt :word48 -> bool)
     (ADfromWalk
        (mmu_state.W2 with
         adr :=
           PTO_ST2 (mmu_state.W2,mmu_state) (ARB :dharma8_state) +
           (((0w :word28) @@
             (((v2w
                  (px
                     ((((47 :num) >< (0 :num)) mmu_state.W2.va :word48),
                      mmu_state.W2.l,mmu_state.W2.br,mmu_state.W2.n,
                      mmu_state.W2.conc ∧
                      (mmu_state.W2.l =
                       (4 :num) −
                       nlu
                         (mmu_state.W2.n,mmu_state.W2.br,
                          mmu_state.W2.conc) (ARB :dharma8_state)))) :
                  17 word) @@
              (0w
                :word3))
                :word20))
              :word48)) (ARB :dharma8_state)).paddress)`;

val mmu_int_invariant_def = Define `
mmu_int_invariant mmu_state policy policy_pt =
((mmu_state.ST2 = final) ==>
 (mmu_state.DESC2.fault.typ = Fault_None) ==>
  (mmu_state.MEMREQ.write ==> (!desc1 .
     (¬policy (combine_descriptors(desc1, mmu_state.DESC2) ARB ).paddress)) /\
  (policy_pt mmu_state.PENDINGWALK.desc.paddress)))
/\
((mmu_state.ST2 = walk) ==> (
 (policy_pt mmu_state.PENDINGWALK.desc.paddress)
 ))
/\
((mmu_state.ST2 = init) ==> (
  (mmu_state.W2.st = 2)  /\
  (mmu_state.W2.n = (w2n mmu_state.VTCR.T0SZ)) /\
  (~mmu_state.W2.up) /\
  (mmu_intermediate_walk_inv mmu_state policy_pt)
))
/\
((mmu_state.ST2 = mem) ==> (
  (mmu_state.W2.st = 2)  /\
  (mmu_state.W2.n = (w2n mmu_state.VTCR.T0SZ)) /\
  (policy_pt mmu_state.DESC2.paddress)
)) /\
(
 ((* mmu_state.W2 is built on top of the request of the MMU1:mmu_state.MEMREQ *)
  (* Notice that this is not the initial request of the core, that is passed as parameter *)
  (* as curr_req *)
  (* so except if the MMU1 is performing a page walk, *)
  (* curr_req and mmu_state.MEMREQ access types are in sync *)
  (* (mmu_state.W2.acctype <> AccType_PTW) /\ *)
  (* (mmu_state.W2.acctype <> AccType_IFETCH) /\ *)
  (* (mmu_state.ST2 <> final) /\ *)
  (* This is obviously not possible for wait, since it is where W2 is initialized *)
  (mmu_state.ST2 <> wait)
 ) ==>
 ( (mmu_state.W2.wr = mmu_state.MEMREQ.write) /\
   (mmu_state.W2.acctype = mmu_state.MEMREQ.acctype)
 )
) /\
 (* When the first stage sends a PTW, then it also reset the erite flag *)
 ((mmu_state.MEMREQ.acctype = AccType_PTW)
   ==> (~mmu_state.MEMREQ.write))
 /\
(* Part of the static configutaion *)
(* number of leading zeros for virtual addresses, size offset for region addressed by VTTBR_EL2 *)
(mmu_state.VTCR.T0SZ = 32w) /\
(* granule size for translation (00: 4KB, 01: 64 KB, 10: 16KB) *)
(mmu_state.VTCR.TG0 = 0w)  /\
(* starting level of Stage 2 page table walk, depending on TG0, skip 2-SL0 levels *)
(mmu_state.VTCR.SL0 = 1w) /\
((rec'mm_feat (reg'mm_feat mmu_state.IDMM2)).TGRAN4 = 0w) /\
(* Stage2 is not disabled *)
(~ Disabled_ST2 mmu_state) /\
(?ASID BADDR. (!b.(policy_pt
  (8w * (3w && b) +
   if ARB.MMUISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO then align (BADDR,5)
   else BADDR))) /\
  (* This basically states that the whole MB is reserved *)
  (* by the page table *)
  (!ia:17 word.(policy_pt
  ((0w:word28 @@ ((ia @@ (0w:word3)):word20)) +
   if ARB.MMUISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO then align (BADDR,5)
   else BADDR))) /\
  (mmu_state.VTTBR  = TTBRType ASID BADDR)
  ) /\
T`;

val _ = export_theory();
