open HolKernel boolLib bossLib Parse;
open lcsymtacs;

open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open armCombineTheory;
open invariantsIntTheory;

(*

HOL_Interactive.toggle_quietdec();
open lcsymtacs;
open dharma8Theory;
open wordsLib;
open guancioTacticsLib;
open guancioTactics2Lib;
open armCombineTheory;
open invariantsIntTheory;
HOL_Interactive.toggle_quietdec();

*)

val _ = new_theory "MMU_StartWalk_ST2_int";


val MMU_StartWalk_ST2_int_def = ``
(mmu_state.ST2 = init) ==>
mmu_int_invariant mmu_state policy policy_pt ==>
let (mmu_state, s) = MMU_StartWalk_ST2 mmu_state ARB in
mmu_int_invariant mmu_state policy policy_pt 
``;



val ADfromResult_fault_thm = prove(``! x y z f g. (ADfromResult (x,y,z,f) g).fault = y``,
  (srw_tac [][ADfromResult_def])
  \\ (Q.ABBREV_TAC `cnd = (x.st = 2) ∧ x.el < 2 ∧ (x.attr.typ = MemType_Normal) ∧
           ((x.acctype = AccType_IFETCH) ∧ z ∨
            f ∧ x.acctype ≠ AccType_IFETCH)`)
  \\ (Cases_on `cnd`)
  \\ (DEABBREV_AND_FIX_TAC)
  \\ (fs [])
);

val MMU_StartWalk_ST2_int_thm = store_thm(
    "MMU_StartWalk_ST2_int_thm",
    ``^MMU_StartWalk_ST2_int_def``,
  (srw_tac [] [])
  \\ (RULE_ASSUM_TAC (SIMP_RULE (srw_ss()) [mmu_int_invariant_def]))
  \\ (FULL_SIMP_TAC (srw_ss()) [MMU_StartWalk_ST2_def])
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RW_TAC (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])
  
  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [prepare_initial_lookup_def])

  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [NoFaultfromWalk_def])
  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF, NoFault_def, CreateFaultRecord_def] thm)))
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `v`])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_VAR)

  \\ LET_NORM_TAC
  \\ (rpt FIX_ABBREV_TAC)
  \\ LET_NORM_TAC
  \\ (RM_ABBR_VAR)

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC

  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `TxSZ`])

  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
  \\ (LET_NORM_TAC)
  \\ (TERM_PAT_ASSUM_W_THM ``Abbrev(a = if b then c else d)`` (fn a::b::c =>
     Q.ABBREV_TAC `cnd=^b`))

  (* It is not a problem if we have a translation fault *)
  \\ (Cases_on `cnd`)
  THENL [
        (FULL_SIMP_TAC (srw_ss()) [])
        \\ (`mmu_state'.DESC2.fault.typ <> Fault_None` by ALL_TAC)
        THENL [(fs [])
               \\ (rpt FIX_ABBREV_TAC)
               \\ (fs [Abbr `mmu_state'`, Abbr `r1'`, Abbr `s'`])
               \\ (fs [ADfromResult_def])
               \\ (Cases_on `(r0'.st = 2) ∧ r0'.el < 2 ∧ (r0'.attr.typ = MemType_Normal) ∧   ((r0'.acctype = AccType_IFETCH) ∧ mmu_state.HCR2.ID ∨ mmu_state.HCR2.CD ∧ r0'.acctype ≠ AccType_IFETCH)`)
               THENL [
                     (fs []), (fs [])
               ]
               \\ (Cases_on `(r0'.st = 2) ∧ r0'.el < 2 ∧ (r0'.attr.typ = MemType_Normal) ∧ mmu_state.HCR2.CD`)
               THENL [
                     (fs []), (fs [])
               ],
               ALL_TAC
        ]
        \\ (DEABBREV_AND_FIX_TAC)
        \\ (fs [mmu_int_invariant_def, Disabled_ST2_def]),
        ALL_TAC
  ]

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s0`])
  \\ LET_NORM_TAC
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'`])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (RM_ABBR_VAR)

  \\ LET_NORM_TAC
  \\ (rpt FIX_ABBREV_TAC)
  \\ LET_NORM_TAC
  \\ (RM_ABBR_VAR)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s'0'`])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ (rpt FIX_ABBREV_TAC)
  \\ (RM_ABBR_VAR)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `s30`])

  \\ (Cases_on `v.typ <> Fault_None`)
  THENL [
     (FULL_SIMP_TAC (srw_ss()) [])
     \\ (fs [])
     \\ (rpt FIX_ABBREV_TAC)
     \\ (RM_ABBR_VAR)
     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
     \\ (rpt FIX_ABBREV_TAC)
     \\ (fs [Abbr `mmu_state'`, mmu_int_invariant_def])
     \\ (`((ADfromResult (s10'',v,mmu_state.HCR2.ID,mmu_state.HCR2.CD) s).fault.
 typ <>  Fault_None)` by ALL_TAC)
     THENL [
           (fs [ADfromResult_fault_thm]),
           ALL_TAC
     ]
     \\ (DEABBREV_AND_FIX_TAC)
     \\ (fs [Disabled_ST2_def]),
     ALL_TAC
  ]


  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (fs [])

  \\ (rpt FIX_ABBREV_TAC)
  \\ (RM_ABBR_VAR)
  \\ (RM_ABBR_VAR)
  \\ (RM_ABBR_CONST)
  \\ (FULL_SIMP_TAC (srw_ss()) [Abbr `r0'`])

  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

  \\ (rpt FIX_ABBREV_TAC)
  \\ (fs [Abbr `s10`])
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
  \\ (DEABBREV_AND_FIX_TAC)
  \\ (fs [mmu_int_invariant_def])

  (* Remains to check that we output a ptdesc in the right region *)
  (* \\ (`^tmp` by cheat) *)
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [Disabled_ST2_def, mmu_intermediate_walk_inv_def])
  \\ (fs [])
);


(* ( *)
(* (SIMP_RULE (srw_ss()) [NoFaultfromWalk_def, LET_DEF, NoFault_def, CreateFaultRecord_def]) o *)
(* (SIMP_RULE (srw_ss()) [ASSUME `` *)
(* (check_address_size *)
(*                      (NoFaultfromWalk mmu_state.W2 (ARB :dharma8_state), *)
(*                       (w2w *)
(*                          (PTO_ST2 (mmu_state.W2,mmu_state) *)
(*                             (ARB :dharma8_state) + *)
(*                           (((0w :word28) @@ *)
(*                             (((v2w *)
(*                                  (px *)
(*                                     ((((47 :num) >< (0 :num)) *)
(*                                         mmu_state.W2.va :word48), *)
(*                                      mmu_state.W2.l,mmu_state.W2.br, *)
(*                                      mmu_state.W2.n, *)
(*                                      mmu_state.W2.conc ∧ *)
(*                                      (mmu_state.W2.l = *)
(*                                       (4 :num) − *)
(*                                       nlu *)
(*                                         (mmu_state.W2.n,mmu_state.W2.br, *)
(*                                          mmu_state.W2.conc) *)
(*                                         (ARB :dharma8_state)))) : *)
(*                                  17 word) @@ *)
(*                              (0w *)
(*                                :word3)) *)
(*                                :word20)) *)
(*                              :word48)) :word64),mmu_state.W2.m) *)
(*                      (ARB :dharma8_state)).typ = Fault_None *)
(* ``]) o *)
(*  (SIMP_RULE (srw_ss()) [LET_DEF, *)
(*  ASSUME ``~( *)
(*  (64 > mmu_state.W2.n + 39 ∨ *)
(*                             64 < mmu_state.W2.n + 16 ∧ *)
(*                             ARB.MMUISW.IMPL_VT0SZ_LESS_THAN_16_FAULT) ∨ *)
(*                            SL0fault mmu_state ARB ∨ *)
(*                            TAKE (64 − mmu_state.W2.n) *)
(*                              (w2v mmu_state.W2.va) ≠ *)
(*                            if mmu_state.W2.up then *)
(*                              onevec (64 − mmu_state.W2.n) *)
(*                            else nullvec (64 − mmu_state.W2.n)) *)
(* ``]) o *)
(*  (SIMP_RULE (srw_ss()) [prepare_initial_lookup_def]) o *)
(*  (SIMP_RULE (srw_ss()) [ASSUME ``mmu_state.ST2 = init``]) o *)
(*  EVAL_RULE o *)
(*  (SIMP_CONV (srw_ss()) [MMU_StartWalk_ST2_def])) *)
(* ``(FST (MMU_StartWalk_ST2 mmu_state ARB)).DESC2.paddress``; *)



val _ = export_theory();
