open HolKernel boolLib bossLib Parse;
open lcsymtacs;

open dharma8Theory;
open wordsLib;

open guancioTacticsLib;
open guancioTactics2Lib;

open armCombineTheory;
open invariantsIntTheory;

open MMU_Init_ST2_intTheory;
open MMU_StartWalk_ST2_intTheory;
open MMU_FetchDescriptor_ST2_intTheory;
open MMU_ExtendWalk_ST2_intTheory;
open MMU_Complete_ST2_intTheory;

(*

HOL_Interactive.toggle_quietdec();
open lcsymtacs;
open dharma8Theory;
open wordsLib;
open guancioTacticsLib;
open guancioTactics2Lib;
open armCombineTheory;
open invariantsIntTheory;
open MMU_Init_ST2_intTheory;
open MMU_StartWalk_ST2_intTheory;
open MMU_FetchDescriptor_ST2_intTheory;
open MMU_ExtendWalk_ST2_intTheory;
open MMU_Complete_ST2_intTheory;
HOL_Interactive.toggle_quietdec();

*)

val _ = new_theory "MMU_ST2_sched_int";


val MMU_ST2_sched_int_def = ``
(* This depends on how MMU_ST2_sched is invoked by MMU_sched *)
((mmu_state.ST2 <> wait) = (omem = NONE)) ==>
((omem <> NONE) ==> (
 (ptw = ((THE omem).acctype = AccType_PTW)) /\
 ((((THE omem).acctype <> AccType_PTW)) ==>
  ((curr_req.acctype = (THE omem).acctype) /\
   (curr_req.write = (THE omem).write)
  ))
  /\
 (((THE omem).acctype = AccType_PTW) ⇒ ¬(THE omem).write)
))==>
(mmu_int_invariant mmu_state policy policy_pt) ==>
(* This part depends on the configuration of pts *)
(!w f walk .
(lookup <> NONE) ==>
(
 ((w,f) =walk_extend
         (walk,cast_desc (v2w (THE lookup).data) ARB,
          ARB.MMUISW.IMPL_CONTIGUOUS_BIT_FAULT, ARB) ARB) ==>
 ((w.acctype = walk.acctype) /\
  (w.wr = walk.wr) /\
  (
    (w.c) ==>
    (walk.wr) ==>
    ((check_walk_result (w,8,mmu_state.HCR2.PTW,ARB) ARB).typ = Fault_None) ==>
    (¬policy w.adr)
  ) /\
  (
    (~w.c) ==>
    ((w.st = 2) ∧ (w.n = 32) ∧ (policy_pt w.adr))
  )
(* we are assuming that we have only one level of pt *)
  (* /\ w.c *)
 ))
(* We must know something of w if the translation failed *)
 ) ==>
let s' = MMU_ST2_sched (lookup,omem,curr_req,desc1,ptw,mmu_state) ARB in
let ((message, cons, mmu_state), s'') = s' in
let goal = 
(
(mmu_int_invariant mmu_state policy policy_pt) /\
(mmu2mem_policy message policy)
) in
goal
``;


val MMU_ST2_sched_int_thm = store_thm(
    "MMU_ST2_sched_int_thm",
    ``^MMU_ST2_sched_int_def``,
  (srw_tac [] [])
  \\ (full_simp_tac (srw_ss()) [MMU_ST2_sched_def])
  \\ LET_NORM_TAC
  (* use the existing variables *)
  \\ (rev_full_simp_tac (srw_ss()) [])
  (* remove unused variabled *)
  \\ (rw_tac (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ NORMALIZE_VAR_TAC
  \\ RM_ABBR_VAR

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR
  \\ RM_ABBR_CONST

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC

  \\ (POP_ASSUM (fn thm => ASSUME_TAC (
          SIMP_RULE (srw_ss()) [LET_DEF] thm)))

  \\ (`s0' = F` by (DEABBREV_AND_FIX_TAC \\ (Cases_on `IS_SOME omem`) \\ (fs [])))
  \\ (`s1 = NONE` by (DEABBREV_AND_FIX_TAC \\ (Cases_on `IS_SOME omem`) \\ (fs [])))
  \\ (`s2 = s0` by (DEABBREV_AND_FIX_TAC \\ (Cases_on `IS_SOME omem`) \\ (fs [])))
  \\ (`s4 = ARB` by (DEABBREV_AND_FIX_TAC \\ (Cases_on `IS_SOME omem`) \\ (fs [])))
  (* This is usefoul to remove several case analysis on omem *)
  \\ (`s3.ST2 = mmu_state.ST2` by (
        DEABBREV_AND_FIX_TAC
        \\ (Cases_on `~(IS_SOME omem)`)
        THENL [(fs []), (fs [])]))

  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (RW_TAC (srw_ss()) [])

  \\ LET_NORM_TAC
  \\ LET_NORM_TAC
  \\ RM_ABBR_VAR

  (* cnd = mmu_state.ST2 ≠ walk ∨ lookup ≠ NONE *)
  \\ (TERM_PAT_ASSUM_W_THM ``Abbrev(a = if b then c else d)`` (fn a::b::c =>
     Q.ABBREV_TAC `cnd=^b`))

  \\ (Cases_on `~cnd`)

  THENL [
        (fs [])
        \\ DEABBREV_AND_FIX_TAC
        \\ (fs [mmu_int_invariant_def])
        \\ (Cases_on `IS_SOME omem`)
        THENL [
           (fs [mmu2mem_policy_def]) \\
           (REV_FULL_SIMP_TAC (srw_ss()) []),
           (fs [mmu2mem_policy_def])
        ]
        ,ALL_TAC
  ]
  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (Cases_on `mmu_state.ST2`)

  THENL [
     (* MMU_Init_ST2_def *)
     (FULL_SIMP_TAC (srw_ss()) [])
     \\ (rpt LET_NORM_TAC)
     \\ (DEABBREV_AND_FIX_TAC)
     \\ (RW_TAC (srw_ss()) [mmu2mem_policy_def])
     \\ (`(IS_SOME omem)` by ALL_TAC)
     THENL [
       METIS_TAC [optionTheory.NOT_IS_SOME_EQ_NONE],
       ALL_TAC
     ]
     \\ (fs [])

     \\ (ASSUME_TAC((
         (SPEC ``desc1.paddress``) o (GEN ``ipaddress:word48``) o
         (SPEC ``mmu_state with MEMREQ := THE omem``) o (GEN ``mmu_state:MMU_CONFIG2``)
       ) MMU_Init_ST2_int_thm))
     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
     \\ (`mmu_int_invariant (mmu_state with MEMREQ := THE omem) policy policy_pt` by ALL_TAC)
     THENL [
       (fs [mmu_int_invariant_def, Disabled_ST2_def]),
       ALL_TAC
     ]

     \\ (fs [])
     \\ (Q.ABBREV_TAC `s= (MMU_Init_ST2
            ((THE omem).acctype = AccType_PTW,desc1.paddress,curr_req,
             mmu_state with MEMREQ := THE omem) ARB)`)

     \\ (RW_TAC (srw_ss()) [])
     \\ (fs [])
     ,
     ALL_TAC, ALL_TAC, ALL_TAC, ALL_TAC]

  THENL [
     (* MMU_StartWalk_ST2_def *)
     (FULL_SIMP_TAC (srw_ss()) [])
     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
     \\ (fs [Abbr `s3`])
     \\ (`~ (Disabled_ST2 mmu_state)` by ALL_TAC)
     THENL [
           (fs [mmu_int_invariant_def]),
           ALL_TAC
     ]

     \\ (fs [])

     \\ (ASSUME_TAC MMU_StartWalk_ST2_int_thm)

     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
     \\ (fs [])

     \\ (DEABBREV_AND_FIX_TAC)
     \\ (Q.ABBREV_TAC `s= MMU_StartWalk_ST2 mmu_state ARB`)
     \\ (LET_NORM_TAC)
     \\ (fs [])
     \\ (RW_TAC (srw_ss()) [mmu2mem_policy_def])

     ,ALL_TAC, ALL_TAC, ALL_TAC]

  THENL [
     (* MMU_FetchDescriptor_ST2_def *)
     (FULL_SIMP_TAC (srw_ss()) [])
     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
     \\ (fs [Abbr `s3`])

     \\ (ASSUME_TAC((
         (SPEC ``curr_req: MEM_REQUEST``) o (GEN ``req: MEM_REQUEST``)
       ) MMU_FetchDescriptor_ST2_int_thm))

     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
     \\ (DEABBREV_AND_FIX_TAC)
     \\ (Q.ABBREV_TAC `s= MMU_FetchDescriptor_ST2 (curr_req,mmu_state) ARB`)
     \\ (LET_NORM_TAC)
     \\ (fs [])

     ,ALL_TAC, ALL_TAC]

  THENL [
     (* MMU_ExtendWalk_ST2_def *)
     (FULL_SIMP_TAC (srw_ss()) [])
     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
     \\ (fs [Abbr `s3`])
     \\ (`IS_SOME lookup` by (METIS_TAC [optionTheory.NOT_IS_SOME_EQ_NONE]))
     \\ (fs [Abbr `s0`])
     \\ (`lookup <> NONE` by (METIS_TAC [optionTheory.NOT_IS_SOME_EQ_NONE]))
     \\ (fs [])

     \\ (ASSUME_TAC((
         (SPEC ``(THE lookup): MEM_REQUEST``) o (GEN ``lookup: MEM_REQUEST``)
       ) MMU_ExtendWalk_ST2_int_thm))

     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

     (* Prove the precondition *)
     \\ (TERM_PAT_ASSUM_W_THM ``a ==> b`` (fn a::b::[] =>
         (Q.ABBREV_TAC `cnd1=^a`)))
     \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

     \\ (DEABBREV_AND_FIX_TAC)
     \\ (Q.ABBREV_TAC `s= (MMU_ExtendWalk_ST2 (THE lookup,mmu_state) ARB)`)
     \\ (LET_NORM_TAC)
     \\ (fs [mmu2mem_policy_def])

     ,ALL_TAC
  ]
  (* MMU_Complete_ST2 *)

  \\ (FULL_SIMP_TAC (srw_ss()) [])
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])
  \\ (fs [Abbr `s3`])
  \\ (ASSUME_TAC MMU_Complete_ST2_integrity_thm)
  \\ (REV_FULL_SIMP_TAC (srw_ss()) [])

  \\ (DEABBREV_AND_FIX_TAC)
  \\ (Q.ABBREV_TAC `s= (MMU_Complete_ST2 (desc1,mmu_state) ARB)`)
  \\ (LET_NORM_TAC)
  \\ (fs [])
);

val _ = export_theory();
