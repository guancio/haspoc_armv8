structure guancioTactics2Lib :> guancioTactics2Lib =
struct

open HolKernel boolLib bossLib
open pairLib;
open pairTools;
open blastLib;
open guancioTacticsLib;


val new_let_tac =
LAST_ASSUM (fn thm =>
  let
      val _ = print_term (concl thm)
      val t = find_term is_let (concl thm)
      val body::value::[] = my_match ``LET a b`` t
      val (var, f) = dest_pabs body
      val var_list = strip_pair var
      val val_list = strip_pair value
  in
    (* this is a single variable *)
    (* Let x = f a b c in g(c) ==> Ab(x=f a b c) /\ g(c) *)
    if (List.length var_list) = 1 then
      (PAT_ASSUM (concl thm) (fn thm1 => (LET_X_TO_ONE_TAC t thm1)))
    (* this is a let (a,b) = (x,y) in C  ==>  {x/a, y/b}C*)
    else if ((List.length var_list) = (List.length val_list)) then
      (PAT_ASSUM (concl thm) (fn thm1 =>
         ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm1)))
    (* this is a let (a,b) = x in C  ==>  (a,b) = x /\ C*)
    else
      (PAT_ASSUM (concl thm) (fn thm1 =>
        LET_ONE_TO_TUPLE_TAC t thm1))
  (*      *)
  (*     if (not (is_pair  var)) then *)
  (*       (PAT_ASSUM (concl thm) (fn thm1 => (LET_X_TO_ONE_TAC t thm1))) *)
  end
  );

val split_pair_tac =
LAST_ASSUM (fn thm =>
  let val t = find_term (fn t=> (is_var t) andalso
                                (((List.length o strip_prod o type_of) t) > 1))
                        (concl thm)
      val _ = print_term t
  in
    PairCases_on `^t`
  end
  );

val split_pair_tac =
proof_free_vars (fn vars =>
     let val vars = List.filter (fn t=> ((List.length o strip_prod o type_of) t) > 1) vars
     in
        if (List.length vars) > 0 then
          PairCases_on `^(List.hd vars)`
        else FAIL_TAC "no more split"
     end);


val trm = ``((used_core,used_mem,rpl2core,rpl2mem,mmu_state),no_state) =
             mmu_interface (core2mmu,mem2mmu,mmu_state) ARB``;

fun ABBREV_TUPLE vars value =
 proof_free_vars (fn all_vars =>
    let val var = variant all_vars (mk_var ("my_temp_var", type_of value))
    in
        (Q.ABBREV_TAC `^var=^value`)
        \\ (pairLib.PairCases_on `^var`)
        \\ (PAT_ASSUM ``Abbrev (a = ^var)`` (fn thm =>
           let val new_vars = (fst o dest_eq o snd o dest_comb o concl) thm
           		 val (s,_) = match_term vars new_vars
           in
             (ASSUME_TAC thm)
             \\ (MAP_EVERY (fn v =>
                  Q.ABBREV_TAC `^(variant all_vars (#redex v))=^(#residue v)`
                  ) s)
             \\ (MAP_EVERY (fn v =>
                       PAT_ASSUM ``Abbrev (x=y)`` (fn thm => ALL_TAC)
	       	     ) s)
           end
        ))
    end
 );

fun ABBREV_VAR trm =
    proof_free_vars (fn vars =>
    let val (var, value) = dest_eq trm
        val var = variant (vars@(free_vars value)) var
    in
      (Q.ABBREV_TAC `^var = ^value`)
    end);

val LET_TUPLE_TAC = (FIRST_ASSUM (fn thm =>
    let
      val t = find_term is_let (concl thm)
      val body::value::[] = my_match ``LET a b`` t
      val (var, f) = dest_pabs body
    in
      (full_simp_tac (bool_ss) [REWRITE_CONV [Once LET_DEF] t])
      \\ (ABBREV_TUPLE var value)
      \\ (full_simp_tac (srw_ss()) [])
    end
  ));

val LET_VAR_TAC =  (FIRST_ASSUM (fn thm =>
    let
      val t = find_term is_let (concl thm)
      val body::value::[] = my_match ``LET a b`` t
      val (var, f) = dest_pabs body
    in
      (ABBREV_VAR ``^var = ^value``)
      \\ (PAT_ASSUM ``a = b`` (fn thm => ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm)))
    end));

val LET_NORM_TAC =
   (* The first part split the tuple to avoid SND FST etc *)
   ((split_pair_tac \\ (rpt split_pair_tac) \\ (full_simp_tac (srw_ss()) []) \\ (rpt SPLIT_ABBREVS))
    ORELSE
   (FIRST_ASSUM (fn thm =>
    let
      val t = find_term is_let (concl thm)
      val body::value::[] = my_match ``LET a b`` t
      val (var, f) = dest_pabs body
    in
      if (is_pair var) then
         (full_simp_tac (bool_ss) [REWRITE_CONV [Once LET_DEF] t])
         \\ (ABBREV_TUPLE var value)
         \\ (full_simp_tac (srw_ss()) [])
      else
         (full_simp_tac (bool_ss) [REWRITE_CONV [Once LET_DEF] t])
         \\ (ABBREV_VAR (``^var = ^value``))
         \\ (full_simp_tac (srw_ss()) [])
    end))
    );

val NORMALIZE_VAR_TAC =  (EVERY_ASSUM (fn thm =>
     if (not o markerSyntax.is_abbrev o concl) thm then ALL_TAC
     else
     let val (var, trm) = markerSyntax.dest_abbrev (concl thm)
         val var1 = List.filter (fn v => v = var) ["no_state",  "used_core", "used_mem", "rpl2core", "rpl2mem", "mmu_state'",  "mmu2core",   "mmu2mem'", "mmu_state'"
          ]
     in
        if (List.length var1) = 0 then ALL_TAC
        else if (not (is_var trm)) then ALL_TAC
        else
         PAT_ASSUM (concl thm) (fn thm1=>
            ASSUME_TAC (SYM (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm1)))
     end
     ))
  \\ (full_simp_tac (srw_ss()) [])
  \\ (EVERY_ASSUM (fn thm =>
     if (not o is_eq o concl) thm then ALL_TAC
     else
     let val (var1, var2) = dest_eq (concl thm) in
         if (not (is_var var2)) then ALL_TAC
         else if (not (is_var var1)) then ALL_TAC
         else
         let val vars = List.filter (fn v => v = (fst o dest_var) var2) ["no_state",  "used_core", "used_mem", "rpl2core", "rpl2mem", "mmu_state'",  "mmu2core",   "mmu2mem'", "mmu_state'"] in
            if (List.length vars) = 0 then ALL_TAC
            else PAT_ASSUM (concl thm) (fn thm1=> ALL_TAC)
         end
     end
     ));


val RM_ABBR_CONST = (EVERY_ASSUM (fn thm =>
     if (not o markerSyntax.is_abbrev o concl) thm then ALL_TAC
     else
     let val (var, trm) = markerSyntax.dest_abbrev (concl thm)
         val var = (fst o dest_eq o snd o dest_comb o concl) thm
     in
         if (not (is_const trm)) then ALL_TAC
         else if (not o is_var) var then ALL_TAC
         else (FULL_SIMP_TAC (srw_ss()) [Abbr `^var`])
     end
     ));

val RM_ABBR_VAR = (EVERY_ASSUM (fn thm =>
     if (not o markerSyntax.is_abbrev o concl) thm then ALL_TAC
     else 
     let val (var, trm) = markerSyntax.dest_abbrev (concl thm)
         val var = (fst o dest_eq o snd o dest_comb o concl) thm
     in
         if (not (is_var trm)) then ALL_TAC
         else if (not o is_var) var then ALL_TAC
         else TRY (FULL_SIMP_TAC (srw_ss()) [Abbr `^var`])
     end
     ));

val RM_ABBR_TRUE_VAR = (EVERY_ASSUM (fn thm =>
    if (not o is_comb o concl) thm then ALL_TAC
    else
      let val (f,var) = (dest_comb o concl) thm
     in
         if (not (is_var var)) then ALL_TAC
         else if (f <> ``Abbrev``) then ALL_TAC
         else
         ((print_term var);
          (print_thm (Once (ISPEC var markerTheory.Abbrev_def)));
          (PAT_ASSUM (concl thm) (fn thm1 =>
            (ASSUME_TAC (SIMP_RULE (srw_ss()) [markerTheory.Abbrev_def] thm1))))
         )
     end
     ));


val NEW_LET_NORM_TAC =  (FIRST_ASSUM (fn thm =>
    let
      val t = find_term is_let (concl thm)
      val body::value::[] = my_match ``LET a b`` t
      val (var, f) = dest_pabs body
    in
         (* needed to move the theorem as last assumption *)
         (PAT_ASSUM (concl thm) ASSUME_TAC)
         \\ (ABBREV_VAR (``^var = ^value``))
         \\ (POP_ASSUM (fn thm_abbr =>
            (POP_ASSUM (fn thm =>
             (ASSUME_TAC (SIMP_RULE (srw_ss()) [Once LET_DEF] thm))
             \\ (ASSUME_TAC thm_abbr)
            ))))
    end));

val NEW_RM_ABBR_VAR = (rpt (FIRST_ASSUM (fn thm =>
     if (not o markerSyntax.is_abbrev o concl) thm then FAIL_TAC "NO ABBREV"
     else 
     let val (var, trm) = markerSyntax.dest_abbrev (concl thm)
         val var = (fst o dest_eq o snd o dest_comb o concl) thm
     in
         if (not (is_var trm)) then FAIL_TAC "NOT VAR DX"
         else if (not o is_var) var then FAIL_TAC "NOT VAR SX"
         else (FULL_SIMP_TAC (srw_ss()) [Abbr `^var`])
     end
     )));

val INVERT_ABBR_CONST = (EVERY_ASSUM (fn thm =>
         if (not o is_comb o concl) thm then ALL_TAC
         else
          let val (f,exp) = (dest_comb o concl) thm in
          if (f <> ``Abbrev``) then ALL_TAC
          else if (not (is_eq exp)) then ALL_TAC
          else
            let val (a,b) = dest_eq exp in
                if (not (is_const a)) then ALL_TAC
                else PAT_ASSUM (concl thm) (fn thm1=>
            ASSUME_TAC (REWRITE_RULE [SYM_CONV exp] thm1))
            end
          end
          ));


end