signature guancioTactics2Lib =
sig

include Abbrev

val new_let_tac : tactic
val split_pair_tac : tactic
val ABBREV_TUPLE : term -> term -> term list * term -> goal list * validation
val ABBREV_VAR : term -> term list * term -> goal list * validation
val LET_TUPLE_TAC : tactic
val LET_VAR_TAC : tactic
val LET_NORM_TAC : tactic
val NORMALIZE_VAR_TAC : goal -> goal list * (thm list -> thm)
val RM_ABBR_CONST : tactic
val RM_ABBR_VAR : tactic
val RM_ABBR_TRUE_VAR : tactic

val NEW_LET_NORM_TAC : tactic
val NEW_RM_ABBR_VAR : tactic
val INVERT_ABBR_CONST : tactic

end
 