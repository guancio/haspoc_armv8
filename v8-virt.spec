-----------------------------------------------------------------------
-- This a definition of the ARMv8 virtualization extensions for      --
-- hypervisors and the stage 2 address translation                   --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2014-11-05	factoring out into separate file


{-

val () = Runtime.LoadF "v8-base.spec, v8-mem.spec, v8-mmu.spec, v8-virt.spec";

-}


---------------------------
-- Concatenated Page Tables
---------------------------

nat n_for_SL0 (MMU :: MMU_CONFIG2) = 
{
    var tsz = [MMU.VTCR.T0SZ]::nat;

    when (tsz < 16 and MMUISW.IMPL_VT0SZ_LESS_THAN_IS_16_FOR_SL0) do tsz <- 16; -- min 16

    return VAlen(tsz)
}


-- faulty SL0 settings connected to implemented physical address size, and VTCR.T0SZ
bool SL0fault (MMU :: MMU_CONFIG2) = 
{
    b = MMU.W2.br;
    sl0 = [MMU.VTCR.SL0] :: nat;
    n = n_for_SL0(MMU);
    impn = ps2len(MMU.IDMM2.PAR<2:0>, 48);
    min_level, margin = match b
    {
	case 9  => 2, 4
	case 11 => 1, 4
	case _  => 1, 1
    };
    border_2 = (min_level + 2)*b + b+3 + margin;
    not_supported = sl0 > 2 or sl0 == 2 and impn <= border_2;
    bits_resolved = (min_level + sl0)*b + b+3;
    size_mismatch = n > bits_resolved + 4 or n <= bits_resolved - b;
    return not_supported or size_mismatch
}

-- when we should concatenate first level page tables, assumes that n is not to big, would cause translation fault before anyway
bool concatenated (sl0 :: nat, n :: nat, b :: nat) =
{
    min_level = if b==9 then 2 else 1;
    return n > (min_level + sl0)*b + b+3
}


-------------------------
-- Stage 2 specific stuff
-------------------------

bool Disabled_ST2(MMU :: MMU_CONFIG2) = !(MMU.HCR2.DC or MMU.HCR2.VM)

ADR PTO_ST2 (w::Walk, MMU :: MMU_CONFIG2) = return page_table_origin(nflb(w.n,w.br,w.conc),MMU.&VTTBR,MMUISW.IMPL_TTBR_MISALIGNED_TREAT_AS_ZERO)

bits(3) PS_ST2(MMU :: MMU_CONFIG2) = if MMU.VTCR.PS >+ MMU.IDMM2.PAR<2:0> then MMU.IDMM2.PAR<2:0> else MMU.VTCR.PS


nat TSZ_ST2(MMU :: MMU_CONFIG2) = 
{
    var tsz = [MMU.VTCR.T0SZ] :: nat;

    when (tsz < 16 and !MMUISW.IMPL_VT0SZ_LESS_THAN_16_FAULT) do tsz <- 16; -- min 16
    return tsz
}

-- TG field defining translation granule size
bits(2) TG_ST2(MMU :: MMU_CONFIG2) = 
{
    tg = MMU.VTCR.TG0;
    
    return (if TGImplemented(MMU.&IDMM2,tg) then tg else MMUISW.IMPL_VTCR_TG_DEFAULT)
}

MemoryAttributes VTCRtoAttr(acctype :: AccType, el :: nat, MMU :: MMU_CONFIG2) = 
{
    sh, orgn, irgn = MMU.VTCR.SH0, MMU.VTCR.ORGN0, MMU.VTCR.IRGN0;

    CD = (el < 2) and match acctype {
	case AccType_IFETCH => MMU.HCR2.ID 
	case _ => MMU.HCR2.CD
    };

    var attr::MemoryAttributes;
    attr.outer <- ShortConvertAttrsHints(orgn, !CD);
    attr.inner <- ShortConvertAttrsHints(irgn, !CD);
    attr.shareable <- sh<1>;
    attr.outershareable <- sh == '10';
    attr.typ <- MemType_Normal;

    return attr
}

Walk InitWalk_ST2(va :: VA, acctype :: AccType, wr :: bool, el ::  nat, MMU :: MMU_CONFIG2) = 
{
   var w = init_walk(va, acctype, wr, el, 2, TG_ST2(MMU), PS_ST2(MMU), TSZ_ST2(MMU), VTCRtoAttr(acctype, el, MMU), PAlen(MMU.IDMM2), false, true, false);
   w.conc <- concatenated([MMU.VTCR.SL0], w.n, w.br);
   return w
}

-- mapping IPA when ST2 address translation disabled
Walk FlatMap_ST2 (w :: Walk) = 
{
    var walk = w;

    -- set attributes so that they do not affect stage 1 memory type, WB cacheable, non-shareable
    walk.attr.typ <- MemType_Normal;
    walk.attr.inner <- ShortConvertAttrsHints('11', true);
    walk.attr.outer <- ShortConvertAttrsHints('11', true);
    walk.attr.shareable <- false;
    walk.attr.outershareable <- false;

    walk.adr <- w.va<47:0>;
    walk.c <- true;

    return walk
}

-- combining stage 1 and 2 cache attributes
MemAttrHints combine_cacheability (st1 :: MemAttrHints, st2 :: MemAttrHints) = 
{
    var attr;

    attr.attrs <- st1.attrs && st2.attrs;
    attr.hints <- st1.hints;
    attr.transient <- st1.transient;
    
    return attr
}

-- combining stage 1 and 2 memory attributes
MemoryAttributes combine_attributes (st1 :: MemoryAttributes, st2 :: MemoryAttributes) = 
{
    var attr;

    attr <- match st1.typ
    {
	case MemType_Device => st1
	case MemType_Normal => st2
    };

    when (st1.typ == MemType_Device and st1.typ  == st2.typ) do
    {
	attr.device <- match st1.device, st2.device
	{
	    case DeviceType_nGnRnE, _ or _ , DeviceType_nGnRnE => DeviceType_nGnRnE
	    case DeviceType_nGnRE,  _ or _ , DeviceType_nGnRE  => DeviceType_nGnRE
	    case DeviceType_nGRE,   _ or _ , DeviceType_nGRE   => DeviceType_nGRE
	    case DeviceType_GRE, DeviceType_GRE                => DeviceType_GRE
	}
    };

    when (st1.typ == MemType_Normal and st1.typ  == st2.typ) do
    {
	attr.inner <- combine_cacheability(st1.inner, st2.inner);
	attr.outer <- combine_cacheability(st1.outer, st2.outer);
	attr.shareable <- st1.shareable or st2.shareable;
	attr.outershareable <- st1.outershareable or st2.outershareable
    };
    
    return attr
}

-- combine descriptors into new ST1 descriptor, only called if st1 is non-faulty
AddressDescriptor combine_descriptors ( st1 :: AddressDescriptor, st2 :: AddressDescriptor ) = 
{
    var desc = st2;
    when !IsFault(st2) do {
	desc.memattrs <- combine_attributes(st1.memattrs, st2.memattrs);
	-- reset to ST1 NoFault, needed to identify memory requests in scheduling
	desc.fault.secondstage <- false
    };
    return desc
}

---------------------
-- Atomic transitions
---------------------

-- Take current descriptor from ST1 and perform walk init
MMU_CONFIG2 MMU_Init_ST2 (PTW :: bool, ipaddress :: bits(48), curr_req :: MEM_REQUEST, mmu :: MMU_CONFIG2) = 
{
    var MMU = mmu;
    when (MMU.ST2 != wait) do #MMU_SCHED_FAULT;

    va = [ipaddress]`64; --`
    -- it is wrong to set the writa flag if we are translating in behalf
    -- of a page table walk of the stage 1
    write = curr_req.write and !PTW;
    acctype = if PTW then AccType_PTW else curr_req.acctype;
    EL = curr_req.EL;

    MMU.W2 <- InitWalk_ST2(va, acctype, write, EL, MMU);
    MMU.F2 <- NoFaultfromWalk(MMU.W2);

    MMU.ST2 <- init;
    return MMU
}


-- Flat map transition when MMU stage 2 disabled
MMU_CONFIG2 MMU_FlatMap_ST2 (mmu :: MMU_CONFIG2) = 
{
    var MMU = mmu;
    when (MMU.ST2 != init) do #MMU_SCHED_FAULT;

    -- modify relevant parameters
    MMU.W2.n <- 48;
    MMU.W2.m <- PAlen(MMU.IDMM2);
    MMU.W2.l <- 0;

    -- flat map, check if input address size > implemented physical address
    MMU.W2 <- FlatMap_ST2(MMU.W2);
    MMU.F2 <- check_address_size(NoFaultfromWalk(MMU.W2), MMU.W2.va, PAlen(MMU.IDMM2));

    MMU.DESC2 <- ADfromResult(MMU.W2,MMU.F2,MMU.HCR2.ID,MMU.HCR2.CD);

    MMU.ST2 <- final;
    return MMU
}

-- preparation of initial VTTBR lookup / SL0 checking
MMU_CONFIG2 MMU_StartWalk_ST2 (mmu :: MMU_CONFIG2) = 
{
    var MMU = mmu;
    when (MMU.ST2 != init) do #MMU_SCHED_FAULT;

    (w,f) = prepare_initial_lookup(MMU.W2, PTO_ST2(MMU.W2,MMU), SL0fault(MMU), true, MMUISW.IMPL_VT0SZ_LESS_THAN_16_FAULT, false);
    MMU.W2 <- w;
    MMU.F2 <- f;
    if (f.typ == Fault_None) then
    {
	MMU.DESC2 <- ADfromWalk(MMU.W2);
	MMU.ST2 <- mem
    }
    else 
    {
	MMU.DESC2 <- ADfromResult(MMU.W2,MMU.F2,MMU.HCR2.ID,MMU.HCR2.CD);

	MMU.ST2 <- final
    };
    return MMU
}

-- request descriptor from memory
MEM_REQUEST * MMU_CONFIG2 MMU_FetchDescriptor_ST2 (curr_req :: MEM_REQUEST, mmu :: MMU_CONFIG2) = 
{
    var MMU = mmu;
    when (MMU.ST2 != mem) do #MMU_SCHED_FAULT;

    var req :: MEM_REQUEST;

    req.core_id <- curr_req.core_id;
    req.id <- curr_req.id;
    req.va <- MMU.W2.va;
    req.write <- false;
    req.desc <- MMU.DESC2;
    req.bytesize <- 8;
    req.acctype <- AccType_PTW;
    req.ns <- true;

    MMU.PENDINGWALK <- req;
    
    MMU.ST2 <- walk;

    return (req,MMU)
}

MMU_CONFIG2 MMU_ExtendWalk_ST2 (r :: MEM_REQUEST, mmu :: MMU_CONFIG2) = 
{
    var MMU = mmu;
    when (MMU.ST2 != walk) do #MMU_SCHED_FAULT;

    when ((MMU.PENDINGWALK.desc.paddress == r.desc.paddress) and (MMU.PENDINGWALK.write == r.write)) do {
       MMU.F2 <- r.desc.fault;
       when (MMU.F2.typ == Fault_None) do {
     desc = cast_desc([r.data]);
     (w, f) = walk_extend(MMU.W2, desc, MMUISW.IMPL_CONTIGUOUS_BIT_FAULT, UNKNOWN);
     MMU.W2 <- w;
     MMU.F2 <- f;
     when (f.typ == Fault_None and !w.c) do {
      	    MMU.DESC2 <- ADfromWalk(w);
         MMU.ST2 <- mem
     };
     when (f.typ == Fault_None and w.c) do {
         MMU.F2 <- check_walk_result(w, 8, MMU.HCR2.PTW, UNKNOWN)
     }
       };
       when (MMU.F2.typ != Fault_None or MMU.W2.c) do {
     MMU.DESC2 <- ADfromResult(MMU.W2,MMU.F2,MMU.HCR2.ID,MMU.HCR2.CD);
     MMU.ST2 <- final
       }
    };

    return MMU
}

(MEM_REQUEST option) * MMU_CONFIG2 MMU_Complete_ST2 (desc1 :: AddressDescriptor, mmu :: MMU_CONFIG2) = 
{
    var MMU = mmu;

    MMU.MEMREQ.desc <- combine_descriptors(desc1, MMU.DESC2);

    MMU.ST2 <- wait;
    
    var res = Some(MMU.MEMREQ);
    when MMU.MEMREQ.desc.fault.typ != Fault_None or MMU.MEMREQ.acctype == AccType_AT do {
	MMU.ST2RPL <- res;
	res <- None};	
    
    return (res, MMU)
}


---------------------------------
-- MMU automaton for ST1 and ST2
---------------------------------


-- automaton for Stage 2, does not directly reply to core, either sends out final memory request or informs ST1 of fault
(MEM_REQUEST option) * bool * MMU_CONFIG2
MMU_ST2_sched (lookup :: MEM_REQUEST option, omem :: MEM_REQUEST option, curr_req :: MEM_REQUEST, desc1 :: AddressDescriptor, PTW :: bool, mmu :: MMU_CONFIG2) = 
{
    var MMU = mmu;
    var l;
    when IsSome(lookup) do {
	lu = ValOf(lookup);
	l <- lu
    };
    var out_mem = None;
    var consumed = false;

    when IsSome(omem) do MMU.MEMREQ <- ValOf(omem);
    
    when !(MMU.ST2 == walk and lookup == None) do
    {
	match MMU.ST2
	{
	    case wait => MMU <- MMU_Init_ST2(PTW, desc1.paddress, curr_req, MMU)
	    case init => if Disabled_ST2(MMU) then MMU <- MMU_FlatMap_ST2(MMU) else MMU <- MMU_StartWalk_ST2(MMU)
	    case mem  => {(omem, mmu_new) = MMU_FetchDescriptor_ST2(curr_req,MMU); out_mem <- Some(omem); MMU <- mmu_new}
	    case walk => {MMU <- MMU_ExtendWalk_ST2(l,MMU); consumed <- true}
	    case final => {(omem, mmu_new) = MMU_Complete_ST2(desc1,MMU); out_mem <- omem; MMU <- mmu_new}
	}
    };

    return (out_mem, consumed, MMU)
}
   

-- combined automaton
(MEM_REQUEST option) * (MEM_REQUEST option) * bool * MMU_CONFIG
MMU_sched (req :: MMU_REQUEST option, lookup :: MEM_REQUEST option, mmu :: MMU_CONFIG) = 
{
    var MMU = mmu;
    var out_mem = None;
    var out_mmu = None;
    var consumed1 = false;
    
    -- only make another step of ST1 when ST2 is not busy
    when (MMU.MMU2.ST2 == wait and !(MMU.MMU1.ST1 == wait and req == None)) do
    {
	l = if IsSome(MMU.MMU2.ST2RPL) then MMU.MMU2.ST2RPL else lookup;
  -- Simplified since MMU.CURR_REQ is updated in the caller
	-- r = if MMU.MMU1.ST1 == wait then Fst(get_mmureq(req)) else MMU.CURR_REQ;
  r = MMU.CURR_REQ;
	ommu, omem, cons, mmu1_new = MMU_ST1_sched(r,l, MMU.MMU1);
	out_mem <- omem;
	out_mmu <- ommu;
	MMU.MMU1 <- mmu1_new;
	consumed1 <- cons and !(IsSome(MMU.MMU2.ST2RPL));
	MMU.MMU2.ST2RPL <- None
    };

    
    ns10 = (MMU.CURR_REQ.EL < 2 and MMU.CURR_REQ.ns);
    
    -- only step if ST1 is loading a descriptor or has the IPA result, only step in Non-Secure EL1/0
    var step2 = ns10 and ((MMU.MMU2.ST2 != wait) or IsSome(out_mem));
    var consumed2 = false;
    
    -- if ST2 is stepped, ST1 may have sent a request to memory in this step, out_mem is overwritten but ST2 saves the original message
    when step2 do
    {
	omem, cons, mmu2_new = MMU_ST2_sched(lookup, out_mem, MMU.CURR_REQ, MMU.MMU1.DESC1, (MMU.MMU1.ST1 == walk), MMU.MMU2);
	out_mem <- omem; MMU.MMU2 <- mmu2_new;
	-- in some cases (ST2fault, ST2 AT instruction) the result is sent back to the core from ST2 
	when (MMU.MMU1.ST1 == wait and IsSome(MMU.MMU2.ST2RPL)) do {
	    out_mmu <- MMU.MMU2.ST2RPL;
	    MMU.MMU2.ST2RPL <- None};
	consumed2 <- cons
    };


    return (out_mmu, out_mem, consumed1 or consumed2, MMU)
}


MMU_CONFIG init_mmu_regs (mmu :: MMU_CONFIG, req :: MMU_REQUEST) = {
    var MMU = mmu;
    r, R = req;

    MMU.MMU1.SCTLr <- R.sctlr;
    MMU.MMU1.TCR1 <- R.tcr1;
    MMU.MMU1.TCR23 <- R.tcr23;
    MMU.MMU1.TTBR <- R.ttbr;
    MMU.MMU1.MAIr <- R.mair;
    MMU.MMU1.HCR1 <- R.hcr;
    MMU.MMU1.IDMM1 <- R.idmm;
    
    MMU.MMU2.VTTBR <- R.vttbr;
    MMU.MMU2.VTCR <- R.vtcr;
    MMU.MMU2.IDMM2 <- R.idmm;

    -- choose correct write and el parameters for AT instruction
    at, st2 = match r.acctype, r.sopar {
	case AccType_AT, ATrans(_,S2,_,_) => true, S2
	case _, _ => false, false
    };

    -- enable/disable ST2 for AT instruction
    when at do {if st2 then MMU.MMU1.HCR1.VM <- true else {MMU.MMU1.HCR1.VM <- false; MMU.MMU1.HCR1.DC <- false}};  

    MMU.MMU2.HCR2 <- MMU.MMU1.HCR1;

    return MMU
}


-- MMU interface
(MMU_REQUEST option) * (MEM_REQUEST option) * (MEM_REQUEST option) * (MEM_REQUEST option) * MMU_CONFIG
mmu_interface (mmu_req :: MMU_REQUEST option, mem_req ::MEM_REQUEST option, mmu :: MMU_CONFIG) = 
{
    var MMU = mmu;
    var in_core = mmu_req;
    icorer, icoreR = get_mmureq(mmu_req);
    var in_mem = mem_req;

    var out_core = None;
    var out_mem = None;
    var consumed = false;

    -- only step MMU if busy or new request from core received
    when !(MMU.S == mmu_wait and mmu_req == None and mem_req == None) do {
	match MMU.S {
	    case mmu_wait => {
		    -- case split on acc_type for sysops, identify TLBI and instructions that need no translation
		    fw, tlb = match icorer.acctype, icorer.sopar {
			case AccType_TLB, _ => false, true
			-- forward memory barriers and cache flushes by set/way directly to memory
			case AccType_MB, _ or AccType_IC, IFlush(true,_) or AccType_DC, DFlush(_,_,true,_) => true, false
			case _, _ => false, false
		    };

		    -- distinguish between TLB steps, forwarding, and instructions which actually need translation
		    if tlb then { out_core <- Some(icorer); consumed <- true } -- TODO: model TLBI semantics here
		    else {
		       if fw then {out_mem <- Some(icorer); consumed <- true; MMU.S <- mmu_reply }
		       else when mmu_req != None do {
			   MMU <- init_mmu_regs(MMU,ValOf(mmu_req));
			   MMU.CURR_REQ <- icorer;
			   MMU.S <- mmu_trans;
			   
			   o_mmu, o_mem, cons, mmu_new = MMU_sched(mmu_req, mem_req, MMU);
			   out_core <- o_mmu;
			   out_mem <- o_mem;
			   consumed <- cons;
			   MMU <- mmu_new}
		    };
		    in_mem <- None -- nothing consumed from memory input
		    }
	    case mmu_trans => {
		    o_mmu, o_mem, cons, mmu_new = MMU_sched(mmu_req, mem_req, MMU);
		    out_core <- o_mmu;
		    out_mem <- o_mem;
		    consumed <- cons;
		    MMU <- mmu_new;
	
		    when (MMU.MMU1.ST1 == wait and MMU.MMU2.ST2 == wait) do {
			if out_mem != None then MMU.S <- mmu_reply else MMU.S <- mmu_wait};
		    in_core <- None -- nothing consumed from core input
		}
	    case mmu_reply => when mem_req != None do {
		    out_core <- mem_req;
		    consumed <- true;
		    MMU.S <- mmu_wait
	    }
        }
    };
    
    -- no messages consumed at all?
    when (!consumed) do {in_core <- None; in_mem <- None};
 
    return (in_core, in_mem, out_core, out_mem, MMU)
}
