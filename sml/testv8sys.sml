use "/home/christoph/HOL/examples/l3-machine-code/lib/Ptree.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Ptree.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/MutableMap.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/MutableMap.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/IntExtra.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/IntExtra.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Nat.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Nat.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Set.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Set.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/L3.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/L3.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Bitstring.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/Bitstring.sml";
use "/home/christoph/HOL/examples/l3-machine-code/lib/BitsN.sig";
use "/home/christoph/HOL/examples/l3-machine-code/lib/BitsN.sml";

use "slarmv8.sig";
use "slarmv8.sml";

open slarmv8;


fun core_step (m2c, mmu2c, c_out) = 
    let val c_or = (fn c_state:(MEM_REQUEST * CORE_STATE) => if m2c <> NONE then m2c else mmu2c)
	val (used_msg, rpl) = core_interface c_or
	val c_out1 = if rpl = NONE then c_out else rpl
	val m2c1 = if used_msg = m2c then NONE else m2c
	val mmu2c1 = if used_msg = mmu2c then NONE else mmu2c
    in
	(m2c1, mmu2c1, c_out1)
    end;


fun mem_step (mmu2m, m2c, m2mmu) = 
    let val m_or = ((fn chan => fn m_state:(MEM_REQUEST * MEM_STATE) => (chan, nofault)) mmu2m)
	val (used_msg, (rpl, rcvr)) = mem_interface m_or
	val m2c1 = if rpl <> NONE andalso rcvr = rcvr_core then rpl else m2c
	val m2mmu1 = if rpl <> NONE andalso rcvr = rcvr_mmu then rpl else m2mmu
	val mmu2m1 = if used_msg <> NONE then NONE else mmu2m
    in
	(mmu2m1, m2c1, m2mmu1)
    end;

fun mmu_step (c2mmu, m2mmu, mmu2c, mmu2m) = 
    let val mmu_or = (fn mmu_conf:(MMU_config)  => (c2mmu, m2mmu))
	val (used_c2mmu, (used_m2mmu, (rpl_c, rpl_m))) = mmu_interface mmu_or
	val mmu2c1 = if rpl_c = NONE then mmu2c else rpl_c
	val c2mmu1 = if used_c2mmu <> NONE then NONE else c2mmu
	val mmu2m1 = if rpl_m = NONE then mmu2m else rpl_m
	val m2mmu1 = if used_m2mmu <> NONE then NONE else m2mmu
    in
	(c2mmu1, m2mmu1, mmu2c1, mmu2m1)
    end;

type STATE = CORE_CONFIG * MMU_config * MEM_CONFIG * MEM_REQUEST option * (MEM_REQUEST * MMU_REGS) option * MEM_REQUEST option * MEM_REQUEST option * MEM_REQUEST option;

fun one_step((c, mmu, m, m2c, c2mmu, m2mmu, mmu2c, mmu2m), id) =
    let val _ = C := c
	val _ = M := m
	val _ = MMU := mmu
        val (m2c1, c2mmu1, m2mmu1, mmu2c1, mmu2m1) = (
	    if id = 1 then 
		let val (m2c2, mmu2c2, c2mmu2) = core_step (m2c, mmu2c, c2mmu) in 
		    (m2c2, c2mmu2, m2mmu, mmu2c2, mmu2m)
		end
	    else if id = 2 then 
		let val (c2mmu2, m2mmu2, mmu2c2, mmu2m2) = mmu_step (c2mmu, m2mmu, mmu2c, mmu2m) in 
		    (m2c, c2mmu2, m2mmu2, mmu2c2, mmu2m2)
		end
	    else 
	    	let val (mmu2m2, m2c2, m2mmu2) = mem_step (mmu2m, m2c, m2mmu) in
	    	    (m2c2, c2mmu, m2mmu2, mmu2c, mmu2m2)
		end
	)
    in
	(!C, !MMU, !M, m2c1, c2mmu1, m2mmu1, mmu2c1, mmu2m1)
    end;

(* execute memory steps in one atomic step *)
fun mem_step_atomic c = one_step(one_step(c,3),3);

(* fast forward MMU until memory access or core reply (fault) *)
fun mmu_step_ff (c : STATE) = 
    let 
    	val c' = ref c 
    in
        while ((#7 (!c')) = NONE andalso (#8 (!c')) = NONE) do c' := one_step(!c',2);
	!c'
    end;


(* fast forward MMU and memory steps until next ST1 memory access or core reply (fault) *)
fun mmu_step_ff_ST2_atomic c : STATE = 
    let 
    	val c' = ref c 
	fun stop_loop_fault(s : STATE) = (#7 s) <> NONE
	fun ST2_lookup(s : STATE) = (#8 s) <> NONE andalso #secondstage(#fault(#desc(valOf(#8 s))))
	fun stop_loop_mem(s : STATE) = (#8 s) <> NONE andalso not (ST2_lookup(s))
    in
        while not (stop_loop_fault(!c') orelse stop_loop_mem(!c')) do (
	      c' := mmu_step_ff (!c');
	      if ST2_lookup(!c') then c' := mem_step_atomic (!c') else ()
	);
	!c'
    end;

(* complete atomic MMU and depending memory steps *) 
fun mmu_step_atomic c : STATE = 
    let 
    	val c' = ref c 
	fun stop_loop_fault(s : STATE) = (#7 s) <> NONE
	fun table_lookup(s : STATE) = (#8 s) <> NONE andalso #acctype(valOf(#8 s)) = AccType_PTW
	fun stop_loop_mem(s : STATE) = (#8 s) <> NONE andalso not (table_lookup(s))
    in
        while not (stop_loop_fault(!c') orelse stop_loop_mem(!c')) do (
	      c' := mmu_step_ff (!c');
	      if table_lookup(!c') then c' := mem_step_atomic (!c') else ()
	);
	!c'
    end;

(* fast forward core to next MMU request, for now halt at faults *)
fun core_step_ff (c : STATE) = 
    let 
    	val c' = ref c 
	fun stop_loop(s : STATE) = (#5 s) <> NONE orelse (#S(#1 s) = core_issue andalso #typ(#CURR_FAULT(#1 s)) <> Fault_None)
    in
        while not(stop_loop (!c')) do c' := one_step(!c',1);
	!c'
    end;

(* atomic combination of core, mmu, and memory cycle *)
fun cmm_cycle (c : STATE) = 
    let 
    	val c' = core_step_ff c
	val c'' = if (#5 (c')) <> NONE then mmu_step_atomic c' else c'
    in
	if (#8 (c'')) <> NONE then mem_step_atomic c'' else c''
    end;

(* sequential execution of whole program until PC reaches certain address *)
fun execute_until_pc (c : STATE, pc : string) = 
    let 
    	val c' = ref c 
	val x = valOf(BitsN.fromHexString(pc,64))
	fun stop_loop(s : STATE) = #PC(#1 (!c')) = x orelse (#S(#1 s) = core_issue andalso #typ(#CURR_FAULT(#1 s)) <> Fault_None)
    in
	while not(stop_loop(!c')) do c' := cmm_cycle (!c');
	!c'
    end;

fun set_state (c : STATE) = 
    let
        val _ = C := (#1 c)
        val _ = MMU := (#2 c)
	val _ = M := (#3 c)
    in
	c
    end;

(* I/O *)

fun byteToHex b = 
    let 
    	val x = Word8.toString b
    in
	if (String.size x) = 1 then "0" ^ x else x
    end;

fun fileToHex f = 
    let 
    	val strm = TextIO.openIn f
	val s = Byte.stringToBytes(valOf(TextIO.inputLine strm))
	val i = ref 0
	val x = ref ""	    	
    in
	TextIO.closeIn strm;
	while (!i < (Word8Vector.length s - 1)) do (
	      x := (!x) ^ byteToHex(Word8Vector.sub(s,!i));
	      i := (!i) + 1
	);
	!x
    end;	      

fun b2s b = if b then "1" else "0";
val bl2s = foldl (fn (b, s) => s ^ b2s(b)) "";
fun b2B b l = valOf(BitsN.fromBinString(b,l));

fun bs2bs64 s = if size(s) < 64 then B2bs64 ("0" ^ s) else s;

fun X2X48 s = if size(s) < 12 then X2X48 ("0" ^ s) else s;

 
fun show_cstate ( c : STATE ) = 
    case #S(#1 c) of 
    	   core_issue => "core_issue"
	 | core_fetch => "core_fetch"
	 | core_dec => "core_dec"
	 | core_mem => "core_mem";

fun show_req (r : MEM_REQUEST) = 
    let
	val _ = print("va:0x" ^ (BitsN.toHexString(#va r)) ^ " pa:0x" ^ (BitsN.toHexString(#paddress(#desc r))) ^ " size:" ^ Int.toString(#bytesize r))
	val d = #data r
	val _ = if d = [] then print(" typ:") else print(" write_data:" ^ (bl2s d) ^ " typ:")
	val _ = PolyML.print (#acctype r)
    in
	()
    end;

fun show_nonempty_gpr (c : STATE, i) = 
    let 
    	val gpr = BitsN.toBinString(Map.lookup(#REG(#1 c),i))
    in
	if gpr <> "0" then (
	   print ("X" ^ Int.toString(i));
	   if i > 10 then () else print " ";
	   print (" = " ^ bs2bs64(gpr) ^ "\n")
	)
	else
	   ()
    end;	

fun show_gpr (c : STATE ) = 
    let 
    	val gpr = #REG(#1 c)
	val i = ref 0
    in 
	while (!i < 32) do (
	      show_nonempty_gpr(c,!i);
	      i := (!i) + 1
	)
    end;

fun descu ((a1,a2), (b1,b2)) = BitsN.<+(a1,b1);

fun insert f a [] = [a]
 |  insert f a (b::l) = if f(a,b) then b::(insert f a l) else a::(b::l);

fun ins_sort f [] = []
 |  ins_sort f (a::l) = insert f a (ins_sort f l);

fun descusort l = ins_sort descu l;



fun show_mem (c : STATE) = 
    let
	val w = descusort (write_list (#3 c))
	fun print_entry (a,b) = print("0x" ^ X2X48(BitsN.toHexString a) ^ ": " ^ bs2bs64(BitsN.toBinString b) ^ "\n")
	fun print_list [] = ()
	 |  print_list (a::l) = (print_entry a; print_list l)
    in
	print_list w
    end;

fun show (c : STATE) = 
    let
	val _ = print ("CORE_PC    = 0x" ^ (BitsN.toHexString(#PC(#1 c))) ^ "\n")
    	val _ = print ("CORE_STATE = ")
	val _ = PolyML.print (#S(#1 c))
	val _ = print ("CORE_INSTR = ")
	val _ = PolyML.print(Decode (#CURR_INSTR (#1 c)))
	val _ = print ("CORE_FAULT = ")
	val _ = PolyML.print(#typ(#CURR_FAULT (#1 c)),BitsN.toHexString(#ipaddress(#CURR_FAULT(#1 c))))
	val _ = print ("CHAN_C2MMU = ")
	val _ = PolyML.print((#5 c))
	val _ = print ("CHAN_MMU2C = ")
	val _ = PolyML.print((#7 c))
	val _ = print ("MMU_STATE  = ")
	val _ = PolyML.print(#S (#2 c))
	val _ = print ("MMU_REQ    = ")
	val _ = show_req(#CURR_REQ (#2 c))
	val _ = print ("MMU_ST1    = ")
	val _ = PolyML.print(#ST1 (#2 c))
	val _ = print ("MMU_FAULT1 = ")
	val _ = PolyML.print(#typ(#F1 (#2 c)),BitsN.toHexString(#ipaddress(#F1(#2 c))))
	val _ = print ("MMU_ST2    = ")
	val _ = PolyML.print(#ST2 (#2 c))
	val _ = print ("MMU_FAULT2 = ")
	val _ = PolyML.print(#typ(#F2 (#2 c)),BitsN.toHexString(#ipaddress(#F2(#2 c))))
	val _ = print ("CHAN_MMU2M = ")
	val _ = PolyML.print((#6 c))
	val _ = print ("CHAN_M2MMU = ")
	val _ = PolyML.print((#8 c))
	val _ = print ("MEM_STATE  = ")
	val _ = PolyML.print(#S (#3 c))
	val _ = print ("MEM_REQ    = ")
	val _ = show_req(#curr (#3 c))
	val _ = print ("CHAN_M2C   = ")
	val _ = PolyML.print((#4 c))
	val _ = show_gpr c
	val _ = show_mem c
    in
	()
    end;

(* initialization *)
   
(* ST1: 2MB blocks, startl: 1, lastl: 2
   ST2: 32MB blocks, startl: 2, lastl: 2 *)

(* test both stages 
val _ = mmu_setup_std_both(); 
val code = "E1031EAAE2000058420400912000A0D2028C02F8FE0301AAC0035FD61F2003D541524D7638737973";
val _ = code_setup(code,"100010000");
val _ = set_pc("00010000");
*)

(* test ST2 only
val _ = mmu_setup_std_ST2_only();
val code = "E1031EAAE2000058420400912000A0D2028C02F8FE0301AAC0035FD61F2003D541524D7638737973";
val _ = code_setup(code,"100010000");
val _ = set_pc("100010000");
*)

(* test ST1 only 
val _ = mmu_setup_std_ST1_only();
val code = "E1031EAAE2000058420400912000A0D2028C02F8FE0301AAC0035FD61F2003D541524D7638737973";
val _ = code_setup(code,"100010000");
val _ = set_pc("00010000");
*)

(* test flat mapping 
val _ = mmu_setup_std_flat();
val code = "E1031EAAE2000058420400912000A0D2028C02F8FE0301AAC0035FD61F2003D541524D7638737973";
val _ = code_setup(code,"100010000");
val _ = set_pc("100010000");
*)

val _ = mmu_setup_std_flat();
val _ = install_elf(fileToHex("asm/a.out"), "100010000");

val _ = init_automaton();

(* set link register to "10001029A", misaligned, out of range *)
val _ = set_reg(30,"10001029A");

val zero_32 = BitsN.fromInt(0,32);

val m2c = NONE:MEM_REQUEST option;
val c2mmu = NONE:(MEM_REQUEST * MMU_REGS) option;
val m2mmu = NONE:MEM_REQUEST option;
val mmu2c = NONE:MEM_REQUEST option;
val mmu2m = NONE:MEM_REQUEST option;

val c_init = (!C, !MMU, !M, m2c, c2mmu, m2mmu, mmu2c, mmu2m);

fun GPR r = (X(64)) (BitsN.fromNat(r,5));
fun decode_fetch (c:STATE) = Decode(valOf(BitsN.fromBinString(bl2s(#data(valOf(#4 c))),32)));

(* execute whole program in one step *)

val c' =  execute_until_pc(c_init,"10001029A");
val _ = show c';



(*
val c_1 = cmm_cycle c_init;		(* fetch "mov	X1, X30" *)
val c_2 = cmm_cycle c_1;		(* execute X1 := LR, fetch "ldr     X2, message" *)
*)

(* atomic transitions

val c_1 = core_step_ff c_init;		(* Core transitions until fetch request to MMU *)
val c_2 = mmu_step_ff c_1;		(* MMU transitions unitl first memory request (ST2) *)
val c_3 = mem_step_atomic c_2;		(* atomic memory transition *)
val c_4 = mmu_step_ff c_3;		(* MMU transitions until next memory request (ST1) *)
val c_5 = mem_step_atomic c_4;		(* atomic memory transition *)
val c_6 = mmu_step_ff_ST2_atomic c_5;	(* MMU and memory transitions until the next ST1 memory request *)
val c_7 = mem_step_atomic c_6;		(* atomic memory transition *)
val c_8 = mmu_step_ff_ST2_atomic c_7;	(* MMU and memory transitions until the final memory request for fetch *)
val c_9 = mem_step_atomic c_8;		(* atomic memory transition *)
val c_10 = core_step_ff c_9;		(* Core, finish instruction execution, start next one *)
val c_11 = mmu_step_atomic c_10;	(* MMU and memory transitions for complete translation of fetch address *)
val c_12 = mem_step_atomic c_11;	(* atomic memory transition *)
val c_13 = core_step_ff c_12;		(* Core, decode, start memory operation *)
val c_14 = mmu_step_atomic c_13;	(* MMU and memory transitions for complete translation of load address *)
val c_15 = mem_step_atomic c_14;	(* atomic memory transition *)
val c_16 = cmm_cycle c_15; 		(* atomic core, mmu, memory cycle, finish fetch "add	X2, X2, #1" *)
val c_17 = cmm_cycle c_16; 		(* atomic core, mmu, memory cycle, execute X2 := X2 + 1, next, finish fetch "mov  X0, #65536" *)
val c_18 = cmm_cycle c_17; 		(* atomic core, mmu, memory cycle, execute X0 := 0, next, finish fetch "str	X2, [X0, #40]!" *)
val c_19 = cmm_cycle c_18; 		(* atomic core, mmu, memory cycle, decode, request execute store operation *)
val c_20 = cmm_cycle c_19; 		(* atomic core, mmu, memory cycle, write back address to X0, fetch "mov     X30, X1" *)
val c_21 = cmm_cycle c_20; 		(* atomic core, mmu, memory cycle, execute LR := X1, next, finish fetch "ret" *)
val c_22 = cmm_cycle c_21; 		(* atomic core, mmu, memory cycle, return to "10001029A", alignment fault on fetch *)
val c_23 = cmm_cycle c_22;		(* atomic core, mmu, memory cycle, loop *)

*)

(* step by step simulation 

val c_1 = one_step(c_init, 1); 	 (* Core init, req fetch translation *) 
val c_2 = one_step(c_1, 2);	 (* MMU ST1 init *)
val c_3 = one_step(c_2, 2);	 (* MMU ST1, prepare first lookup, ST2 init *)
val c_4 = one_step(c_3, 2);	 (* MMU ST2, prepare first lookup *)
val c_5 = one_step(c_4, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_6 = one_step(c_5, 3);	 (* Memory init, receive request *)
val c_7 = one_step(c_6, 3);	 (* Memory serve, reply to MMU *)
val c_8 = one_step(c_7, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_9 = one_step(c_8, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_10 = one_step(c_9, 3);	 (* Memory init, receive request *)
val c_11 = one_step(c_10, 3);	 (* Memory serve, reply to MMU *)
val c_12 = one_step(c_11, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare next lookup, ST2 init *)
val c_13 = one_step(c_12, 2);	 (* MMU ST2, prepare lookup *)
val c_14 = one_step(c_13, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_15 = one_step(c_14, 3);	 (* Memory init, receive request *)
val c_16 = one_step(c_15, 3);	 (* Memory serve, reply to MMU *)
val c_17 = one_step(c_16, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_18 = one_step(c_17, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_19 = one_step(c_18, 3);	 (* Memory init, receive request *)
val c_20 = one_step(c_19, 3);	 (* Memory serve, reply to MMU *)
val c_21 = one_step(c_20, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare final memory access, ST2 init *)
val c_22 = one_step(c_21, 2);	 (* MMU ST2, prepare lookup *)
val c_23 = one_step(c_22, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_24 = one_step(c_23, 3);	 (* Memory init, receive request *)
val c_25 = one_step(c_24, 3);	 (* Memory serve, reply to MMU *)
val c_26 = one_step(c_25, 2);	 (* MMU ST2, finish walk, combine descriptors *)
val c_27 = one_step(c_26, 2);	 (* MMU ST1 finish, send fetch request to mem*)
val c_28 = one_step(c_27, 3);	 (* Memory init, receive request *)
val c_29 = one_step(c_28, 3);	 (* Memory serve, reply to core *)
val c_30 = one_step(c_29, 1);	 (* Core receive fetch result, go to decode phase *)
val c_31 = one_step(c_30, 1);	 (* Core decode and execute, MOV instruction, X1 := LR *)

val c_32 = one_step(c_31, 1);	 (* Core init, req fetch translation *) 
val c_33 = one_step(c_32, 2);	 (* MMU ST1 init *)
val c_34 = one_step(c_33, 2);	 (* MMU ST1, prepare first lookup, ST2 init *)
val c_35 = one_step(c_34, 2);	 (* MMU ST2, prepare first lookup *)
val c_36 = one_step(c_35, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_37 = one_step(c_36, 3);	 (* Memory init, receive request *)
val c_38 = one_step(c_37, 3);	 (* Memory serve, reply to MMU *)
val c_39 = one_step(c_38, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_40 = one_step(c_39, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_41 = one_step(c_40, 3);	 (* Memory init, receive request *)
val c_42 = one_step(c_41, 3);	 (* Memory serve, reply to MMU *)
val c_43 = one_step(c_42, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare next lookup, ST2 init *)
val c_44 = one_step(c_43, 2);	 (* MMU ST2, prepare lookup *)
val c_45 = one_step(c_44, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_46 = one_step(c_45, 3);	 (* Memory init, receive request *)
val c_47 = one_step(c_46, 3);	 (* Memory serve, reply to MMU *)
val c_48 = one_step(c_47, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_49 = one_step(c_48, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_50 = one_step(c_49, 3);	 (* Memory init, receive request *)
val c_51 = one_step(c_50, 3);	 (* Memory serve, reply to MMU *)
val c_52 = one_step(c_51, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare final memory access, ST2 init *)
val c_53 = one_step(c_52, 2);	 (* MMU ST2, prepare lookup *)
val c_54 = one_step(c_53, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_55 = one_step(c_54, 3);	 (* Memory init, receive request *)
val c_56 = one_step(c_55, 3);	 (* Memory serve, reply to MMU *)
val c_57 = one_step(c_56, 2);	 (* MMU ST2, finish walk, combine descriptors *)
val c_58 = one_step(c_57, 2);	 (* MMU ST1 finish, send fetch request to mem*)
val c_59 = one_step(c_58, 3);	 (* Memory init, receive request *)
val c_60 = one_step(c_59, 3);	 (* Memory serve, reply to core *)
val c_61 = one_step(c_60, 1);	 (* Core receive fetch result, go to decode phase *)
val c_62 = one_step(c_61, 1);	 (* Core decode memory operation, send load request *)
val c_63 = one_step(c_62, 2);	 (* MMU ST1 init *)
val c_64 = one_step(c_63, 2);	 (* MMU ST1, prepare first lookup, ST2 init *)
val c_65 = one_step(c_64, 2);	 (* MMU ST2, prepare first lookup *)
val c_66 = one_step(c_65, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_67 = one_step(c_66, 3);	 (* Memory init, receive request *)
val c_68 = one_step(c_67, 3);	 (* Memory serve, reply to MMU *)
val c_69 = one_step(c_68, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_70 = one_step(c_69, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_71 = one_step(c_70, 3);	 (* Memory init, receive request *)
val c_72 = one_step(c_71, 3);	 (* Memory serve, reply to MMU *)
val c_73 = one_step(c_72, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare next lookup, ST2 init *)
val c_74 = one_step(c_73, 2);	 (* MMU ST2, prepare lookup *)
val c_75 = one_step(c_74, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_76 = one_step(c_75, 3);	 (* Memory init, receive request *)
val c_77 = one_step(c_76, 3);	 (* Memory serve, reply to MMU *)
val c_78 = one_step(c_77, 2);	 (* MMU ST2, receive descriptor, extend walk, finalize and return to ST1 *)
val c_79 = one_step(c_78, 2);	 (* MMU ST1, request next pte on level 1 *)
val c_80 = one_step(c_79, 3);	 (* Memory init, receive request *)
val c_81 = one_step(c_80, 3);	 (* Memory serve, reply to MMU *)
val c_82 = one_step(c_81, 2);	 (* MMU ST1, receive descriptor, extend walk, prepare final memory access, ST2 init *)
val c_83 = one_step(c_82, 2);	 (* MMU ST2, prepare lookup *)
val c_84 = one_step(c_83, 2);	 (* MMU ST2 request 32 MB block descriptor, level 2 *)
val c_85 = one_step(c_84, 3);	 (* Memory init, receive request *)
val c_86 = one_step(c_85, 3);	 (* Memory serve, reply to MMU *)
val c_87 = one_step(c_86, 2);	 (* MMU ST2, finish walk, combine descriptors *)
val c_88 = one_step(c_87, 2);	 (* MMU ST1 finish, send load request to mem*)
val c_89 = one_step(c_88, 3);	 (* Memory init, receive request *)
val c_90 = one_step(c_89, 3);	 (* Memory serve, reply to core *)
val c_91 = one_step(c_90, 1);	 (* Core receive load data, write back, X2 := m_8("100010020") (="sys8vMRA") *)

*)