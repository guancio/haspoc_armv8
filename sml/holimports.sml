val holpathChr = "/home/christoph/HOL";
val holpathOpt = "/opt/hol_11";
val holpath = holpathChr;


use (holpath ^ "/examples/l3-machine-code/lib/Ptree.sig");
use (holpath ^ "/examples/l3-machine-code/lib/Ptree.sml");
use (holpath ^ "/examples/l3-machine-code/lib/MutableMap.sig");
use (holpath ^ "/examples/l3-machine-code/lib/MutableMap.sml");
use (holpath ^ "/examples/l3-machine-code/lib/IntExtra.sig");
use (holpath ^ "/examples/l3-machine-code/lib/IntExtra.sml");
use (holpath ^ "/examples/l3-machine-code/lib/Nat.sig");
use (holpath ^ "/examples/l3-machine-code/lib/Nat.sml");
use (holpath ^ "/examples/l3-machine-code/lib/Set.sig");
use (holpath ^ "/examples/l3-machine-code/lib/Set.sml");
use (holpath ^ "/examples/l3-machine-code/lib/L3.sig");
use (holpath ^ "/examples/l3-machine-code/lib/L3.sml");
use (holpath ^ "/examples/l3-machine-code/lib/Bitstring.sig");
use (holpath ^ "/examples/l3-machine-code/lib/Bitstring.sml");
use (holpath ^ "/examples/l3-machine-code/lib/BitsN.sig");
use (holpath ^ "/examples/l3-machine-code/lib/BitsN.sml");
use (holpath ^ "/examples/l3-machine-code/lib/PureMap.sig");
use (holpath ^ "/examples/l3-machine-code/lib/PureMap.sml");
