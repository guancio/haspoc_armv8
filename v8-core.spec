--------------------------------------------------------------------
-- This a high-level definition of the ARMv8 instruction core     --
-- Author: Christoph Baumann, KTH CSC Stockholm		          --
--------------------------------------------------------------------


{-

val () = Runtime.LoadF "v8-base_types.spec, v8-mem_types.spec, v8-base.spec, v8-mem.spec, v8-core.spec";

-}

exception MEM_INPUT_MISMATCH


-------------------------------
-- helper functions
-------------------------------

MMU_REGS mmu_regs (va :: VA, tr :: nat) = 
{
  var R;
  R.sctlr <- SCTLRType(&SCTLR(tr));
  R.ttbr <- if (va<63> and tr < 2) then TTBRType(C.&TTBR1_EL1)
                                   else TTBRType(&TTBR0(tr));
  R.tcr1 <- TCR_EL1([C.&TCR_EL1]::bits(64));
  R.tcr23 <- match tr {
      case 3 => TCR_EL2_EL3(C.&TCR_EL3)
      case 2 => TCR_EL2_EL3(C.&TCR_EL2)
      case _ => UNKNOWN};
  R.mair <- MAIR(tr);
  R.idmm <- mm_feat(C.&ID_AA64MMFR0_EL1);
  R.hcr <- HCR_EL2(C.&HCR_EL2::bits(64));
  R.vttbr <- TTBRType(C.&VTTBR_EL2);
  R.vtcr <- VTCR_EL2(C.&VTCR_EL2::bits(32));

  return R
}


-- effective address for memory accesses
VA ea (n :: reg, postindex :: bool, offset :: dword, rn_unknown :: bool) = 
{
   address = if n == 31 then
                SP
             else if rn_unknown then
                UNKNOWN
             else
                X(n);
   return (if postindex then address else address + offset)
}

bool list write_data(t :: reg, t2 :: reg, size :: bits(N), pair :: bool, rt_unknown :: bool) with N in 8, 16, 32, 64, 128 = 
{

         tdata`N = match N { --`
	     case 8  => [X(t)`8] -- `
	     case 16 => [X(t)`16] -- `
	     case 32 => [X(t)`32] -- `
	     case _  => [X(t)`64] -- `
	 };

	 pairdata`N = match N { --`
	     case 64  => if BigEndian then
  		 [X(t)`32 : X(t2)`32]
	     else
	         [X(t2)`32 : X(t)`32]
	     case 128 => if BigEndian then
		 [X(t)`64 : X(t2)`64]
	     else
	         [X(t2)`64 : X(t)`64]
	     case _   => UNKNOWN 
		 };
	 
	 data = if rt_unknown then UNKNOWN
                  else if pair then pairdata
		  else tdata;

	 return [data]
}

AccType memop_type (memop :: MemOp) = match memop {
    case MemOp_PREFETCH => AccType_IFETCH
    case _ => AccType_NORMAL
}

(MMU_REQUEST option) issue_curr(l :: nat) = {
	req = Element(l,C.CURR_REQ);
	tr = if req.EL == 0 then 1 else req.EL;
    
	return Some(req, mmu_regs(req.va, tr))	
}

unit run_curr() = {
	C.branch_hint <- None;
	Run (Decode (C.CURR_INSTR));
	when not (IsSome (C.branch_hint)) do C.PC <- C.PC::bits(64) + 4
}



-- decodes va, data to write, acctype, memop, size, excl
VA * (bool list) * AccType * MemOp * nat * bool decode_mem(instr :: word) = match Decode(instr) {
	case LoadStore (LoadStoreImmediate@8 (size, regsize_word, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, unsigned_offset, offset, n, t)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    1,    false
	case LoadStore (LoadStoreImmediate@16 (size, regsize_word, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, unsigned_offset, offset, n, t)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    2,    false
	case LoadStore (LoadStoreImmediate@32 (size, regsize_word, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, unsigned_offset, offset, n, t)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    4,    false
	case LoadStore (LoadStoreImmediate@64 (size, regsize_word, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, unsigned_offset, offset, n, t)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    8,    false

	case LoadStore( LoadStoreRegister@8 (size, regsize_word, memop, signed, m, extend_type, shift, n, t)) =>
	    ea(n, false, ExtendReg (m, extend_type, shift), false),    write_data(t, t, size, false, false),    AccType_NORMAL,    memop,    1,    false
	case LoadStore( LoadStoreRegister@16 (size, regsize_word, memop, signed, m, extend_type, shift, n, t)) =>
	    ea(n, false, ExtendReg (m, extend_type, shift), false),    write_data(t, t, size, false, false),    AccType_NORMAL,    memop,    2,    false
	case LoadStore( LoadStoreRegister@32 (size, regsize_word, memop, signed, m, extend_type, shift, n, t)) =>
	    ea(n, false, ExtendReg (m, extend_type, shift), false),    write_data(t, t, size, false, false),    AccType_NORMAL,    memop,    4,    false
	case LoadStore( LoadStoreRegister@64 (size, regsize_word, memop, signed, m, extend_type, shift, n, t)) =>
	    ea(n, false, ExtendReg (m, extend_type, shift), false),    write_data(t, t, size, false, false),    AccType_NORMAL,    memop,    8,    false

	case LoadStore (LoadLiteral@32 (size, memop, signed, offset, t)) =>
	    C.PC + offset,    UNKNOWN,    memop_type(memop),    memop,    4,    false
	case LoadStore (LoadLiteral@64 (size, memop, signed, offset, t)) =>
	    C.PC + offset,    UNKNOWN,    memop_type(memop),    memop,    8,    false

	-- this just decodes the first member of the pair here, the second is treated separately as it requires a separate memory access
	case LoadStore (LoadStorePair@32 (size, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t, t2)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    4,    false
	case LoadStore (LoadStorePair@64 (size, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t, t2)) =>
	    ea(n, postindex, offset, false),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    8,    false

	case LoadStore (LoadStoreAcquire@8 (size, memop, acctype, excl, rn_unknown, rt_unknown, s, n, t)) => 
	    ea(n, false, 0, rn_unknown),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    1,    excl
	case LoadStore (LoadStoreAcquire@16 (size, memop, acctype, excl, rn_unknown, rt_unknown, s, n, t)) => 
	    ea(n, false, 0, rn_unknown),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    2,    excl
	case LoadStore (LoadStoreAcquire@32 (size, memop, acctype, excl, rn_unknown, rt_unknown, s, n, t)) => 
	    ea(n, false, 0, rn_unknown),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    4,    excl
	case LoadStore (LoadStoreAcquire@64 (size, memop, acctype, excl, rn_unknown, rt_unknown, s, n, t)) => 
	    ea(n, false, 0, rn_unknown),    write_data(t, t, size, false, rt_unknown),    acctype,    memop,    8,    excl

	-- LDAXP/STLXP are atomic for 64 bit access, but loads are split into two for 128
        case LoadStore (LoadStoreAcquirePair@64 (size, memop, acctype, rn_unknown, rt_unknown, s, n, t, t2)) =>
	    ea(n, false, 0, rn_unknown),    write_data(t, t2, size, true, rt_unknown),    acctype,    memop,    8,    true
        case LoadStore (LoadStoreAcquirePair@128 (size, MemOp_LOAD, acctype, rn_unknown, rt_unknown, s, n, t, t2)) =>
	    ea(n, false, 0, rn_unknown),    write_data(t, t, size, false, rt_unknown),    acctype,    MemOp_LOAD,    8,    true
        case LoadStore (LoadStoreAcquirePair@128 (size, MemOp_STORE, acctype, rn_unknown, rt_unknown, s, n, t, t2)) =>
	    ea(n, false, 0, rn_unknown),    write_data(t, t2, size, true, rt_unknown),    acctype,    MemOp_STORE,    16,    true
	case _ => UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN
}


-- decoding second half of pair operations
VA * (bool list) * AccType * MemOp * nat * bool decode_mem2(instr :: word) = match Decode(instr) {
	case LoadStore (LoadStorePair@32 (size, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t, t2)) =>
	    ea(n, postindex, offset, false)+4,    write_data(t2, t2, size, false, rt_unknown),    acctype,    memop,    4,    true
	case LoadStore (LoadStorePair@64 (size, memop, acctype, signed, wb_unknown, rt_unknown, wback, postindex, offset, n, t, t2)) =>
	    ea(n, postindex, offset, false)+8,    write_data(t2, t2, size, false, rt_unknown),    acctype,    memop,    8,    true
        case LoadStore (LoadStoreAcquirePair@128 (size, MemOp_LOAD, acctype, rn_unknown, rt_unknown, s, n, t, t2)) =>
	    ea(n, false, 0, rn_unknown),    write_data(t2, t2, size, false, rt_unknown),    acctype,    MemOp_LOAD,     8,    true
	case _ => UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN, false
}


-- predicate for checking if memory access is necessary / possible
-- updates also the exclusive access bit
bool access_memory() = {
    I = Decode(C.CURR_INSTR);
    match I {
	case MemoryBarrier (_) or System (SystemInstruction (_)) => true
	case LoadStore (_) => {
		address, data, acctype, memop, bytesize, excl = decode_mem(C.CURR_INSTR);
		!(excl and memop == MemOp_STORE) or
		   ExclusiveMonitorPass(address,bytesize)}
        case _ => false
    }		  
}


-------------------
-- Core transitions
-------------------


(MMU_REQUEST option) CORE_ISSUE () =
{
    drop_result(Mem(C.PC, 4, AccType_IFETCH));
    
    var mmu_req = None :: MMU_REQUEST option;

    when (C.CURR_FAULT.typ == Fault_None) do {
        mmu_req <- issue_curr(0);
	C.S <- core_fetch
    };
    
    return mmu_req
}

-- TODO: for non-atomic requests loop here to receive all parts
(MMU_REQUEST option) CORE_FETCH(reply :: MEM_REQUEST) = 
{
    C.CURR_REPLY <- C.CURR_REPLY : (reply @ Nil);
    C.CURR_FAULT <- reply.desc.fault;
    l = Length(C.CURR_REPLY);

    var mmu_req = None;
    
    if l == Length(C.CURR_REQ) then {
	if match_replies() then {
	    instr = Mem(C.PC, 4, AccType_IFETCH);
	    when (C.CURR_FAULT.typ == Fault_None and C.CURR_REQ == Nil) do {
		C.CURR_INSTR <- [instr];
	        C.S <- core_dec}
	    }
        else #MEM_INPUT_MISMATCH
	}
    else mmu_req <- issue_curr(l);
    return mmu_req
}


unit CORE_DECEXEC() = 
{
    when (C.CURR_FAULT.typ == Fault_None) do run_curr();
    C.S <- core_issue
}


MMU_REQUEST CORE_DECMEM () = 
{
    match Decode(C.CURR_INSTR) {
      case System (SystemInstruction (op1, op2, CRn, CRm, L, Rt)) => handle_sysop(op1, op2, CRn, CRm, L, Rt)
      case MemoryBarrier (opc, domain, types) => MemBarrier(opc, domain, types)
      case _ => {
	    
    address, data, acctype, memop, bytesize, excl  = decode_mem(C.CURR_INSTR);

    address2, data2, acctype2, memop2, bytesize2, pair  = decode_mem2(C.CURR_INSTR);

    if memop == MemOp_STORE 
    then drop_result(handle_mem_access(address, bytesize, acctype, data, excl)) 
    else drop_result(handle_mem_access(address, bytesize, acctype, Nil, excl));

    -- TODO: currently this overwrites the first pair request, extend Mem with queue
    when pair do {
	if memop2 == MemOp_STORE 
	then drop_result(handle_mem_access(address2, bytesize2, acctype2, data2, false)) 
	else drop_result(handle_mem_access(address2, bytesize2, acctype2, Nil, false))
    }

	  }
    };

    req = Head(C.CURR_REQ);
    
    C.S <- core_mem;
    tr = if req.EL == 0 then 1 else req.EL;
    
    return (req, mmu_regs(req.va, tr))
}

-- wribe back from memory stage
(MMU_REQUEST option) CORE_WB (reply :: MEM_REQUEST) = 
{
    C.CURR_REPLY <- C.CURR_REPLY : (reply @ Nil);
    C.CURR_FAULT <- reply.desc.fault;
    l = Length(C.CURR_REPLY);
    
    if l == Length(C.CURR_REQ) then {
	if match_replies() then {
	    when (C.CURR_FAULT.typ == Fault_None) do run_curr();
	    --@LOG_I(printI(Decode(C.CURR_INSTR)));
	    C.S <- core_issue;
	    return None}
	else #MEM_INPUT_MISMATCH
    }
    else return issue_curr(l)
}


(MMU_REQUEST option) * bool core_sched (imem :: MEM_REQUEST option) =
{
    var in_mem = imem;
    var out_mmu = None;
    var consumed = false;

    when !(imem == None and (C.S == core_fetch or C.S == core_mem)) do {
	match C.S {
	    case core_issue => {ommu = CORE_ISSUE();
		                when (ommu != None) do out_mmu <- ommu}
	    case core_fetch => {ommu = CORE_FETCH(ValOf(imem)); consumed <- true;
			        when (ommu != None) do out_mmu <- ommu}
	    case core_dec   => if access_memory() then out_mmu <- Some(CORE_DECMEM())
		               else CORE_DECEXEC()
	    case core_mem   => {ommu = CORE_WB(ValOf(imem)); consumed <- true;
			        when (ommu != None) do out_mmu <- ommu}
	}
    };

    return (out_mmu, consumed)
}


-- core interface uses oracle to determine memory input, returns consumed input and new output
-- this function is called from HOL as an instantiation of a local scheduling function in the generic compositional theory
(MEM_REQUEST option) * (MMU_REQUEST option) core_interface (o :: MEM_REQUEST option) =
{
	var input = o;

	(out, consumed) = core_sched(input);

	when !consumed do input <- None;	

	return (input, out)
}


-- TODO: for non-atomic memory accesses issue several requests in sequential manner, move state transitions into transition functions, only go to next state if all requests sent/answered or fault
