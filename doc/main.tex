\documentclass[11pt]{article}

\usepackage[pdftex]{graphicx,color} 
\usepackage{amssymb}
\usepackage{amscd}
\usepackage{float}
\usepackage{alltt}
\usepackage{amsmath}
\usepackage{url}
\usepackage{amsthm}
\usepackage{paralist}
\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{calc}


\title{The HASPOC ARMv8 Architecture Models}
\author{Christoph Baumann, KTH CSC}

\begin{document}

\maketitle

\section{DHARMA8}

\newcommand{\STx}[1]{
\begin{tikzpicture}[remember picture,>=stealth',auto,node distance=29.5,
  thick,main node/.style={circle,draw,minimum size=0.5}]

  \node[main node] (wait) {};
  \node[main node] (init)  [above right of=wait, yshift=-7.2] {};
  \node[main node] (final) [below right of=wait, yshift=7.2] {};
  \node[main node] (mem) [right of=init] {};
  \node[main node] (walk) [right of=final] {};
  \node (creq) [left of=wait,color=blue,font=\footnotesize] {};
  \node (lreq) [right of=mem,color=red,font=\footnotesize] {};
  \node (lookup) [below of=walk,color=red,font=\footnotesize] {};
  \node (freq) [below of=final,color=red,font=\footnotesize] {};
  
  \path[->,every node/.style={font=\tiny}]
    (wait) edge node {} (init)
    (final) edge node {} (wait)
    (init) edge node {} (mem)
    (init) edge node {} (final)
    (walk) edge node {} (final)
    (walk) edge [bend left=20, distance=2mm] node [left] {} (mem)
    (mem) edge [bend left=20, distance=2mm] node [right] {} (walk)
  ;
  \draw ($(wait.west)+(-0.3,1)$)  rectangle ($(walk.south east)+(0.4,-0.4)$);
  
  \path[->]
    (creq) edge [color=#1] (wait)
    (lookup) edge [color=red] (walk)
  ;
  \path
    (mem) edge [color=red] (lreq)
    (final) edge [color=red] (freq)
  ;

\end{tikzpicture}
}

\newsavebox\STone
\sbox{\STone}{\STx{blue}}

\newsavebox\STtwo
\sbox{\STtwo}{\STx{red}}


\begin{figure} 
  \center
\begin{tikzpicture}[>=stealth',auto,node distance=4cm,
  thick,main node/.style={circle,draw,minimum size=13mm}]

  \node[main node] (wait) {$\mathit{wait}$};
  \node[main node] (init)  [right of=wait,xshift=-2cm,yshift=1.5cm] {$\mathit{init}$};
  \node[main node] (final) [right of=wait,xshift=-2cm,yshift=-1.5cm] {$\mathit{final}$};
  \node[main node] (mem) [right of=init] {$\mathit{mem}$};
  \node[main node] (walk) [right of=final] {$\mathit{walk}$};
  \node (creq) [left of=wait,color=blue,font=\footnotesize,xshift=2.5cm] {$\mathit{req}$};
  \node (lreq) [right of=mem,color=red,font=\footnotesize,xshift=-2.5cm] {$\mathit{req}$};
  \node (lookup) [below of=walk,color=red,font=\footnotesize,yshift=2.5cm] {$\mathit{lookup}$};
  \node (freq) [below of=final,color=red,font=\footnotesize,yshift=2.5cm] {$\mathit{req}$};
  
  \path[->,every node/.style={font=\tiny}]
    (wait) edge node {Init} (init)
    (final) edge node {Complete} (wait)
    (init) edge node {StartWalk, $\bar f$} (mem)
    (init) edge node {\parbox{2cm}{FlatMap / \\StartWalk, $f$}} (final)
    (walk) edge node {ExtendWalk, $f \lor c$} (final)
    (walk) edge [bend left=20, distance=1cm] node [left] {\parbox{1.4cm}{ExtendWalk,\\$\bar f\land \bar c$}} (mem)
    (mem) edge [bend left=20, distance=1cm] node [right] {FetchDescriptor} (walk)
  ;

  \path[->]
    (creq) edge [color=blue] (wait)
    (mem) edge [color=red] (lreq)
    (lookup) edge [color=red] (walk)
    (final) edge [color=red] (freq)
  ;
  
\end{tikzpicture}
 \label{STx}\caption{Automaton for one MMU stage, connected to {\color{blue} core} and {\color{red} memory}. Here $f$ stands for ``fault'' and $c$ for ``complete''.}
\end{figure}


\begin{figure}
  \center
    \begin{tikzpicture}[remember picture,node distance=3cm,thick,>=stealth']
    \node (st1) {\usebox{\STone}};
    \node (st2) [below of=st1] {\usebox{\STtwo}};
      \coordinate (st1inc) at ($(st1.west)+(5.5mm,3.15mm)$);
      \coordinate (st1ow) at ($(st1.east)-(5.5mm,-8.1mm)$);
      \coordinate (st1of) at ($(st1.south)-(1.53mm,-4.2mm)$);
      \coordinate (st1inl) at ($(st1.south)+(8.8mm,4.2mm)$);
      \coordinate (st2in) at ($(st2.west)+(5.5mm,3mm)$);
      \coordinate (st2ow) at ($(st2.east)-(5.5mm,-8.1mm)$);
      \coordinate (st2of) at ($(st2.south)-(1.53mm,-4.2mm)$);
      \coordinate (st2inl) at ($(st2.south)+(8.8mm,4.2mm)$);

    \draw[color=red] (st1of) -- ++(0,-3.5mm) coordinate (conn);
    \draw[color=red] (st1ow) |- (conn);
    \draw[color=red] (conn) -| (st2in);
    \draw[color=red] (st2ow) -- ++(0,-22mm) coordinate (memout);
    \draw[color=red] (st2of) |- (memout);
    \draw[color=red,->] (memout) -- ++(1,0) coordinate;
    \node[color=red] at ($(memout)+(1.3,-0.4mm)$) {$\mathit{req}$};
    \draw[color=red] (st2inl) -- ++(12mm,0) coordinate (memin);
    \draw[color=red] (st1inl) -| (memin);
    \draw[color=red] (memin) -- ++(0.67,0) coordinate;
    \node[color=red] at ($(memin)+(1.26,-0.1mm)$) {$\mathit{lookup}$};
    \draw[color=blue] (st1inc) -- ++(-7mm,0) coordinate;
    \node[color=blue] at ($(st1inc)-(1.05,0.4mm)$) {$\mathit{req}$};

    \draw ($(st1inc)+(-0.3,1.7)$)  rectangle ($(memout)+(0.6,-0.3)$);

    \node at (st1.north west) [xshift=1cm,yshift=1mm] {ST1};
    \node at (st2.south west) [xshift=1cm,yshift=4mm] {ST2};
    \node at (st1.north) [xshift=1.5mm,yshift=12mm,font=\huge] {MMU};

    
    \end{tikzpicture}
    \label{MMU}\caption{MMU combined of two stages, connected to {\color{blue} core} and {\color{red} memory}.}
\end{figure}


\section{SHARMA8}

\end{document}
