-----------------------------------------------------------------------
-- This is an extension of:					     --	
-- 								     --
-- Formal Specification of the ARMv8-A instruction set architecture  --
-- (c) Anthony Fox, University of Cambridge                          --
-- 								     --
-- for introducing system level functionality 			     --
-- and a memory system interface				     --
-- Author: Christoph Baumann, KTH CSC Stockholm			     --
-----------------------------------------------------------------------


-- Notes:
-- 2014-11-05	moved to separate file
-- 2014-12-11   new memory interface functions

{-

val () = Runtime.LoadF "v8-base.spec, v8-id.spec, v8-mem_types.spec, v8-mem.spec";

-}


--------------------------------------------------------------
-- Core -> Memory interface for reads, writes, and address translation
--------------------------------------------------------------


-- assumed to be aligned
unit MemSingle (address::dword, size::nat, acctype::AccType, data :: bool list, excl :: bool) = 
{
      -- when address != Align(address, size) do #ALIGNMENT_FAULT
      -- TODO: ClearExclusive, p. 5145

      write = data != Nil;

      var req;
      req.core_id <- C.CORE_ID;
      req.id <- C.REQ_CNT;
      req.va <- address;
      req.write <- write;
      req.acctype <- acctype;
      req.EL <- [C.PSTATE.EL];
      req.bytesize <- size;
      when write do req.data <- BER(data);
      req.ns <- !(req.EL == 3 or req.EL == 1 and !C.SCR_EL3.NS);
      req.sopar <- if excl then Exclusive else SysOpNone;
      req.desc.fault.acctype <- acctype;
      req.desc.fault.typ <- Fault_None;
      
      C.REQ_CNT <- C.REQ_CNT+1;
      C.CURR_REQ <- C.CURR_REQ : (req @ Nil)
}

-- match memory replies against current requests
bool match_replies() = 
{
    lrpl = Length(C.CURR_REPLY);
    lreq = Length(C.CURR_REQ);
    var eq = true;
    if lrpl == lreq and lrpl > 0 then {
	    for i in 0 .. lrpl-1 do
		eq <- eq and (Element(0,C.CURR_REPLY).id == Element(0,C.CURR_REQ).id);
	    return eq}
    else return false
}

-- This is the heart of the Mem function that allows sequential
-- programming of the instruction set. It can be used to issue
-- requests in the ISSUE and MEM transitions or return the received
-- values in FETCHDEC and WB. It also clears the local exclusive
-- monitor when needed and sets the return value for exclusive stores.
(bool list) handle_mem_access (address :: VA, size :: nat, acctype :: AccType, data :: bool list, excl :: bool) = 
{
    write = data != Nil;
    C.CURR_FAULT <- NoFault;
    aligned = CheckAlignment (address, size, acctype, write);
    atomic = (aligned and !(acctype in set{AccType_VEC, AccType_VECSTREAM})) or size == 1;    

    var value :: bool list = Nil;
    
    when C.CURR_FAULT.typ == Fault_None do {
	    -- if reply for earlier request was received then use this data
	    if match_replies() then {
		var succ = true;
		var clrex = false;
		if atomic then {
		    r = Head(C.CURR_REPLY);
		    value <- BER(r.data);
		    when clrx(r) do {
			succ <- r.sopar == Exclusive and !Head(r.data);
			clrex <- true};
		    C.CURR_REPLY <- Tail (C.CURR_REPLY);
		    C.CURR_REQ <- Tail (C.CURR_REQ)}  
	        else {
	       	    for i in 0 .. size-1 do {
			r = Head(C.CURR_REPLY);
			value <- r.data : value;
			when clrx(r) do {
			    succ <- succ and r.sopar == Exclusive and !Head(r.data);
			    clrex <- true};
			C.CURR_REPLY <- Tail (C.CURR_REPLY);
			C.CURR_REQ <- Tail (C.CURR_REQ)
		    };
		    value <- BER(value)
		};
		when clrex do {
		    C.LEXMON.active <- false;
		    C.XSUCCESS <- succ}
	    }
	    -- otherwise issue request(s) 
	    else {
		if atomic then MemSingle(address, size, acctype, data, excl)
		else for i in 0 .. size - 1 do MemSingle (address + [i], 1, acctype, get_byte(i,data), excl)
	    }		    
    };

    return value
-- 
--   
}

unit drop_result(data :: bool list) = nothing

component Mem (address::dword, size::nat, acctype::AccType) :: bool list  
{
    value = handle_mem_access(address, size, acctype, Nil, false)
    assign value = drop_result(handle_mem_access(address, size, acctype, value, false))
}
	

bool match_reply_sys(req :: MEM_REQUEST) = 
{
    if C.CURR_REPLY != Nil then {
	    rpl = Head(C.CURR_REPLY);
	    return (rpl.id == req.id)
	    }
    else return false
}

-- call this function for issuing and receiving AT requests, answers are decoded and PAR is updated
unit handle_at(req :: MEM_REQUEST) =
{
    when req.acctype != AccType_AT do #SYSOP_SCHEDULE_FAULT;
    C.CURR_FAULT <- NoFault;
    
    if match_reply_sys(req) then {
	    rpl = Head(C.CURR_REPLY);
	    set_PAR(rpl.desc, rpl.ns);
	    C.CURR_REPLY <- Tail(C.CURR_REPLY);
	    C.CURR_REQ <- Tail(C.CURR_REQ)}
    -- otherwise issue request 
    else C.CURR_REQ <- C.CURR_REQ : (req @ Nil)
}

-- call this function for issuing and receiving DC, IC and TLBI requests, returned faults are recorded
unit handle_cachetlb(req :: MEM_REQUEST) =
{
    when req.acctype notin set { AccType_DC, AccType_IC, AccType_TLB } do #SYSOP_SCHEDULE_FAULT;
    C.CURR_FAULT <- NoFault;
    
    if match_reply_sys(req) then {
	    C.CURR_FAULT <- Head(C.CURR_REPLY).desc.fault;
	    C.CURR_REPLY <- Tail(C.CURR_REPLY);
	    C.CURR_REQ <- Tail(C.CURR_REQ)}
    -- otherwise issue request 
    else C.CURR_REQ <- C.CURR_REQ : (req @ Nil)
}

unit MemBarrier (op :: MemBarrierOp, domain :: MBReqDomain, t :: MBReqTypes) =
{
    C.CURR_FAULT <- NoFault;
    
    var req;

    if C.S == core_mem then req <- Head (C.CURR_REQ)
    else {
	req.core_id <- C.CORE_ID;
	req.id <- C.REQ_CNT;
	req.write <- false;
	req.acctype <- AccType_MB;
	req.EL <- [C.PSTATE.EL];
	req.bytesize <- 1;
	req.data <- Nil;
	req.ns <- !(req.EL == 3 or req.EL == 1 and !C.SCR_EL3.NS);
	req.sopar <- MemBar(op,domain,t);
	req.desc.fault.acctype <- AccType_MB;
	req.desc.fault.typ <- Fault_None;
	C.REQ_CNT <- C.REQ_CNT+1
    };

    when req.acctype != AccType_MB do #SYSOP_SCHEDULE_FAULT;
	 
    if match_reply_sys(req) then {
	    C.CURR_REPLY <- Tail(C.CURR_REPLY);
	    C.CURR_REQ <- Tail(C.CURR_REQ)}
    -- otherwise issue request 
    else C.CURR_REQ <- C.CURR_REQ : (req @ Nil)

}

unit handle_sysop(sys_op1::bits(3), sys_op2::bits(3), sys_crn::bits(4), sys_crm::bits(4), has_result::bool, t::reg) =
{
    var req;
    if C.S == core_mem then req <- Head (C.CURR_REQ)
    else {
	req <- decode_sysop(sys_op1,sys_op2,sys_crn,sys_crm,has_result,t);
	trp = trap_sysop(req.sopar);
	when trp > 0 do {if trp == 4 then #TRAP_TO_EL2 else #UNDEFINED_FAULT ("InsufficientPrivileges")}
    };
    
    match req.acctype {
	case AccType_DC or AccType_IC or AccType_TLB => handle_cachetlb(req)
	case AccType_AT => handle_at(req)
	case _ => #SYSOP_SCHEDULE_FAULT
    }	  
}	 



-- TODO: PC wrap around issue 
-- TODO: semantics for DC, TLBI, barriers, same signatures as sequential models

