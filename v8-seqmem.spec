-----------------------------------------------------------
-- ARMv8 minimal sequential memory model                 --
-----------------------------------------------------------

-- Runtime.LoadF "v8-base_types.spec, v8-mem_types.spec, v8-seqmem.spec";

construct MEM_STATE { mem_wait mem_serve }
construct MEM_RCVR {rcvr_mmu, rcvr_core}

record MEM_CONFIG {
       MISW :: MEM_IMPL_SWITCHES
       curr :: MEM_REQUEST
       rcvr :: MEM_RCVR
       S :: MEM_STATE
       W :: bits(45) list
       PLEXMON :: ExclusiveMonitor list
       GEXMON :: ExclusiveMonitor list
       ERG :: nat
}

type MEM_ABS = ADR -> bits(8)
type MEM_TZC = bits(36) -> TrustZonePermission

construct MEM_FAULT {sync_parity, sync_external, async_parity, async_external, nofault}


------------------------
-- Exclusive Monitors
------------------------


-- initialize exclusive monitors for number of cores
MEM_CONFIG init_exmons(nc :: nat, mc :: MEM_CONFIG) = {
    var exmon :: ExclusiveMonitor;
    exmon.active <- false;
    var MC = mc;
    MC.PLEXMON <- Nil;
    MC.GEXMON <- Nil;

    for i in 1 .. nc do {
        MC.PLEXMON <- exmon @ MC.PLEXMON;
        MC.GEXMON <- exmon @ MC.GEXMON
    };
    return MC
}		  

ExclusiveMonitor get_exmon(id :: nat, global :: bool, MC :: MEM_CONFIG) = {
    when id >= Length(MC.PLEXMON) or id >= Length(MC.GEXMON) do
        #ASSERT("core id out of range");
    match global {
	case true => Element(id,MC.GEXMON)
	case false => Element(id,MC.PLEXMON)
    }
}

bool * bool match_ex(req :: MEM_REQUEST, global :: bool, MC :: MEM_CONFIG) = {
    exmon = get_exmon(req.core_id, global, MC);
    a = req.desc.paddress;
    mtch = exmon.active and match_ERG(a,[exmon.address]`48,MC.ERG); --`
    strex = req.write and req.sopar == Exclusive;
    anyway = match global {
	case false => exmon.active and !MC.MISW.IMPL_PLEXMON_STREX_NEEDS_TO_MATCH
	case true => exmon.active and !MC.MISW.IMPL_GEXMON_STREX_NEEDS_TO_MATCH
    };
    return (mtch, strex and anyway)
}

bool gex_supported(req :: MEM_REQUEST, MC :: MEM_CONFIG) = {
    inner = match req.desc.memattrs.inner.attrs {
	case MemAttr_NC => MC.MISW.IMPL_GEXMON_INC_SUPPORT
	case MemAttr_WT => MC.MISW.IMPL_GEXMON_IWT_SUPPORT
        case MemAttr_WB => true
    };		  
    outer = match req.desc.memattrs.outer.attrs {
	case MemAttr_NC => MC.MISW.IMPL_GEXMON_ONC_SUPPORT
	case MemAttr_WT => MC.MISW.IMPL_GEXMON_OWT_SUPPORT
        case MemAttr_WB => true
    };		  
    return inner and outer
}

bool pass_ex(req :: MEM_REQUEST, MC :: MEM_CONFIG) = {
    mtchl, succl = match_ex(req, false, MC);
    mtchg, succg = match_ex(req, true, MC);
    global = (req.desc.memattrs.shareable or req.desc.memattrs.outershareable) and gex_supported(req,MC);
    strex = req.write and (req.sopar == Exclusive);
    match strex, succl, global, succg {
	case true  , false , _     , _     => false
	case true  , true  , false , _     => true
	case true  , true  , true  , false => false
	case true  , true  , true  , true  => true
	case false , _     , _     , _     => true	  
    }
}

MEM_CONFIG clear_ex(req :: MEM_REQUEST, mc :: MEM_CONFIG) = {
    var MC = mc;
    when req.write do {
    var exmon :: ExclusiveMonitor;
    exmon.active <- false;

    mtchl, succl = match_ex(req, false, MC);
    mtchg, succg = match_ex(req, true, MC);
    
    global = (req.desc.memattrs.shareable or req.desc.memattrs.outershareable) and gex_supported(req, MC);
    
    -- clear monitors for current core?
    clrpl = match (req.sopar == Exclusive), mtchl {
	case true , _     => true
	case false, true  => MC.MISW.IMPL_PLEXMON_MATCHING_STR_CLEARS
        case false, false => MC.MISW.IMPL_PLEXMON_MISMATCH_STR_CLEARS
    };
    clrg = succl and match global, (req.sopar == Exclusive), mtchg, succg {
	case false, false, true , _     => MC.MISW.IMPL_GEXMON_NONSHAREABLE_WRITE_CLEARS
        case false, true , true , _     => MC.MISW.IMPL_GEXMON_NONSHAREABLE_WRITE_CLEARS and MC.MISW.IMPL_GEXMON_STREX_SUCCESS_CLEARS 
        case false, true , false, _     => MC.MISW.IMPL_GEXMON_NONSHAREABLE_WRITE_CLEARS and MC.MISW.IMPL_GEXMON_STREX_MISMATCH_SUCCESS_CLEARS
        case true , true , true , _     => MC.MISW.IMPL_GEXMON_STREX_SUCCESS_CLEARS
	case true , true , false, true  => MC.MISW.IMPL_GEXMON_STREX_MISMATCH_SUCCESS_CLEARS
	case true , true , false, false => MC.MISW.IMPL_GEXMON_STREX_MISMATCH_FAIL_CLEARS
	case true , false, true , _     => MC.MISW.IMPL_GEXMON_MATCHING_STR_CLEARS
        case _    , false, false, _     => false
    };

    -- update global monitors and local for current one
    -- TODO: only consider cores in the same shareability domain
    for i in 0 .. Length(MC.GEXMON)-1 do 
	if i == req.core_id then {
	    when clrpl do MC.PLEXMON <- Update(exmon,i,MC.PLEXMON);
	    when clrg do MC.GEXMON <- Update(exmon,i,MC.GEXMON)}
        else {
	    gex = get_exmon(i,true,MC);
	    mtch = gex.active and match_ERG(req.desc.paddress,[gex.address]`48,MC.ERG);	--`
	    relevant = global or MC.MISW.IMPL_GEXMON_NONSHAREABLE_WRITE_CLEARS;
	    when relevant and mtch and (req.sopar != Exclusive or succg) do
	        MC.GEXMON <- Update(exmon,i,MC.GEXMON)
	}   
    };
    return MC
}	     

--------------------------
-- Memory Semantics
--------------------------

MEM_CONFIG rcv(q :: MEM_REQUEST, mc :: MEM_CONFIG) = {
    var MC = mc;
    MC.curr <- q;
    MC.rcvr <- if q.acctype == AccType_PTW then rcvr_mmu else rcvr_core;
    MC.S <- mem_serve;
    return MC
}

MEM_CONFIG * MEM_ABS write_mem(size :: bits(N), ad :: ADR, data :: bool list, mc :: MEM_CONFIG, pm :: MEM_ABS) with N in 8, 16, 32, 64, 128 = 
{
    n = N div 8;
    var d = data;
    var MC = mc;
    var PM = pm;
    
    for i in n-1 .. 0 do {
	PM(ad+[i]) <- [Take(8,d)];
	d <- Drop(8,d)
    };

    --@LOG_MW ([ad], N, data);
	     
    -- update write list, 64-bit granularity
    MC.W <- ad<47:3> @ MC.W;
    when N > 64 do MC.W <- (ad<47:3> + 1) @ MC.W;
    return (MC, PM)
}


bool list read_mem(size :: bits(N), ad :: ADR, PM :: MEM_ABS) with N in 8, 16, 32, 64, 128 = 
{
    n = N div 8;
    var d = Nil :: bool list;

    -- convert to little endian bool list
    for i in 0 .. n-1 do d <- [PM(ad+[i])] : d;

    --@LOG_MR ([ad], N, d);
	     
    return d
}

MEM_TZC make_TZC() = {var tzc; return tzc}

bool TZ_check(q :: MEM_REQUEST, TZC :: MEM_TZC) =
{
    p = TZC(q.desc.paddress<47:12>);

    allowed = match q.ns, q.write
    {
	case true,  false => p.SR
	case true,  true  => p.SW
	case false, false => p.NSR
	case false, true  => p.NSW
    };

    -- TODO: for now TZ check overriden for all system instructions, refine for DZero, writing DC instructions, such NS cache maintenance should never affect secure cache entries, therefore no TZ faults, check this up!
    override = (q.sopar != SysOpNone);
    
    return allowed or override
}


MEM_REQUEST * MEM_CONFIG * MEM_ABS reply(fault :: MEM_FAULT, mc :: MEM_CONFIG, TZC :: MEM_TZC, pm :: MEM_ABS) = {
     var MC = mc;
     var PM = pm;
     var q = MC.curr;
     
     adr = q.desc.paddress;

     -- add TrustZone check, overruns other bus faults
     f = if !TZ_check(q, TZC) then sync_external else fault;
	      
     q.desc.fault.typ <- match f {
	 case sync_parity    => if q.acctype == AccType_PTW then Fault_SyncParityOnWalk else Fault_SyncParity
	 case sync_external  => if q.acctype == AccType_PTW then Fault_SyncExternalOnWalk else Fault_SyncExternal
 	 case async_parity   => Fault_AsyncParity
 	 case async_external => Fault_AsyncExternal
	 case nofault        => Fault_None
     };

     pex = pass_ex(q, MC);
     
     when q.desc.fault.typ == Fault_None and pex do {
     match q.acctype {
	 -- Memory Barriers and cache flush not implemented, NOP for now
	 case AccType_MB or AccType_IC => nothing
	 case AccType_DC => match q.sopar { -- DC ZVA writes blocks of zeroes, N words big block that contains adr
					    case DZero(N) => {a = Align(adr,4*N);
							      for i in 0 .. (N-1) do {
								  (MC1, PM1) = write_mem(0`32, [[a]+i*4], [0`32], MC, PM);
								  MC <- MC1; PM <- PM1}} 
		 case _ => nothing
	     }
	 case _ => { -- reads and writes
		 if q.write then match q.bytesize {
			 case 1 => {(MC1, PM1) = write_mem(0`8, [adr], q.data, MC, PM); --`
				    MC <- MC1; PM <- PM1}
			 case 2 => {(MC1, PM1) = write_mem(0`16, [adr], q.data, MC, PM); --`
				    MC <- MC1; PM <- PM1}
			 case 4 => {(MC1, PM1) = write_mem(0`32, [adr], q.data, MC, PM); --`
				    MC <- MC1; PM <- PM1}
			 case 8 => {(MC1, PM1) = write_mem(0`64, [adr], q.data, MC, PM); --`
				    MC <- MC1; PM <- PM1}
			 case _ => {(MC1, PM1) = write_mem(0`128, [adr], q.data, MC, PM); --`
				    MC <- MC1; PM <- PM1}
		 }	
		 else match q.bytesize {
			 case 1 => q.data <- read_mem(0`8, [adr], PM) --`
			 case 2 => q.data <- read_mem(0`16, [adr], PM) --`
			 case 4 => q.data <- read_mem(0`32, [adr], PM) --`
			 case 8 => q.data <- read_mem(0`64, [adr], PM) --`
			 case _ => q.data <- read_mem(0`128, [adr], PM) --`
		 };
		 -- Update Exclusive monitors in case of Load Exclusive
		 when q.sopar == Exclusive do {
		     var exmon :: ExclusiveMonitor;
		     exmon.active <- true;
		     exmon.address <- [q.desc.paddress];
		     MC.PLEXMON <- Update(exmon,q.core_id, MC.PLEXMON);
		     when (q.desc.memattrs.shareable or
			   q.desc.memattrs.outershareable or
			   MC.MISW.IMPL_GEXMON_NONSHAREABLE_LDEX_RESERVES) do
		             MC.GEXMON <-  Update(exmon,q.core_id, MC.GEXMON)
		 }			      
	 }
     }
     };

     MC <- clear_ex(q, MC);

     when q.write and q.sopar == Exclusive do q.data <- [!pex] @ Nil;
     
     MC.S <- mem_wait;

     return (q, MC, PM)
}
     

-- internal scheduler returns new output action, triggers automaton step
-- S=1 -> receive
-- S=2 -> reply
(MEM_REQUEST option) * bool * MEM_CONFIG * MEM_ABS
mem_sched (req :: (MEM_REQUEST option), fault :: MEM_FAULT, mc :: MEM_CONFIG, TZC :: MEM_TZC, pm :: MEM_ABS) =
{
	var out = None;
	var consumed = false;
	var MC = mc;
	var PM = pm;
	
	when !(req == None and MC.S == mem_wait) do
	{
		match MC.S
		{
		    case mem_wait  => { MC <- rcv(ValOf(req),mc); consumed <- true } 
		    case mem_serve => { (o,mc_new,pm_new) = reply(fault,MC,TZC,PM); out <- Some(o); MC <- mc_new; PM <- pm_new}
		}
	};


	return (out, consumed, MC, PM)
}


-- memory interface uses oracle to determine input, returns consumed input and new output
-- this function is called from HOL as an instantiation of a generic local scheduling function (with polymorphic types for oracle and return types
(MEM_REQUEST option) * (MEM_REQUEST option) * MEM_RCVR * MEM_CONFIG * MEM_ABS
mem_interface (mem_req :: MEM_REQUEST option, mem_f :: MEM_FAULT, MC :: MEM_CONFIG, TZC :: MEM_TZC, PM :: MEM_ABS) =
{
	var input = mem_req;

	(out, consumed, mc_new, pm_new) = mem_sched(input, mem_f, MC, TZC, PM);

	when !consumed do input <- None;	

	return (input, out, mc_new.rcvr, mc_new, pm_new)
}


-- TODO: add core_id for multicore interface, to config and interface

-------------------------
-- write list for output
-------------------------

(ADR * dword) list write_list (mc :: MEM_CONFIG, m :: ADR -> bits(8)) = 
{
    var w = mc.W;
 
    var written :: bits(45) -> bool;

    var res = Nil;
    l = Length(w);
    
    
    when Length(w) > 0 do {
    for i in 1 .. l do { 
	when !written(Head(w)) do {
	    written(Head(w)) <- true;
	    ad = Head(w):'000';
	    res <- (ad, m(ad+7):m(ad+6):m(ad+5):m(ad+4):m(ad+3):m(ad+2):m(ad+1):m(ad)) @ res
	};
	w <- Tail(w)}
    };
    return res
}
